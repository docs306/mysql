MySQL行数：如何在MySQL中获取行数
简介：在本教程中，您将学习各种方法来获取数据库中的MySQL行数。

获取单个表的MySQL行计数
要获取单个表的行数，请在SELECT语句中使用COUNT(*)，如下所示：
```
SELECT 
    COUNT(*)
FROM
    table_name; 
```
例如，要获取示例数据库customers中表的行数，请使用以下语句：
```
SELECT 
    COUNT(*)
FROM
    customers; 
+----------+
| COUNT(*) |
+----------+
|       98 |
+----------+
1 row in set (0.07 sec)
```
获取两个或多个表的MySQL行计数
要获取多个表的行数，可以使用UNION运算符组合每个SELECT语句返回的结果集。

例如，要在单个查询中获取customers和orders表的行数，请使用以下语句。
```
SELECT 
    'customers' tablename, 
     COUNT(*) rows
FROM
    customers 
UNION 
SELECT 
    'orders' tablename, 
     COUNT(*) rows
FROM
    orders; 
+-----------+------+
| tablename | rows |
+-----------+------+
| customers |   98 |
| orders    |  327 |
+-----------+------+
2 rows in set (0.00 sec)
```
获取特定数据库中所有表的MySQL行计数
要获取特定数据库mysqldemo中所有表的行数，请使用以下步骤：

首先，获取数据库中的所有表名
其次，构造一个SQL语句，其中包含SELECT COUNT(*) FROM table_name以分隔的所有表的所有语句UNION。
第三，使用预处理语句执行SQL 语句。
首先，要获取数据库的所有表名，请information_schema按以下方式从数据库中查询：
```
SELECT
	table_name 
FROM
	information_schema.TABLES 
WHERE
	table_schema = 'mysqldemo' 
	AND table_type = 'BASE TABLE'; 
+--------------+
| TABLE_NAME   |
+--------------+
| customers    |
| employees    |
| offices      |
| orderdetails |
| orders       |
| payments     |
| productlines |
| products     |
+--------------+
8 rows in set (0.02 sec)
```
其次，要构造SQL语句，我们使用GROUP_CONCAT 和CONCAT 函数，如下所示：
```
SELECT 
    CONCAT(GROUP_CONCAT(CONCAT('SELECT \'',
                        table_name,
                        '\' table_name,COUNT(*) rows FROM ',
                        table_name)
                SEPARATOR ' UNION '),
            ' ORDER BY table_name')
INTO @sql 
FROM
    table_list; 
```
在此查询中，table_list是表名的列表，这是第一步中查询的结果。

以下查询将第一个查询用作派生表，并以字符串形式返回SQL语句。
```
SELECT
    CONCAT(GROUP_CONCAT(CONCAT('SELECT \'',
                        table_name,
                        '\' table_name,COUNT(*) rows FROM ',
                        table_name)
                SEPARATOR ' UNION '),
            ' ORDER BY table_name')
INTO @sql
FROM
    (SELECT
        table_name
    FROM
        information_schema.tables
    WHERE
        table_schema = 'classicmodels'
            AND table_type = 'BASE TABLE') table_list 
```
如果您使用的是MySQL 8.0+，则可以使用MySQL CTE（公用表表达式）代替派生表：
```
WITH table_list AS (
SELECT
    table_name
  FROM information_schema.tables
  WHERE table_schema = 'classicmodels' AND
        table_type = 'BASE TABLE'
)
SELECT CONCAT(
            GROUP_CONCAT(CONCAT("SELECT '",table_name,"' table_name,COUNT(*) rows FROM ",table_name) SEPARATOR " UNION "),
            ' ORDER BY table_name'
        )
INTO @sql
FROMtable_list; 
```
第三，@sql使用  准备好的语句执行该语句，如下所示：
```
PREPARE s FROM  @sql;
EXECUTE s;
DEALLOCATE PREPARE s; 
```
通过一个查询获取数据库中所有表的MySQL行计数
一种获取数据库中所有表的行数的快速方法是information_schema直接从数据库查询数据：
```
SELECT
    table_name,
    table_rows
FROM
    information_schema.tables
WHERE
    table_schema = 'mysqldemo'
ORDER BY table_name; 
```
此方法有时不准确，因为的行数information_schema与表中的实际行数不同步。为了避免这种情况，必须ANALYZE TABLE在从information_schema数据库查询行数之前运行该语句。
```
 ANALYZE TABLE table_name, ...; 
```