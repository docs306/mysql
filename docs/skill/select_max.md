MySQL 查询第 N 高记录

使用MAX或MIN功能可以轻松选择数据库表中的最高或最低记录。但是，选择第n 个最高记录有点棘手。例如，从产品表中获得第二昂贵的产品。

要选择第n 个 最高记录，您需要执行以下步骤：

首先，您获得n个最高记录并按升序排序。第n 个 最高记录是结果集中的最后一个记录。
然后按降序对结果集进行排序并获取第一个结果集。
以下是按升序获取第n 个最高记录的查询：
```
SELECT 
    *
FROM
    table_name
ORDER BY column_name ASC
LIMIT N; 
```
获得第n 个最高记录的查询如下：
```
SELECT 
    *
FROM
    (SELECT 
        *
    FROM
        table_name
    ORDER BY column_name ASC
    LIMIT N) AS tbl
ORDER BY column_name DESC
LIMIT 1; 
```
幸运的是，MySQL为我们提供了LIMIT子句，子句约束返回结果集中的行数。您可以将以上查询重写为以下查询：
```
SELECT 
    *
FROM
    table_name
ORDER BY column_name DESC
LIMIT n - 1, 1; 
```
查询返回n-1行后的第一行，因此您获得第n 个最高记录。

获得第n个最高记录示例
例如，如果要在表中获得第二昂贵的产品（n = 2）products，请使用以下查询：
```
SELECT 
    productCode, productName, buyPrice
FROM
    products
ORDER BY buyPrice DESC
LIMIT 1 , 1; 
+-------------+--------------------------------+----------+
| productCode | productName                    | buyPrice |
+-------------+--------------------------------+----------+
| S18_2238    | 1998 Chrysler Plymouth Prowler |   101.51 |
+-------------+--------------------------------+----------+
1 row in set (0.01 sec)
```
获得第n 个最高记录的第二种技术是使用MySQL子查询：
```
SELECT *
FROM table_name AS a 
WHERE n - 1 = (
 SELECT COUNT(primary_key_column) 
 FROM products b 
 WHERE  b.column_name > a. column_name) 
```
您可以使用第一种技术获得相同的结果，以获得第二个最昂贵的产品，如下面的查询：
```
SELECT 
    productCode, productName, buyPrice
FROM
    products a
WHERE
    1 = (SELECT 
            COUNT(productCode)
        FROM
            products b
        WHERE
            b.buyPrice > a.buyPrice); 
+-------------+--------------------------------+----------+
| productCode | productName                    | buyPrice |
+-------------+--------------------------------+----------+
| S18_2238    | 1998 Chrysler Plymouth Prowler |   101.51 |
+-------------+--------------------------------+----------+
1 row in set (0.04 sec)
```