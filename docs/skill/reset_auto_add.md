MySQL 重置自增值
简介：在本教程中，我们将向您展示重置中列的AUTO_INCREMENTMySQL自动递增值的各种方法。

MySQL为您提供了一个称为自动增量的有用功能。您可以将AUTO_INCREMENT属性分配给表的列，以便为新行生成唯一标识。通常，您将AUTO_INCREMENT属性用于表的主键列。

无论何时向表中插入新行，MySQLAUTO_INCREMENT都会自动为列分配序列号。

例如，如果表有八行，并且您在不指定自动增量列的值的情况下插入新行，则MySQL将自动插入id值为9 的新行。

有时，您可能需要重置自动增量列的值，以便插入表中的第一个记录的标识从特定数字开始，例如1。

在MySQL中，您可以通过各种方式重置自动增量值。

MySQL重置自动增量值示例
首先，创建一个名为的tmp表，并将AUTO_INCREMENT属性  分配给  id  主键列。
```
CREATE TABLE tmp (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(45) DEFAULT NULL,
  PRIMARY KEY (id)
); 
```
其次，将一些示例数据插入tmp  表中：
```
INSERT INTO tmp(name)
VALUES('test 1'),
      ('test 2'),
      ('test 3'); 
```
三，查询tmp  表以确认插入操作：
```
SELECT 
    *
FROM
    tmp; 
+----+--------+
| id | name   |
+----+--------+
|  1 | test 1 |
|  2 | test 2 |
|  3 | test 3 |
+----+--------+
3 rows in set (0.00 sec)
```
我们有三行，ID列的值分别为1,2和3.完美！是时候练习重置ID列的自动增量值了。

查看自动量值，红色标记的位置
```
mysql> show create table tmp \G;
*************************** 1. row ***************************
       Table: tmp
Create Table: CREATE TABLE `tmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
1 row in set (0.02 sec)
```
使用ALTER TABLE语句
您可以使用ALTER TABLE语句重置自动增量值。ALTER TABLE  重置自动增量值的语句的语法如下：
```
ALTER TABLE table_name AUTO_INCREMENT = value; 
```
您可以在ALTER TABLE子句后指定表名，并value在表达式中指定要重置的表名AUTO_INCREMENT=value。

请注意，value必须大于或等于自动增量列的当前最大值。

让我们删除tmp  表中最后一条id 值为3的记录：
```
DELETE FROM tmp 
WHERE
    ID = 3; 
```
如果插入新行，MySQL将为新行的id列分配4  。但是，您可以使用以下ALTER TABLE  语句将MySQL生成的数字重置为3 ：
```
ALTER TABLE tmp AUTO_INCREMENT = 3; 
```
再查看一下自增值，红色标记：
```
mysql> show create table tmp \G;
*************************** 1. row ***************************
       Table: tmp
Create Table: CREATE TABLE `tmp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
1 row in set (0.02 sec)
```
现在，让我们尝试在tmp  表中插入一个新行并从中查询数据以查看效果：
```
INSERT INTO tmp(name)
VALUES ('MySQL example 3');
 
SELECT 
    *
FROM
    tmp; 
+----+-----------------+
| id | name            |
+----+-----------------+
|  1 | test 1          |
|  2 | test 2          |
|  3 | MySQL example 3 |
+----+-----------------+
3 rows in set (0.02 sec)
```
我们有三行，最后一个自动增量值是3而不是4，这是我们所期望的。

使用TRUNCATE TABLE语句
TRUNCATE TABLE语句从表中删除所有数据并重置自动递增值为零。

以下说明了TRUNCATE TABLE  语句的语法：
```
TRUNCATE TABLE table_name;
``` 
通过使用TRUNCATE TABLE  语句，您可以永久删除表中的所有数据，并将自动增量值重置为零。

使用DROP TABLE和CREATE TABLE语句
您可以使用一对语句：DROP TABLE和CREATE TABLE来重置自动增量列。请注意，此方法会永久删除表中的所有数据。

与TRUNCATE TABLE  语句一样，这些语句会删除表并重新创建它，因此，自动增量的值将重置为零。
```
DROP TABLE table_name;
CREATE TABLE table_name(...); 
```