MySQL NULL 映射


EFCodd博士是数据库关系模型的创建者，他在关系数据库理论中引入了NULL这一概念。根据Dr.EFCodd，NULL意味着未知的价值或缺少信息。

MySQL还支持NULL代表丢失或不适用信息的概念。

在数据库表中，存储包含NULL值的数据。当您以报告的形式向用户显示数据时，显示NULL值是没有意义的。

为了使报告更具可读性和可理解性，您必须将NULL值显示为其他值，例如未知，缺失或不可用（N / A）。为此，您可以使用IF函数。

IF函数的语法如下：
```
IF(exp,exp_result1,exp_result2); 
```
如果exp求值为TRUE（when exp <> 0和exp <> NULL），则IF函数返回exp_result1值，否则返回exp_result2值。

IF函数的返回值可以是字符串或数字，具体取决于exp_result1和exp_result2表达式。

让我们通过一些例子来练习，以便更好地理解。

我们将使用示例数据库中的customers表。

以下是customers表中包含customername state和的部分数据country：
```
SELECT 
    customername, state, country
FROM
    customers
ORDER BY country; 
+------------------------------------+---------------+--------------+
| customername                       | state         | country      |
+------------------------------------+---------------+--------------+
| Australian Collectors, Co.         | Victoria      | Australia    |
| Anna's Decorations, Ltd            | NSW           | Australia    |
| Souveniers And Things Co.          | NSW           | Australia    |
| Australian Gift Network, Co        | Queensland    | Australia    |
| Australian Collectables, Ltd       | Victoria      | Australia    |
| Salzburg Collectables              | NULL          | Austria      |
| Mini Auto Werke                    | NULL          | Austria      |
| Petit Auto                         | NULL          | Belgium      |
| Royale Belge                       | NULL          | Belgium      |
| Canadian Gift Exchange Network     | BC            | Canada       |
...
```
从上面的结果集中，您可以看到某些客户无法使用状态值。您可以使用IF函数将NULL值显示为N/A：
```
SELECT 
    customername, IF(state IS NULL, 'N/A', state) state, country
FROM
    customers
ORDER BY country; 
+------------------------------------+---------------+--------------+
| customername                       | state         | country      |
+------------------------------------+---------------+--------------+
| Australian Collectors, Co.         | Victoria      | Australia    |
| Anna's Decorations, Ltd            | NSW           | Australia    |
| Souveniers And Things Co.          | NSW           | Australia    |
| Australian Gift Network, Co        | Queensland    | Australia    |
| Australian Collectables, Ltd       | Victoria      | Australia    |
| Salzburg Collectables              | N/A           | Austria      |
| Mini Auto Werke                    | N/A           | Austria      |
| Petit Auto                         | N/A           | Belgium      |
| Royale Belge                       | N/A           | Belgium      |
| Canadian Gift Exchange Network     | BC            | Canada       |
...
```
除IF函数外，MySQL还提供了IFNULL 函数，允许您NULL直接处理值。以下是IFNULL函数的语法：

IFNULL(exp,exp_result); 
如果求值为某个值，则IFNULL函数返回exp_result表达式exp的NULL值，否则返回exp表达式的值。

以下查询使用IFNULL函数显示NULL为未知如下：
```
SELECT customername, 
       IFNULL(state,"N/A") state, 
       country
FROM customers
ORDER BY country; 
+------------------------------------+---------------+--------------+
| customername                       | state         | country      |
+------------------------------------+---------------+--------------+
| Australian Collectors, Co.         | Victoria      | Australia    |
| Anna's Decorations, Ltd            | NSW           | Australia    |
| Souveniers And Things Co.          | NSW           | Australia    |
| Australian Gift Network, Co        | Queensland    | Australia    |
| Australian Collectables, Ltd       | Victoria      | Australia    |
| Salzburg Collectables              | N/A           | Austria      |
| Mini Auto Werke                    | N/A           | Austria      |
| Petit Auto                         | N/A           | Belgium      |
| Royale Belge                       | N/A           | Belgium      |
| Canadian Gift Exchange Network     | BC            | Canada       |
...
```