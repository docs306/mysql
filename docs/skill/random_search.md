MySQL 随机查询
简介：在本教程中，您将学习从MySQL中的数据库表中查询随机记录的各种技术。

有时，您必须从表中选择随机记录，例如：

- 选择博客中的一些随机帖子并将其显示在侧边栏中。
- 选择随机引用以显示“当日报价”小插件。
- 选择图库中的随机图片并用作精选图片。
MySQL使用ORDER BY RAND() 选择随机记录
MySQL没有任何内置语句来从数据库表中选择随机记录。为了实现这一点，您可以使用RAND功能。以下查询从数据库表中选择一个随机记录：
```
SELECT 
    *
FROM
    tbl
ORDER BY RAND()
LIMIT 1; 
```
让我们更详细地检查一下查询。

- RAND()函数为表中的每一行生成一个随机值。
- 在ORDER BY子句由RAND()函数产生的随机数排序表中的所有行。
- LIMIT子句选择在随机排序的结果集中的第一行。
如果要从数据库表中选择N个随机记录，则需要更改LIMIT子句，如下所示：
```
SELECT 
    *
FROM
    table
ORDER BY RAND()
LIMIT N; 
```
例如，要在customers表中选择5个随机客户，请使用以下查询：
```
SELECT 
    t.customerNumber, t.customerName
FROM
    customers AS t
ORDER BY RAND()
LIMIT 5; 
+----------------+------------------------------+
| customerNumber | customerName                 |
+----------------+------------------------------+
|            242 | Alpha Cognac                 |
|            157 | Diecast Classics Inc.        |
|            486 | Motor Mint Distributors Inc. |
|            324 | Stylish Desk Decors, Co.     |
|            465 | Anton Designs, Ltd.          |
+----------------+------------------------------+
5 rows in set (0.51 sec)
```
您可能会获得不同的结果集，因为它是随机的。

这种技术适用于小表。对于大表，它会非常慢，因为MySQL必须对整个表进行排序以选择随机表。查询的速度还取决于表中的行数。表具有的行越多，为每行生成随机数所花费的时间就越多。

MySQL使用INNER JOIN子句选择随机记录
此技术要求您要选择随机记录的表具有自动增量 主键字段，并且序列中没有间隙。

以下查询基于主键列生成随机数：
```
SELECT
	ROUND( RAND( ) * ( SELECT MAX( id ) FROM TABLE ) ) AS id; 
```
我们可以使用上面的查询返回的结果集加入表，如下所示：
```
SELECT
	t.* 
FROM
	TABLE AS t
	JOIN ( SELECT ROUND( RAND( ) * ( SELECT MAX( id ) FROM TABLE ) ) AS id ) AS x 
WHERE
	t.id >= x.id 
	LIMIT 1; 
```
使用此技术，您必须多次执行查询才能获得多个随机行，因为如果增加限制，查询将只提供从随机选定行开始的连续行。

以下查询从customers表中返回一个随机客户。
```
SELECT
	t.customerNumber,
	t.customerName 
FROM
	customers AS t
	JOIN ( SELECT ROUND( RAND( ) * ( SELECT MAX( customerNumber ) FROM customers ) ) AS customerNumber ) AS x 
WHERE
	t.customerNumber >= x.customerNumber 
	LIMIT 1; 
+----------------+-------------------+
| customerNumber | customerName      |
+----------------+-------------------+
|            273 | Franken Gifts, Co |
+----------------+-------------------+
1 row in set (0.01 sec)
```
MySQL使用变量选择随机记录
如果表中的id列的值在1 .. N范围内且范围内没有间隙，则可以使用以下技术：

首先，选择1..N范围内的随机数。
其次，根据随机数选择记录。
以下语句可帮助您完成此任务：
```
SELECT TABLE.* 
FROM
	(
	SELECT
		ROUND( RAND( ) * ( SELECT MAX( id ) FROM TABLE ) ) random_num,
		@num := @num + 1 
	FROM
		( SELECT @num := 0 ) AS a,
	TABLE 
		LIMIT N 
	) AS b,
	TABLE AS t 
WHERE
	b.random_num = t.id; 
```