MySQL 间隔值

MySQL间隔值简介
MySQL间隔值主要用于日期和时间计算。要创建间隔值，请使用以下表达式：
```
INTERVAL expr unit 
```
后跟INTERVAL关键字是expr确定间隔值，并unit指定间隔单位。例如，要创建1天间隔，请使用以下表达式：
```
INTERVAL 1 DAY 
```
请注意，INTERVAL并且UNIT不区分大小写，因此以下表达式与上面的表达式相同：
```
interval 1 day 
```
我们主要使用日期和时间算术的间隔值，如下所示：
```
date + INTERVAL expr unit
date - INTERVAL expr unit 
```
间隔值也用于在各种时间的功能，例如DATE_ADD，DATE_SUB，TIMESTAMPADD和TIMESTAMPDIFF

MySQL的定义标准格式expr和unit，如下表所示：
```
单位 	描述 
DAY	DAYS
DAY_HOUR	‘DAYS HOURS’
DAY_MICROSECOND	‘DAYS HOURS:MINUTES:SECONDS.MICROSECONDS’
DAY_MINUTE	‘DAYS HOURS:MINUTES’
DAY_SECOND	‘DAYS HOURS:MINUTES:SECONDS’
HOUR	HOURS
HOUR_MICROSECOND	‘HOURS:MINUTES:SECONDS.MICROSECONDS’
HOUR_MINUTE	‘HOURS:MINUTES’
HOUR_SECOND	‘HOURS:MINUTES:SECONDS’
MICROSECOND	MICROSECONDS
MINUTE	MINUTES
MINUTE_MICROSECOND	‘MINUTES:SECONDS.MICROSECONDS’
MINUTE_SECOND	‘MINUTES:SECONDS’
MONTH	MONTHS
QUARTER	QUARTERS
SECOND	SECONDS
SECOND_MICROSECOND	‘SECONDS.MICROSECONDS’
WEEK	WEEKS
YEAR	YEARS
YEAR_MONTH	‘YEARS-MONTHS’
```
MySQL间隔示例
以下语句2020-01-01为返回添加1天2020-01-02：
```
mysql> SELECT '2020-01-01' + INTERVAL 1 DAY;
+-------------------------------+
| '2020-01-01' + INTERVAL 1 DAY |
+-------------------------------+
| 2020-01-02                    |
+-------------------------------+
1 row in set (0.00 sec)
```
如果在包含a DATE或DATETIMEvalue 的表达式中使用了interval值，并且interval值位于表达式的右侧，则可以使用expr以下示例中所示的负值   ：
```
mysql> SELECT '2020-01-01' + INTERVAL -1 DAY;
+--------------------------------+
| '2020-01-01' + INTERVAL -1 DAY |
+--------------------------------+
| 2019-12-31                     |
+--------------------------------+
1 row in set (0.00 sec)
```
以下语句显示如何使用DATE_ADD和DATE_SUB从日期值中添加/减去1个月：
```
SELECT DATE_ADD('2020-01-01', INTERVAL 1 MONTH) 1_MONTH_LATER, 
       DATE_SUB('2020-01-01',INTERVAL 1 MONTH) 1_MONTH_BEFORE;
+---------------+----------------+
| 1_MONTH_LATER | 1_MONTH_BEFORE |
+---------------+----------------+
| 2020-02-01    | 2019-12-01     |
+---------------+----------------+
1 row in set (0.00 sec) 
```
以下查询使用TIMESTAMPADD(unit,interval,expression)函数将30分钟添加到时间戳值：
```
mysql> SELECT DATE_ADD('2020-01-01', INTERVAL 1 MONTH) 1_MONTH_LATER,
    -> DATE_SUB('2020-01-01',INTERVAL 1 MONTH) 1_MONTH_BEFORE;
+---------------+----------------+
| 1_MONTH_LATER | 1_MONTH_BEFORE |
+---------------+----------------+
| 2020-02-01    | 2019-12-01     |
+---------------+----------------+
1 row in set (0.00 sec)
```
MySQL间隔实际例子
让我们创建一个名为memberships演示的新表：
```
CREATE TABLE memberships (
    id INT AUTO_INCREMENT PRIMARY KEY,
    email VARCHAR(355) NOT NULL,
    plan VARCHAR(255) NOT NULL,
    expired_date DATE NOT NULL
); 
```
memberships表中，expired_date列存储每个成员的成员资格的到期日期。

以下语句将一些行插入memberships表中。
```
INSERT INTO memberships(email, plan, expired_date)
VALUES('john.doe@example.com','Gold','2017-07-13'),
      ('jane.smith@example.com','Platinum','2017-07-10'),
      ('david.corp@example.com','Silver','2017-07-15'),
      ('julia.william@example.com','Gold','2017-07-20'),
      ('peter.drucker@example.com','Silver','2017-07-08'); 
```
假设今天是2017-07-06，您可以使用以下查询在7天内找到其成员资格已过期的成员：
```
SELECT 
    email,
    plan,
    expired_date,
    DATEDIFF(expired_date, '2017-07-06') remaining_days
FROM
    memberships
WHERE
    '2017-07-06' BETWEEN DATE_SUB(expired_date, INTERVAL 7 DAY) AND expired_date; 
+---------------------------+----------+--------------+----------------+
| email                     | plan     | expired_date | remaining_days |
+---------------------------+----------+--------------+----------------+
| john.doe@example.com      | Gold     | 2017-07-13   |              7 |
| jane.smith@example.com    | Platinum | 2017-07-10   |              4 |
| peter.drucker@example.com | Silver   | 2017-07-08   |              2 |
+---------------------------+----------+--------------+----------------+
3 rows in set (0.00 sec)
```