MySQL UUID

MySQL UUID简介
UUID代表Universally Unique IDentifier。UUID基于RFC 4122 “通用唯一标识符（UUID）URN命名空间”定义。

UUID被设计为在空间和时间上全球唯一的数字。预计两个UUID值是不同的，即使它们是在两个独立的服务器上生成的。

在MySQL中，UUID值是一个128位数字，表示为五个十六进制数字的utf8字符串，格式如下：

aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee 
要生成UUID值，请使用以下UUID()函数：

UUID() 
UUID()函数返回符合RFC 4122中描述的UUID版本1的UUID值。

例如，以下语句使用UUID()函数生成UUID值：
```
mysql> select UUID();
+--------------------------------------+
| UUID()                               |
+--------------------------------------+
| cc2bccd0-c979-11e9-ba8d-d70282892727 |
+--------------------------------------+
1 row in set (0.00 sec)
```
MySQL UUID与INT自增作为主键比较
优点
将UUID用作主键 具有以下优点：

表，数据库甚至服务器之间的UUID值是唯一的，它们允许您合并来自不同数据库的行或跨服务器分发数据库。
UUID值不会公开有关数据的信息，因此在URL中使用它们更安全。例如，如果ID为10的客户通过http://www.example.com/customers/10/URL 访问其帐户，则很容易猜到有客户11,12等，这可能是攻击的目标。
可以在避免往返数据库服务器的任何地方生成UUID值。它还简化了应用程序中的逻辑。例如，要将数据插入父表和子表，您必须先插入父表，获取生成的ID，然后将数据插入子表。通过使用UUID，您可以预先生成父表的主键值，并在事务中同时将行插入父表和子表。
缺点
除了优点，UUID值也有一些缺点：

存储UUID值（16字节）比整数（4字节）或甚至大整数（8字节）占用更多存储空间。
调试似乎更难，想象一下表达WHERE id = 'df3b7cb7-6a95-11e7-8846-b05adad3f0ae' 而不是WHERE id = 10
使用UUID值可能会导致性能问题，因为它们的大小和没有被排序。
MySQL UUID解决方案
在MySQL中，您可以使用紧凑格式（BINARY）存储UUID值，并使用以下函数以人类可读的格式(VARCHAR) 显示它们：

UUID_TO_BIN
BIN_TO_UUID
IS_UUID
请注意UUID_TO_BIN()，BIN_TO_UUID()和IS_UUID()功能仅在MySQL 8.0或更高版本可用。
UUID_TO_BIN() 函数将UUID从人类可读格式（VARCHAR）转换为紧凑格式（BINARY）格式用于存储，并且BIN_TO_UUID()函数将UUID从紧凑格式（BINARY）转换为人类可读格式（VARCHAR）以进行显示。

IS_UUID()如果参数是有效的字符串格式UUID，则函数返回1。如果参数不是有效的字符串格式UUID，则IS_UUID函数返回0.如果参数为NULL，则  IS_UUID()函数返回NULL。

以下是MySQL中有效的字符串格式UUID：

aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee
aaaaaaaabbbbccccddddeeeeeeeeeeee
{aaaaaaaa-bbbb-cccc-dddd-eeeeeeeeeeee} 
MySQL UUID示例
我们来看一个使用UUID作为主键的示例。

以下语句创建一个名为的新表people：
```
CREATE TABLE people (
    id BINARY(16) PRIMARY KEY,
    name VARCHAR(255)
); 
```
要插入 UUID值入id列，使用UUID()和UUID_TO_BIN()功能如下：
```
INSERT INTO people(id, name)
VALUES(UUID_TO_BIN(UUID()),'John Doe'),
      (UUID_TO_BIN(UUID()),'Will Smith'),
      (UUID_TO_BIN(UUID()),'Mary Jane'); 
```
要从UUID列查询数据，可以使用BIN_TO_UUID()函数将二进制格式转换为人类可读的格式：
```
SELECT 
    BIN_TO_UUID(id) id, 
    name
FROM
    people; 
+--------------------------------------+------------+
| id                                   | name       |
+--------------------------------------+------------+
| a5a207e7-c97b-11e9-9ffb-c85b762a1a33 | John Doe   |
| a5a247cf-c97b-11e9-9ffb-c85b762a1a33 | Will Smith |
| a5a249d2-c97b-11e9-9ffb-c85b762a1a33 | Mary Jane  |
+--------------------------------------+------------+
3 rows in set (0.01 sec)
```