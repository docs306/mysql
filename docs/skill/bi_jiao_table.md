MySQL 比较两个表
摘要：在本教程中，您将学习如何比较两个表以查找不匹配的记录。

在数据迁移中，我们经常必须比较两个表以标识一个表中的一条记录，而另一表中没有相应的记录。

例如，我们有一个新数据库，其架构与旧数据库不同。我们的任务是将所有数据从旧数据库迁移到新数据库，并验证数据是否已正确迁移。

要检查数据，我们必须比较两个表，一个在新数据库中，一个在旧数据库中，并识别不匹配的记录。

假设我们有两个表：t1  和 t2。以下步骤比较两个表并标识不匹配的记录：

首先，使用UNION语句合并两个表中的行；仅包括需要比较的列。返回的结果集用于比较。
```
SELECT t1.pk, t1.c1
FROM t1
UNION ALL
SELECT t2.pk, t2.c1
FROM t2 
```
其次，根据需要比较的主键和列将记录分组。如果需要比较的列中的值相同，则COUNT(*)返回2，否则 COUNT(*)返回1。

请参阅以下查询：
```
SELECT pk, c1
FROM
 (
   SELECT t1.pk, t1.c1
   FROM t1
   UNION ALL
   SELECT t2.pk, t2.c1
   FROM t2
)  t
GROUP BY pk, c1
HAVING COUNT(*) = 1
ORDER BY pk 
```
如果比较中涉及的列中的值相同，则不返回任何行。

MySQL 比较两个表的例子
让我们来看一个例子模仿上面的步骤。

首先，创建2个具有相似结构的表：
```
CREATE TABLE t1(
    id int auto_increment primary key,
    title varchar(255)    
);
 
CREATE TABLE t2(
    id int auto_increment primary key,
    title varchar(255),
    note varchar(255)
); 
```
其次，在t1和t2表中插入一些数据：
```
INSERT INTO t1(title)
VALUES('row 1'),('row 2'),('row 3');
 
INSERT INTO t2(title,note)
SELECT title, 'data migration'
FROM t1; 
```
第三，比较两个表的id和title列的值：
```
SELECT id,title
FROM (
SELECT id, title FROM t1
UNION ALL
SELECT id,title FROM t2
) tbl
GROUP BY id, title
HAVING count(*) = 1
ORDER BY id; 
```
没有行返回，因为没有不匹配的记录。

第四，在t2表中插入新行：
```
INSERT INTO t2(title,note)
VALUES('new row 4','new'); 
```
第五，再次执行查询以比较两个表中标题列的值。新记录中未匹配的则被返回。
```
+----+-----------+
| id | title     |
+----+-----------+
|  4 | new row 4 |
+----+-----------+
1 row in set (0.00 sec) 
```