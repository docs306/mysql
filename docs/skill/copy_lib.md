MySQL 复制库
简介：本教程介绍如何在同一服务器上以及从服务器到另一台服务器上复制MySQL数据库。

在同一服务器上复制MySQL数据库
要复制MySQL数据库，您需要执行以下步骤：

首先，使用CREATE DATABASE创建一个新数据库。
其次，使用mysqldump工具导出要从中复制的数据库的所有数据库对象和数据。
第三，将SQL转储文件导入新数据库。
为了演示，我们将mysqldemo数据库复制到mysqldemo_backup数据库。

第1步。创建mysqldemo_backup数据库：

首先，登录MySQL数据库服务器：
```
>mysql -u root -p
Enter password: ********** 
```
然后，使用CREATE DATABASE语句如下：
```
> CREATE DATABASE mysqldemo_backup; 
```
三，使用SHOW DATABASES命令验证：
```
> SHOW DATABASES 
```
MySQL数据库服务器返回以下输出：
```
+----------------------+
| Database             |
+----------------------+
| mysqldemo            |
| mysqldemo_backup     |
| information_schema   |
| mysql                |
| performance_schema   |
| sys                  |
+----------------------+
6 rows in set (0.00 sec) 
```
如您所见，我们已mysqldemo_backup成功创建数据库。

第2步。使用mysqldump工具将数据库对象和数据转储到SQL文件中。

假设您要将数据库对象和数据库数据转储mysqldemo到位于D:\db文件夹的SQL文件中，这里是命令：
```
>mysqldump -u root -p mysqldemo > d:\db\mysqldemo.sql
Enter password: ********** 
```
基本上，此命令指示mysqldump使用带密码的root 用户帐户登录MySQL服务器，并将数据库对象和数据库导出mysqldemo到d:\db\mysqldemo.sql。请注意，operator（>）表示导出。

第3步。将d:\db\mysqldemo.sql文件导入mysqldemo_backup数据库。
```
>mysql -u root -p mysqldemo_backup < d:\db\mysqldemo.sql
Enter password: ********** 
```
请注意，operator（<）表示导入。

要验证导入，可以使用SHOW TABLES命令执行快速检查。
```
> SHOW TABLES FROM mysqldemo_backup; 
```
它返回以下输出：
```
+--------------------------------+
| Tables_in_mysqldemo_backup |
+--------------------------------+
| customers                      |
| employees                      |
| offices                        |
| orderdetails                   |
| orders                         |
| payments                       |
| productlines                   |
| products                       |
+--------------------------------+
8 rows in set (0.01 sec) 
```
如您所见，我们已成功将所有对象和数据从mysqldemo数据库复制到mysqldemo_backup数据库。

将MySQL数据库从服务器复制到另一个服务器
要将MySQL数据库从服务器复制到另一个服务器，请使用以下步骤：

将源服务器上的数据库导出到SQL转储文件。
将SQL转储文件复制到目标服务器
将SQL转储文件导入目标服务器
我们来看看如何将mysqldemo数据库从服务器复制到另一个服务器。

首先，将mysqldemo数据库导出到db.sql文件。
```
>mysqldump -u root -p --databases mysqldemo > d:\db\db.sql
Enter password: ********** 
```
请注意，--database选项允许在SQL转储文件中mysqldump包含CREATE DATABASE和USE语句。这些语句将mysqldemo在目标服务器中创建数据库，并将新数据库作为加载数据的默认数据库。

简而言之，当我们使用--databaseoption 时，SQL转储文件的开头包含以下语句。
```
CREATE DATABASE `mysqldemo`.
 
USE `mysqldemo`; 
```
第三，db.sql假设db.sql文件已复制到c:\tmp\ 目录 ，将文件导入数据库服务器。
```
>mysql -u root -p mysqldemo < c:\tmp\db.sql 
```