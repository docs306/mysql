MySQL SELECT INTO 变量
简介：在本教程中，您将学习如何使用MySQL SELECT INTO variable 将查询结果存储在变量中。

MySQL SELECT INTO变量语法
要将查询结果存储在一个或多个变量中，请使用以下SELECT INTO variable语法：
```
SELECT 
    c1, c2, c3, ...
INTO 
    @v1, @v2, @v3,...
FROM 
    table_name
WHERE 
    condition; 
```
在这个语法中：

c1，c2和c3是要选择并存储到变量中的列或表达式。
@ v1，@ v2和@ v3是存储c1，c2和c3值的变量。
变量的数量必须与选择列表中的列数或表达式的数量相同。此外，查询必须返回零行或一行。

如果查询没有返回任何行，MySQL会发出没有数据的警告，并且变量的值保持不变。

如果查询返回多行，MySQL会发出错误。要确保查询始终返回最多一行，请使用LIMIT 1子句将结果集限制为单行。

MySQL SELECT INTO变量示例
我们将使用示例数据库中的customers表进行演示。
```
+--------------------+
| products           |
+--------------------+
| productCode        |
| productName        |
| productLine        |
| productScale       |
| productVendor      |
| productDescription |
| quantityInStock    |
| buyPrice           |
| MSRP               |
+--------------------+
9 rows in set (0.03 sec)
```
MySQL SELECT INTO单变量示例
以下语句使用数字103获取客户的城市并将其存储在@city变量中：
```
SELECT 
    city
INTO
    @city
FROM 
    customers
WHERE 
    customerNumber = 103; 
```
以下语句显示@city变量的内容：
```
SELECT @city; 
+--------+
| @city  |
+--------+
| Nantes |
+--------+
1 row in set (0.00 sec)
```
MySQL SELECT INTO多变量示例
要将选择列表中的值存储到多个变量中，请使用逗号分隔变量。例如，以下语句查找客户编号103的城市和国家/地区，并将数据存储在两个对应的变量@city和@country中：
```
SELECT 
    city,
    country 
INTO
    @city,
    @country
FROM 
    customers
WHERE 
    customerNumber = 103; 
```
以下语句显示了@city和@country变量的内容：
```
SELECT @city, @country; 
+--------+----------+
| @city  | @country |
+--------+----------+
| Nantes | France   |
+--------+----------+
1 row in set (0.00 sec)
```
MySQL SELECT INTO变量 - 多行示例
以下语句导致错误，因为查询返回多行：
```
SELECT 
    creditLimit
INTO
    @creditLimit
FROM 
    customers
WHERE 
    customerNumber > 103; 
```
这是输出：

ERROR 1172 (42000): Result consisted of more than one row
要修复它，请使用以下LIMIT 1子句：
```
SELECT 
    creditLimit
INTO
    @creditLimit
FROM 
    customers
WHERE 
    customerNumber > 103
LIMIT 1; 
mysql> select @creditLimit;
+--------------+
| @creditLimit |
+--------------+
|     71800.00 |
+--------------+
1 row in set (0.00 sec)
```