MySQL WHERE子句来过滤结果集中的行。

MySQL WHERE子句简介
WHERE子句允许您指定查询返回的行的搜索条件。以下显示了WHERE子句的语法：

```sql
SELECT 
    select_list
FROM
    table_name
WHERE
    search_condition; 
```
search_condition是使用逻辑运算符的一个或多个谓词的组合AND，OR和NOT。在SQL中，谓词是一个计算结果为true，false或unknown的表达式。

从中table_name查出search_condition 为true的任何行都将包含在最终结果集中。

除了SELECT语句之外，您还可以在WHERE子句使用and 指定要更新和删除的行。UPDATEDELETE

MySQL WHERE子句实例
我们将使用示例数据库中的  employees表进行演示。

```sql
+----------------+
| employees      |
+----------------+
| employeeNumber |
| lastName       |
| firstName      |
| extension      |
| email          |
| officeCode     |
| reportsTo      |
| jobTitle       |
+----------------+
8 rows in set (0.01 sec)
```
以下查询查找职位名称为的员工Sales Rep：

```sql
SELECT
    lastname,
    firstname,
    jobtitle
FROM
    employees
WHERE
    jobtitle = 'Sales Rep'; 
```
运行结果：

```sql
+-----------+-----------+-----------+
| lastname  | firstname | jobtitle  |
+-----------+-----------+-----------+
| Jennings  | Leslie    | Sales Rep |
| Thompson  | Leslie    | Sales Rep |
| Firrelli  | Julie     | Sales Rep |
| Patterson | Steve     | Sales Rep |
| Tseng     | Foon Yue  | Sales Rep |
| Vanauf    | George    | Sales Rep |
| Bondur    | Loui      | Sales Rep |
| Hernandez | Gerard    | Sales Rep |
| Castillo  | Pamela    | Sales Rep |
| Bott      | Larry     | Sales Rep |
| Jones     | Barry     | Sales Rep |
| Fixter    | Andy      | Sales Rep |
| Marsh     | Peter     | Sales Rep |
| King      | Tom       | Sales Rep |
| Nishi     | Mami      | Sales Rep |
| Kato      | Yoshimi   | Sales Rep |
| Gerard    | Martin    | Sales Rep |
+-----------+-----------+-----------+
17 rows in set (0.00 sec)
```
即使WHERE子句出现在语句的末尾，MySQL 也会WHERE首先评估子句中的表达式以选择匹配的行。它选择jobtitle 为Sales Rep的所有行

```sql
jobtitle = 'Sales Rep'; 
```
然后，MySQL从SELECT子句中的选择列表中选择列。突出显示的区域包含最终结果集中的列和行。

您可以形成一个简单的条件，如上面的查询，或一个将多个表达式与逻辑运算符组合在一起的非常复杂的条件

例如，要在办offericeCode=1中查找所有销售代表，请使用以下查询：

```sql
SELECT 
    lastname, 
    firstname, 
    jobtitle
FROM 
    employees
WHERE
    jobtitle = 'Sales Rep' AND
officeCode = 1; 
```
运行结果：

```sql
+----------+-----------+-----------+
| lastname | firstname | jobtitle  |
+----------+-----------+-----------+
| Jennings | Leslie    | Sales Rep |
| Thompson | Leslie    | Sales Rep |
+----------+-----------+-----------+
2 rows in set (0.01 sec)
```
下表显示了可用于在WHERE子句中形成过滤表达式的比较运算符。
```
操作者	描述
=	等于。几乎可以在任何数据类型中使用它。
<>或！=	不等于。
<	少于。您通常将它与数字和日期/时间数据类型一起使用。
>	大于
<=	小于或等于
> =	大于或等于
```
以下查询使用不等于（<>）运算符来查找不是销售代表的所有员工：

```sql
SELECT
    lastname,
    firstname,
    jobtitle
FROM
    employees
WHERE
    jobtitle <> 'Sales Rep'; 
```
运行结果：

```sql
+-----------+-----------+----------------------+
| lastname  | firstname | jobtitle             |
+-----------+-----------+----------------------+
| Murphy    | Diane     | President            |
| Patterson | Mary      | VP Sales             |
| Firrelli  | Jeff      | VP Marketing         |
| Patterson | William   | Sales Manager (APAC) |
| Bondur    | Gerard    | Sale Manager (EMEA)  |
| Bow       | Anthony   | Sales Manager (NA)   |
+-----------+-----------+----------------------+
6 rows in set (0.00 sec)
```
以下查询查找办公室代码大于5的员工：

```sql
SELECT
	lastname,
	firstname,
	officeCode 
FROM
	employees 
WHERE
	officecode > 5; 
```
运行结果：

```sql
+-----------+-----------+------------+
| lastname  | firstname | officeCode |
+-----------+-----------+------------+
| Patterson | William   | 6          |
| Bott      | Larry     | 7          |
| Jones     | Barry     | 7          |
| Fixter    | Andy      | 6          |
| Marsh     | Peter     | 6          |
| King      | Tom       | 6          |
+-----------+-----------+------------+
6 rows in set (0.00 sec)
```
以下查询返回办公室代码小于或等于4（<= 4）的员工：

```sql
SELECT
	lastname,
	firstname,
	officeCode 
FROM
	employees 
WHERE
	officecode <= 4; 
```
运行结果：

```sql
+-----------+-----------+------------+
| lastname  | firstname | officeCode |
+-----------+-----------+------------+
| Murphy    | Diane     | 1          |
| Patterson | Mary      | 1          |
| Firrelli  | Jeff      | 1          |
| Bondur    | Gerard    | 4          |
| Bow       | Anthony   | 1          |
| Jennings  | Leslie    | 1          |
| Thompson  | Leslie    | 1          |
| Firrelli  | Julie     | 2          |
| Patterson | Steve     | 2          |
| Tseng     | Foon Yue  | 3          |
| Vanauf    | George    | 3          |
| Bondur    | Loui      | 4          |
| Hernandez | Gerard    | 4          |
| Castillo  | Pamela    | 4          |
| Gerard    | Martin    | 4          |
+-----------+-----------+------------+
15 rows in set (0.00 sec)
```
有关MySQL WHERE子句的更多信息......
MySQL为您提供了一些其他运算符，可以在WHERE子句中使用复杂的形成搜索条件，例如：

 - BETWEEN 选择值范围内的值。
 - LIKE 匹配基于模式匹配的值。
 - IN 指定值是否与集合中的任何值匹配。
 - IS NULL检查值是否为NULL。
