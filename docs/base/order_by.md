MySQL ORDER BY子句对结果集进行排序。

MySQL ORDER BY子句介绍
因为在使用SELECT语句从表中查询数据时，结果集不按任何顺序排序。如果要对结果集进行排序，请使用该ORDER BY 子句。ORDER BY  允许的操作：

按单列或多列对结果集进行排序。
按升序或降序对不同列的结果集进行排序。
ORDER BY  子句的语法：

```sql
SELECT column1, column2,...
FROM tbl
ORDER BY column1 [ASC|DESC], column2 [ASC|DESC],... 
```
ASC代表升序 和 DESC代表下降。默认情况下，ORDER BY  如果未指定ASC或DESC 显式，则子句按升序对结果集进行排序  。

让我们练习一些使用ORDER BY 子句的例子。

MySQL ORDER BY 实例
请参阅示例数据库中的下customers表。

```sql
+------------------------+
| customers              |
+------------------------+
| customerNumber         |
| customerName           |
| contactLastName        |
| contactFirstName       |
| phone                  |
| addressLine1           |
| addressLine2           |
| city                   |
| state                  |
| postalCode             |
| country                |
| salesRepEmployeeNumber |
| creditLimit            |
+------------------------+
13 rows in set (0.00 sec)
```
以下查询从customers表中选择联系人，并按姓氏按升序对联系人进行排序。

```sql
SELECT
 contactLastname,
 contactFirstname
FROM
 customers
ORDER BY
 contactLastname; 
```
查询结果：

```sql
+-----------------+------------------+
| contactLastname | contactFirstname |
+-----------------+------------------+
| Accorti         | Paolo            |
| Altagar,G M     | Raanan           |
| Andersen        | Mel              |
| Anton           | Carmen           |
| Ashworth        | Rachel           |
...
```
如果要按降序按姓氏对联系人进行排序，请在ORDER BY子句中将contactLastname列指定为DESC以下查询：

```sql
SELECT
 contactLastname,
 contactFirstname
FROM
 customers
ORDER BY
 contactLastname DESC; 
```
运行结果：

```sql
+-----------------+------------------+
| contactLastname | contactFirstname |
+-----------------+------------------+
| Young           | Jeff             |
| Young           | Julie            |
| Young           | Mary             |
| Young           | Dorothy          |
| Yoshido         | Juri             |
| Walker          | Brydey           |
| Victorino       | Wendy            |
...
```
如果要按姓氏降序排序联系人，按升序排序第一个名称，请在相应列中指定两者   DESC，ASC如下所示：

```sql
SELECT
 contactLastname,
 contactFirstname
FROM
 customers
ORDER BY
 contactLastname DESC,
 contactFirstname ASC; 
```
运行结果：

```sql
+-----------------+------------------+
| contactLastname | contactFirstname |
+-----------------+------------------+
| Young           | Dorothy          |
| Young           | Jeff             |
| Young           | Julie            |
| Young           | Mary             |
| Yoshido         | Juri             |
| Walker          | Brydey           |
...
```
在上面的查询中，ORDER BY 子句首先按降序对结果集进行排序，然后按升序对第一个名称的排序结果进行排序，以生成最终结果集。

MySQL ORDER BY按表达式排序实例
ORDER BY 子句还允许您根据表达式对结果集进行排序。

请参   orderdetails见下表。
```sql
+-----------------+
| orderdetails    |
+-----------------+
| orderNumber     |
| productCode     |
| quantityOrdered |
| priceEach       |
| orderLineNumber |
+-----------------+
5 rows in set (0.02 sec)
```
以下查询从orderdetails表中选择订单行项目。它计算每个行项目的小计，并根据订单号，订单行号和小计对结果集进行排序。

```sql
SELECT
 ordernumber,
 orderlinenumber,
 quantityOrdered * priceEach
FROM
 orderdetails
ORDER BY
 ordernumber,
 orderLineNumber,
 quantityOrdered * priceEach; 
 ```
运行结果：

```sql
+-------------+-----------------+-----------------------------+
| ordernumber | orderlinenumber | quantityOrdered * priceEach |
+-------------+-----------------+-----------------------------+
|       10100 |               1 |                     1729.21 |
|       10100 |               2 |                     2754.50 |
|       10100 |               3 |                     4080.00 |
|       10100 |               4 |                     1660.12 |
|       10101 |               1 |                     4343.56 |
|       10101 |               2 |                     2040.10 |
|       10101 |               3 |                     1463.85 |
|       10101 |               4 |                     2701.50 |
...
```
如果要让查询更具可读性，我们可以使用列别名 ,拼按列别名排序，以下把小计列命名为“subtotal”，如下所示：

```sql
SELECT
 ordernumber,
 orderlinenumber,
 quantityOrdered * priceEach AS subtotal
FROM
 orderdetails
ORDER BY
 ordernumber,
 orderLineNumber,
 subtotal; 
```
运行结果：

```sql
+-------------+-----------------+----------+
| ordernumber | orderlinenumber | subtotal |
+-------------+-----------------+----------+
|       10100 |               1 |  1729.21 |
|       10100 |               2 |  2754.50 |
|       10100 |               3 |  4080.00 |
|       10100 |               4 |  1660.12 |
|       10101 |               1 |  4343.56 |
|       10101 |               2 |  2040.10 |
|       10101 |               3 |  1463.85 |
...
```
我们subtotal 作为表达式 quantityOrdered * priceEach 的列别名 ，并根据subtotal别名对结果集进行排序。

MySQL ORDER BY 使用自定义排序顺序
ORDER BY 子句可以使用FIELD()  函数为列中的值定义自己的自定义排序顺序。

请参orders见下表。

```sql
+----------------+
| orders         |
+----------------+
| orderNumber    |
| orderDate      |
| requiredDate   |
| shippedDate    |
| status         |
| comments       |
| customerNumber |
+----------------+
7 rows in set (0.03 sec)
```
例如，如果要按以下顺序基于以下状态对订单进行排序：
```sql
In Process
On Hold
Canceled
Resolved
Disputed
Shipped
```
您可以使用 FIELD 函数将这些值映射到数值列表，并使用这些数字进行排序;

请参阅以下查询：

```sql
SELECT
	orderNumber, status
FROM
	orders
ORDER BY FIELD(status,
	'In Process',
	'On Hold',
	'Cancelled',
	'Resolved',
	'Disputed','Shipped'); 
```
运行结果：

```sql
+-------------+------------+
| orderNumber | status     |
+-------------+------------+
|       10420 | In Process |
|       10421 | In Process |
|       10422 | In Process |
|       10423 | In Process |
|       10424 | In Process |
|       10425 | In Process |
|       10334 | On Hold    |
|       10401 | On Hold    |
...
```