MySQL BETWEEN运算符来确定值是否在值范围内。

MySQL BETWEEN 运算符简介
BETWEEN运算符是一个逻辑运算符，指定是否在某个范围内的值是。BETWEEN运算符通常用于SELECT，UPDATE和DELETE语句的WHERE子句中。

BETWEEN运算符的语法：

```sql
expr [NOT] BETWEEN begin_expr AND end_expr; 
```
expr 是在   begin_expr 和   end_expr定义的范围内测试的表达式。三个表达式：   expr，  begin_expr和   end_expr 必须具有相同的数据类型。

的BETWEEN操作者如果的值返回真   expr 大于或等于（> =）的值   begin_expr ，且小于或等于的（<=）的值   end_expr，否则它返回零。

所述NOT BETWEEN返回true，如果值   expr 小于（<）的值   begin_expr 或比值的值大   end_expr，否则返回0。

如果有任何表达式NULL，则BETWEEN运算符返回   NULL 。

如果要指定独占范围，可以使用大于（>）和小于（<）运算符。

MySQL BETWEEN 运算符实例
让我们练习一些使用BETWEEN运算符的例子。

1）使用MySQL BETWEEN和数字示例
请参阅示例数据库中的下products表：

```sql
+--------------------+
| products           |
+--------------------+
| productCode        |
| productName        |
| productLine        |
| productScale       |
| productVendor      |
| productDescription |
| quantityInStock    |
| buyPrice           |
| MSRP               |
+--------------------+
9 rows in set (0.02 sec)
```
以下示例使用BETWEEN运算符查找购买价格介于90和之间的产品  100：

```sql
SELECT 
    productCode, 
    productName, 
    buyPrice
FROM
    products
WHERE
    buyPrice BETWEEN 90 AND 100; 
```
运行结果：

```sql
+-------------+--------------------------------------+----------+
| productCode | productName                          | buyPrice |
+-------------+--------------------------------------+----------+
| S10_1949    | 1952 Alpine Renault 1300             |    98.58 |
| S10_4698    | 2003 Harley-Davidson Eagle Drag Bike |    91.02 |
| S12_1099    | 1968 Ford Mustang                    |    95.34 |
| S12_1108    | 2001 Ferrari Enzo                    |    95.59 |
| S18_1984    | 1995 Honda Civic                     |    93.89 |
| S18_4027    | 1970 Triumph Spitfire                |    91.92 |
| S24_3856    | 1956 Porsche 356A Coupe              |    98.30 |
+-------------+--------------------------------------+----------+
7 rows in set (0.01 sec)
```
此查询使用大于或等于(>=）和小于或等于（<=）运算符而不是BETWEEN运算符来获得相同的结果：

```sql
SELECT 
    productCode, 
    productName, 
    buyPrice
FROM
    products
WHERE
    buyPrice >= 90 AND buyPrice <= 100; 
```
要查找购买价格不在20美元到100美元之间的产品，您可以将BETWEEN运行符与NOT运算符合并使用，如下所示：

```sql
SELECT 
    productCode, 
    productName, 
    buyPrice
FROM
    products
WHERE
    buyPrice NOT BETWEEN 20 AND 100; 
```
运行结果：

```
+-------------+-------------------------------------+----------+
| productCode | productName                         | buyPrice |
+-------------+-------------------------------------+----------+
| S10_4962    | 1962 LanciaA Delta 16V              |   103.42 |
| S18_2238    | 1998 Chrysler Plymouth Prowler      |   101.51 |
| S24_2840    | 1958 Chevy Corvette Limited Edition |    15.91 |
| S24_2972    | 1982 Lamborghini Diablo             |    16.24 |
+-------------+-------------------------------------+----------+
4 rows in set (0.00 sec)
```
可以使用小于（>），大于（>）和逻辑运算符（AND）重写上面的查询，如下面的查询：

```sql
SELECT 
    productCode, 
    productName, 
    buyPrice
FROM
    products
WHERE
    buyPrice < 20 OR buyPrice > 100; 
```
2）使用MySQL BETWEEN和日期实例
当您使用BETWEEN带有日期值的运算符时，为了获得最佳结果，您应使用类型转换将列或表达式的类型显式转换为DATE类型。

以下示例返回在01/01/2003到01/31/2003之间具有所需日期的订单：

```sql
SELECT 
   orderNumber,
   requiredDate,
   status
FROM 
   orders
WHERE 
   requireddate BETWEEN 
     CAST('2013-01-01' AS DATE) AND 
     CAST('2013-01-31' AS DATE); 
```
运行结果：

```
+-------------+--------------+---------+
| orderNumber | requiredDate | status  |
+-------------+--------------+---------+
|       10100 | 2013-01-13   | Shipped |
|       10101 | 2013-01-18   | Shipped |
|       10102 | 2013-01-18   | Shipped |
+-------------+--------------+---------+
3 rows in set (0.00 sec)
```
因为所需日期列的数据类型是DATE的，所以我们使用CAST运算符将文字字符串'2013-01-01'和'2013-12-31'转换为DATE值。

