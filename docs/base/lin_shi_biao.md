MySQL临时表简介
在MySQL中，临时表是一种特殊类型的表，允许您存储临时结果集，您可以在单个会话中多次重复使用。

当查询需要带有JOIN子句的单个SELECT语句的数据是不可能或非常耗时，临时表非常方便。在这种情况下，您可以使用临时表来存储立即结果，并使用另一个查询来处理它。

MySQL临时表具有以下特殊功能：

使用CREATE TEMPORARY TABLE创建临时表。请注意，TEMPORARY关键字是在CREATE和TABLE关键字之间添加的  。
当会话结束或连接终止时，MySQL会自动删除临时表。当然，您可以使用   DROP TABLE语句在不再使用它时显式删除临时表。
临时表仅可供创建它的客户端访问。不同的客户端可以创建具有相同名称的临时表而不会导致错误，因为只有创建临时表的客户端才能看到它。但是，在同一会话中，两个临时表不能共享相同的名称。
临时表可以与数据库中的普通表具有相同的名称。例如，如果创建一个名为employees临时表为示例数据库，现有的employees表变得不可访问。您针对employees表发出的每个查询现在都引用临时  employees 表。删除employees临时表时，employees表可用并可再次访问。
即使临时表可以与普通表具有相同的名称，也不建议使用。因为这可能会导致混淆并可能导致意外的数据丢失。
例如，如果与数据库服务器的连接丢失并且您自动重新连接到服务器，则无法区分临时表和普通表。然后，您可以发出一个DROP TABLE  语句来删除实体表而不是临时表，这不是期望的结果。

创建MySQL临时表
要创建临时表，只需将TEMPORARY关键字添加到CREATE TABLE语句中即可。例如，以下语句创建一个临时表，按收入存储前10个客户：
```
CREATE TEMPORARY TABLE top10customers
SELECT p.customerNumber, 
       c.customerName, 
       ROUND(SUM(p.amount),2) sales
FROM payments p
INNER JOIN customers c ON c.customerNumber = p.customerNumber
GROUP BY p.customerNumber
ORDER BY sales DESC
LIMIT 10; 
```
现在，您可以从top10customers临时表中查询数据就像从普通表中查询：
```
SELECT 
    customerNumber, 
    customerName, 
    sales
FROM
    top10customers
ORDER BY sales; 
+----------------+------------------------------+-----------+
| customerNumber | customerName                 | sales     |
+----------------+------------------------------+-----------+
|            146 | Saveley & Henriot, Co.       | 130305.35 |
|            321 | Corporate Gift Ideas Co.     | 132340.78 |
|            276 | Anna's Decorations, Ltd      | 137034.22 |
|            187 | AV Stores, Co.               | 148410.09 |
|            323 | Down Under Souveniers, Inc   | 154622.08 |
|            148 | Dragon Souveniers, Ltd.      | 156251.03 |
|            151 | Muscle Machine Inc           | 177913.95 |
|            114 | Australian Collectors, Co.   | 180585.07 |
|            124 | Mini Gifts Distributors Ltd. | 584188.24 |
|            141 | Euro+ Shopping Channel       | 715738.98 |
+----------------+------------------------------+-----------+
10 rows in set (0.00 sec)
```
删除MySQL临时表
您可以使用DROP TABLE语句删除临时表，但最好添加TEMPORARY关键字，如下所示：
```
DROP TEMPORARY TABLE table_name; 
```
DROP TEMPORARY TABLE  语句仅删除临时表，而不是普通表。当您将临时表命名为普通表的名称时，它可以帮助您避免删除实普通的错误

例如，要删除top10customers临时表，请使用以下语句：
```
DROP TEMPORARY TABLE top10customers; 
```
注意：如果您尝试使用DROP TEMPORARY TABLE语句删除普通表，您将收到一条错误消息，指出您正在尝试删除的表未知。

如果您开发使用连接池或持久连接的应用程序，则无法保证在应用程序终止时自动删除临时表。

因为应用程序使用的数据库连接可能仍处于打开状态并放置在连接池中供其他客户端使用。因此，最好在不再使用临时表时始终删除它们。