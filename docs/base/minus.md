MySQL MINUS运算符。

SQL MINUS运算符简介
MINUS在包含SQL标准的三个操作之一UNION，INTERSECT和MINUS。

MINUS 比较两个查询的结果，并返回第一个查询中不是由第二个查询输出的不同行。

以下说明了MINUS运算符的语法：
```sql
SELECT column_list_1 FROM table_1
MINUS 
SELECT columns_list_2 FROM table_2; 
```
使用MINUS运算符的查询的基本规则如下：

数量和两列的顺序column_list_1和column_list_2必须相同。
两个查询中相应列的数据类型必须兼容。
假设我们有两个表t1并t2具有以下结构和数据：

```sql
CREATE TABLE t1 (
    id INT PRIMARY KEY
);
 
CREATE TABLE t2 (
    id INT PRIMARY KEY
);
 
INSERT INTO t1 VALUES (1),(2),(3);
INSERT INTO t2 VALUES (2),(3),(4); 
```
以下查询返回t1表的查询中的不同值，这些值在表的查询结果中找不到t2。

```sql
SELECT id FROM t1
MINUS
SELECT id FROM t2; 
```
MySQL MINUS示例

以下是MINUS图说明：

MySQL MINUS操作员插图

注意：某些数据库系统（例如Microsoft SQL Server，PostgreSQL等）使用EXCEPT而不是MINUS具有相同功能的数据库系统。

MySQL MINUS运算符
不幸的是，MySQL不支持MINUS运营商。但是，您可以使用MySQL JOIN来模拟它。

要模拟MINUS两个查询，请使用以下语法：

```sql
SELECT 
    column_list 
FROM 
    table_1
    LEFT JOIN table_2 ON join_predicate
WHERE 
    table_2.id IS NULL; 
```
例如，以下查询使用LEFT JOIN子句返回与MINUS运算符相同的结果：
```
SELECT 
    id
FROM
    t1
        LEFT JOIN
    t2 USING (id)
WHERE
    t2.id IS NULL; 
```