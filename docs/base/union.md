MySQL UNION运算符将多个SELECT语句中的两个或多个结果集合并到一个结果集中。

MySQL UNION运算符
MySQL UNION运算符允许您将两个或多个查询结果集合并到一个结果集中。以下说明了UNION运算符的语法：

```sql
SELECT column_list
UNION [DISTINCT | ALL]
SELECT column_list
UNION [DISTINCT | ALL]
SELECT column_list
... 
```
要使用UNION运算符组合两个或多个查询的结果集，必须遵循以下基本规则：

首先，所有SELECT语句中出现的列的数量和顺序  必须相同。
其次，列的数据类型必须相同或可转换。
默认情况下， 即使您未明确指定DISTINCT运算符，UNION运算符也会删除 重复的行。

让我们看看以下示例表：t1和t2：

```sql
DROP TABLE IF EXISTS t1;
DROP TABLE IF EXISTS t2;
 
CREATE TABLE t1 (
    id INT PRIMARY KEY
);
 
CREATE TABLE t2 (
    id INT PRIMARY KEY
);
 
INSERT INTO t1 VALUES (1),(2),(3);
INSERT INTO t2 VALUES (2),(3),(4); 
```
以下语句组合了从表t1和t2表返回的结果集：
```
SELECT id
FROM t1
UNION
SELECT id
FROM t2; 
```
最终结果集包含查询返回的单独结果集的不同值：
```
+----+
| id |
+----+
|  1 |
|  2 |
|  3 |
|  4 |
+----+
4 rows in set (0.00 sec) 
```
因为值为2和3的行是重复的，所以UNION操作员将其删除并仅保留不同的行。

以下维基中说明了来自t1和t2表的两个结果集的并集：

MySQL UNION

如果使用UNION ALL，则重复行（如果可用）将保留在结果中。因为UNION ALL不需要处理重复项，所以它的执行速度比  UNION DISTINCT 快。
```
SELECT id
FROM t1
UNION ALL
SELECT id
FROM t2; 
运行结果：

+----+
| id |
+----+
|  1 |
|  2 |
|  3 |
|  2 |
|  3 |
|  4 |
+----+
6 rows in set (0.00 sec) 
```
如您所见，由于UNION ALL操作，重复项出现在组合结果集中  。

UNION vs. JOIN
JOIN 是水平组合结果集，  UNION是垂直附加结果集。下图说明了UNION和之间的区别JOIN：



MySQL UNION和列别名实例
我们将使用示例数据库中的customers和employees表 进行演示：
```
+----------------+
| employees      |
+----------------+
| employeeNumber |
| lastName       |
| firstName      |
| extension      |
| email          |
| officeCode     |
| reportsTo      |
| jobTitle       |
+----------------+
8 rows in set (0.01 sec) 

+------------------------+
| customers              |
+------------------------+
| customerNumber         |
| customerName           |
| contactLastName        |
| contactFirstName       |
| phone                  |
| addressLine1           |
| addressLine2           |
| city                   |
| state                  |
| postalCode             |
| country                |
| salesRepEmployeeNumber |
| creditLimit            |
+------------------------+
13 rows in set (0.01 sec)
```
假设您要将员工和客户的名字和姓氏组合到一个结果集中，您可以按如下方式使用UNION 运算符：

```sql
SELECT 
    firstName, 
    lastName
FROM
    employees 
UNION 
SELECT 
    contactFirstName, 
    contactLastName
FROM
    customers; 
```
运行结果：
```
+------------+-------------+
| firstName  | lastName    |
+------------+-------------+
| Diane      | Murphy      |
| Mary       | Patterson   |
| Jeff       | Firrelli    |
| William    | Patterson   |
| Gerard     | Bondur      |
...
```
如您所见，MySQL UNION运算符使用第一个SELECT语句的列名来标记输出中的列。

如果要使用自己的列别名，则需要在第一个SELECT语句中显式指定它们，如以下示例所示：

```sql
SELECT 
    concat(firstName,' ',lastName) fullname
FROM
    employees 
UNION SELECT 
    concat(contactFirstName,' ',contactLastName)
FROM
    customers; 
```
在此示例中，我们使用列别名fullname来标记输出，而不是使用第一个查询中的默认列标签   。

MySQL UNION和ORDER BY
如果要对union的结果进行排序，请ORDER BY在最后一个SELECT语句中使用一个子句，  如以下示例所示：

```sql
SELECT 
    concat(firstName,' ',lastName) fullname
FROM
    employees 
UNION SELECT 
    concat(contactFirstName,' ',contactLastName)
FROM
    customers
ORDER BY fullname; 
```
运行结果：
```
+----------------------+
| fullname             |
+----------------------+
| Adrian Huxley        |
| Akiko Shimamura      |
| Allen Nelson         |
| Andy Fixter          |
| Ann  Brown           |
| Anna O'Hara          |
...
```
注意：如果将ORDER BY子句放在每个SELECT语句中，则不会影响最终结果集中行的顺序。

MySQL还为您提供了基于列位置使用ORDER BY子句对结果集进行排序的替代选项，如下所示：

```sql
SELECT 
    concat(firstName,' ',lastName) fullname
FROM
    employees 
UNION SELECT 
    concat(contactFirstName,' ',contactLastName)
FROM
    customers
ORDER BY 1; 
```
ORDER BY 后面使用的数字代着SELECT 字段的顺序序号，从1开始（上面实例中的1代表 fullname 字段）