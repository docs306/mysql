MySQL锁定来在会话之间进行协作访问表。

锁是与表关联的标志。MySQL允许客户端会话显式获取表锁，以防止其他会话在特定时间段内访问同一个表。客户端会话只能为自己获取或释放表锁。它无法获取或释放其他会话的表锁。

在详细介绍之前，我们创建了一个tbl的表练习表锁定语句。

```sql
CREATE TABLE tbl (
  id INT NOT NULL AUTO_INCREMENT,
  col INT NOT NULL,
  PRIMARY KEY (id)
) Engine = InnoDB; 
```
LOCK和UNLOCK TABLES语法
以下语句显式获取表锁：
```
LOCK TABLES table_name [READ | WRITE] 
```
要锁定表，请在LOCK TABLES关键字后指定其名称。此外，您可以指定锁的类型，可以是   READ或  WRITE。

要释放表的锁，请使用以下语句：
```sql
UNLOCK TABLES; 
```
读锁
READ锁具有以下特点：

- READ可以通过多个会话同时获取表的锁。此外，其他会话可以从表中读取数据而无需获取锁。
- 持有READ锁的会话只能读取表中的数据，但无法写入。此外，在READ释放锁之前，其他会话无法将数据写入表。来自另一个会话的写入操作将进入等待状态，直到READ锁定被释放。
- 如果会话正常或异常终止，MySQL将隐式释放所有锁。此功能也与WRITE锁相关。
我们来看看READ锁在以下场景中的工作原理。

在第一个会话中，首先，连接到mysqldemo数据库并使用CONNECTION_ID()函数获取当前连接ID，如下所示：
```sql
SELECT CONNECTION_ID(); 
+-----------------+
| CONNECTION_ID() |
+-----------------+
|            4892 |
+-----------------+
1 row in set (0.00 sec)
```
然后，在tbl表中插入一个新行。
```
INSERT INTO tbl(col) VALUES(10); 
```
接下来，查询tbl表中的数据。
```
SELECT * FROM tbl; 
+----+-----+
| id | col |
+----+-----+
|  1 |  10 |
+----+-----+
1 row in set (0.00 sec)
```
之后，要获取锁定，请使用LOCK TABLE语句。
```
LOCK TABLE tbl READ; 
```
最后，在同一个会话中，如果您尝试在tbl表中插入新行，则会收到错误消息。
```
INSERT INTO tbl(col) VALUES(11); 
ERROR 1099 (HY000): Table 'tbl' was locked with a READ lock and can't be updated
```
因此，一旦READ获取了锁，就无法在同一会话中将数据写入表中。

让我们READ从另一个会话中检查锁定。

首先，连接到mysqldemo并检查连接ID：
```
SELECT CONNECTION_ID(); 
+-----------------+
| CONNECTION_ID() |
+-----------------+
|            4896 |
+-----------------+
1 row in set (0.00 sec)
```
然后，从tbl  表中检索数据：
```
SELECT * FROM tbl; 
+----+-----+
| id | col |
+----+-----+
|  1 |  10 |
+----+-----+
1 row in set (0.00 sec)
```
接下来，在tbl表中插入一个新行：
```
INSERT INTO tbl(col) VALUES(20); 
mysql> INSERT INTO tbl(col) VALUES(20);
...
```
来自第二个会话的插入操作处于等待状态，因为第一个会话READ已经在tbl表上获取了锁，但尚未释放。

您可以从SHOW PROCESSLIST查看详细信息。
```
SHOW PROCESSLIST; 
mysql> SHOW PROCESSLIST;
+------+------+-----------------+-----------+---------+------+------------------------------+---------------------------------+
| Id   | User | Host            | db        | Command | Time | State                        | Info                            |
+------+------+-----------------+-----------+---------+------+------------------------------+---------------------------------+
| 4884 | root | localhost:64108 | mysqldemo | Sleep   | 1479 |                              | NULL                            |
| 4892 | root | localhost       | mysqldemo | Query   |    0 | init                         | SHOW PROCESSLIST                |
| 4896 | root | localhost       | mysqldemo | Query   |   97 | Waiting for table level lock | INSERT INTO tbl(col) VALUES(20) |
+------+------+-----------------+-----------+---------+------+------------------------------+---------------------------------+
3 rows in set (0.00 sec)
```
之后，返回第一个会话并使用UNLOCK TABLES语句释放锁定。READ从第一个会话释放锁定后，INSERT执行第二个会话中的操作。
```
unlock tables;
``` 
最后，检查tbl表的数据，看看INSERT第二个会话的操作是否真的执行了。
```
SELECT * FROM tbl; 
mysql> SELECT * FROM tbl;
+----+-----+
| id | col |
+----+-----+
|  1 |  10 |
|  2 |  20 |
+----+-----+
2 rows in set (0.00 sec)
```

写锁
WRITE 锁具有以下特点：

- 保存表锁的唯一会话可以从表中读取和写入数据。
- 在WRITE锁定释放之前，其他会话无法从表中读取数据并将数据写入表中。
让我们详细了解WRITE锁是如何工作的。

首先，WRITE从第一个会话中获取锁定。
```
LOCK TABLE tbl WRITE; 
```
然后，在tbl表中插入一个新行。
```
INSERT INTO tbl(col) VALUES(11); 
```
有用。

接下来，从tbl表中读取数据。
```
SELECT * FROM tbl; 
+----+-----+
| id | col |
+----+-----+
|  1 |  10 |
|  2 |  20 |
|  3 |  11 |
+----+-----+
3 rows in set (0.00 sec)
```
它也有效。

之后，从第二个会话开始，尝试写入和读取数据：
```
INSERT INTO tbl(col) VALUES(21);
 
SELECT * FROM tbl; 
```
MySQL将这些操作置于等待状态。您可以使用SHOW PROCESSLIST语句进行检查。
```
SHOW PROCESSLIST 
+------+------+-----------------+-----------+---------+------+---------------------------------+---------------------------------+
| Id   | User | Host            | db        | Command | Time | State                           | Info                            |
+------+------+-----------------+-----------+---------+------+---------------------------------+---------------------------------+
| 4884 | root | localhost:64108 | mysqldemo | Sleep   | 2105 |                                 | NULL                            |
| 4892 | root | localhost       | mysqldemo | Query   |    0 | init                            | SHOW PROCESSLIST                |
| 4896 | root | localhost       | mysqldemo | Query   |   27 | Waiting for table metadata lock | INSERT INTO tbl(col) VALUES(21) |
+------+------+-----------------+-----------+---------+------+---------------------------------+---------------------------------+
```
最后，从第一个会话中释放锁定。
```
UNLOCK TABLES; 
```
您将看到第二个会话中的所有待处理操作都已执行，下图说明了结果：
```
+----+-----+
| id | col |
+----+-----+
|  1 |  10 |
|  2 |  20 |
|  3 |  11 |
|  4 |  21 |
+----+-----+
4 rows in set (0.00 sec)
```
读锁与写锁
- 读锁是“共享”锁，它可以防止正在获取写锁，但不能锁定其他读锁。
- 写锁是“独占”锁，可以防止任何其他类型的锁。