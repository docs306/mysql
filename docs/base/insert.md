MySQL INSERT语句向表中添加一行或多行。

MySQL INSERT 语句简介
INSERT语句允许您将一行或多行插入表中。以下说明了INSERT语句的语法：

```sql
INSERT INTO table(c1,c2,...)
VALUES (v1,v2,...); 
```
在这个语法中，

首先，在INSERT INTO子句后面的括号内指定表名和逗号分隔列的列表。
然后，在VALUES关键字后面的括号内放置以逗号分隔的相应列值列表。
列数和值必须相同。此外，列的位置必须与其值的位置相对应。

要使用单个INSERT语句将多行添加到表中，请使用以下语法：

```sql
INSERT INTO table(c1,c2,...)
VALUES 
   (v11,v12,...),
   (v21,v22,...),
    ...
   (vnn,vn2,...); 
```
在此语法中，行在VALUES子句中用逗号分隔。

MySQL INSERT 实例
让我们创建一个的新表 命名为 tasks 来练习INSERT语句。

```sql
CREATE TABLE IF NOT EXISTS tasks (
    task_id INT AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    start_date DATE,
    due_date DATE,
    priority TINYINT NOT NULL DEFAULT 3,
    description TEXT,
    PRIMARY KEY (task_id)
); 
```
使用简单的INSERT语句实例
以下语句向tasks表中添加了一个新行：

```sql
INSERT INTO 
 tasks(title,priority)
VALUES
 ('Learn MySQL INSERT Statement',1); 
```
执行语句后，MySQL返回以下消息：

Query OK, 1 row affected (0.01 sec)
这意味着已成功将一行插入tasks表中。

您可以使用以下查询进行验证：

```sql
SELECT 
    *
FROM
    tasks; 
```
这是输出：

```sql
+---------+------------------------------+------------+----------+----------+-------------+
| task_id | title                        | start_date | due_date | priority | description |
+---------+------------------------------+------------+----------+----------+-------------+
|       1 | Learn MySQL INSERT Statement | NULL       | NULL     |        1 | NULL        |
+---------+------------------------------+------------+----------+----------+-------------+
1 row in set (0.00 sec)
```
在此示例中，我们仅为列title和priority列指定了值。对于其他列，MySQL使用默认值。

task_id列是自动增量列。这意味着只要将一行添加到表中，MySQL就会生成一个顺序整数。

start_date，due_date和description列使用NULL作为默认值，因此，MySQL使用NULL插入到这些列，如果你没有在指定其值 在INSERT的语句中。

使用默认值插入行
如果要在列中插入默认值，可以使用以下两种方法：

首先，在INSERT语句中忽略列名及其值。
其次，在INSERT INTO子句中指定列名，并在VALUES子句中使用DEFAULT关键字。
以下实例演示了第二种方式：

```sql
INSERT INTO 
 tasks(title,priority)
VALUES
 ('Understanding DEFAULT keyword in INSERT statement',DEFAULT); 
```
在此示例中，我们指定了优先级列和   DEFAULT关键字。

因为priority列的默认值是3，如表定义中声明的那样：

```
priority TINYINT NOT NULL DEFAULT 3 
```
MySQL使用数字3插入priority列中。

以下显示tasks插入后表的内容：

```sql
SELECT 
    *
FROM
    tasks; 
```
运行结果：

```
+---------+---------------------------------------------------+------------+----------+----------+-------------+
| task_id | title                                             | start_date | due_date | priority | description |
+---------+---------------------------------------------------+------------+----------+----------+-------------+
|       1 | Learn MySQL INSERT Statement                      | NULL       | NULL     |        1 | NULL        |
|       2 | Understanding DEFAULT keyword in INSERT statement | NULL       | NULL     |        3 | NULL        |
+---------+---------------------------------------------------+------------+----------+----------+-------------+
2 rows in set (0.00 sec)
```
在表格中插入日期
要将文本日期值插入列，请使用以下格式：

'YYYY-MM-DD' 
采用以下格式：

YYYY表示四位数年份，例如2018年。
MM表示两位数的月份，例如01,02和12。
DD表示两位数的日期，例如01,02,30。
以下示例tasks使用开始日期和到期日期值向表中添加新行：

```
INSERT INTO tasks(title, start_date, due_date)
VALUES('Insert date into table','2018-01-09','2018-09-15'); 
```
下图显示tasks插入后表格的内容：
```
+---------+---------------------------------------------------+------------+------------+----------+-------------+
| task_id | title                                             | start_date | due_date   | priority | description |
+---------+---------------------------------------------------+------------+------------+----------+-------------+
|       1 | Learn MySQL INSERT Statement                      | NULL       | NULL       |        1 | NULL        |
|       2 | Understanding DEFAULT keyword in INSERT statement | NULL       | NULL       |        3 | NULL        |
|       3 | Insert date into table                            | 2018-01-09 | 2018-09-15 |        3 | NULL        |
+---------+---------------------------------------------------+------------+------------+----------+-------------+
3 rows in set (0.00 sec)
```
可以在VALUES子句中使用表达式。例如，以下语句使用开始日期和截止日期列的当前日期添加新任务：

```sql
INSERT INTO tasks(title,start_date,due_date)
VALUES
 ('Use current date for the task',CURRENT_DATE(),CURRENT_DATE()); 
```
在这个例子中，我们使用CURRENT_DATE()函数作为start_date和due_date列的值。请注意，CURRENT_DATE()函数是一个返回当前系统日期的日期函数。

以下是tasks插入后表的内容：
```
+---------+---------------------------------------------------+------------+------------+----------+-------------+
| task_id | title                                             | start_date | due_date   | priority | description |
+---------+---------------------------------------------------+------------+------------+----------+-------------+
|       1 | Learn MySQL INSERT Statement                      | NULL       | NULL       |        1 | NULL        |
|       2 | Understanding DEFAULT keyword in INSERT statement | NULL       | NULL       |        3 | NULL        |
|       3 | Insert date into table                            | 2018-01-09 | 2018-09-15 |        3 | NULL        |
|       4 | Use current date for the task                     | 2019-08-08 | 2019-08-08 |        3 | NULL        |
+---------+---------------------------------------------------+------------+------------+----------+-------------+
4 rows in set (0.00 sec)
```
插入多行示例
以下语句向tasks表中添加了三行：

```
INSERT INTO tasks(title, priority)
VALUES
 ('My first task', 1),
 ('It is the second task',2),
 ('This is the third task of the week',3); 
```
在此示例中，每个行数据都被指定为VALUES子句中的值列表。

MySQL返回以下消息：
```
Query OK, 3 rows affected (0.00 sec)
Records: 3  Duplicates: 0  Warnings: 0
```
这意味着已成功插入三行，没有重复或警告。

tasks表具有后插入以下数据：
```
+---------+---------------------------------------------------+------------+------------+----------+-------------+
| task_id | title                                             | start_date | due_date   | priority | description |
+---------+---------------------------------------------------+------------+------------+----------+-------------+
|       1 | Learn MySQL INSERT Statement                      | NULL       | NULL       |        1 | NULL        |
|       2 | Understanding DEFAULT keyword in INSERT statement | NULL       | NULL       |        3 | NULL        |
|       3 | Insert date into table                            | 2018-01-09 | 2018-09-15 |        3 | NULL        |
|       4 | Use current date for the task                     | 2019-08-08 | 2019-08-08 |        3 | NULL        |
|       5 | My first task                                     | NULL       | NULL       |        1 | NULL        |
|       6 | It is the second task                             | NULL       | NULL       |        2 | NULL        |
|       7 | This is the third task of the week                | NULL       | NULL       |        3 | NULL        |
+---------+---------------------------------------------------+------------+------------+----------+-------------+
7 rows in set (0.00 sec)
```