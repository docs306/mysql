MySQL中管理数据库。您将学习如何创建新数据库，删除现有数据库以及显示MySQL数据库服务器中的所有数据库。

让我们开始在MySQL中创建一个新的数据库。

创建数据库
在对数据执行任何其他操作之前，您需要创建一个数据库。数据库是数据的容器。它存储您可以想到的联系人，供应商，客户或任何类型的数据。

在MySQL中，数据库是一组对象，用于存储和操作表，数据库视图，触发器和  存储过程等数据。

要在MySQL中创建数据库，请使用以下CREATE DATABASE  语句：
```
CREATE DATABASE [IF NOT EXISTS] database_name; 
```
让我们 更详细地研究一下这个CREATE DATABASE：

 CREATE DATABASE  语句后跟是您要创建的数据库名称。建议数据库名称尽可能具有意义和描述性。
IF NOT EXISTS  是可选子句。IF NOT EXISTS子句可防止您创建数据库服务器中已存在的新数据库时出错。MySQL数据库服务器中不能有2个具有相同名称的数据库。
例如，要创建mysqldemo数据库，可以CREATE DATABASE  按如下方式执行语句：
```
CREATE DATABASE mysqldemo; 
```
执行此语句后，MySQL返回一条消息，通知是否已成功创建新数据库。

显示数据库
SHOW DATABASES语句列出了MySQL数据库服务器中的所有数据库。您可以使用SHOW DATABASES语句检查您创建的数据库，或者在创建新数据库之前查看数据库服务器上的所有数据库，例如：
```
SHOW DATABASES; 
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| mysqldemo          |
+--------------------+
4 rows in set (0.01 sec)
```
如输出中清楚所示，我们在MySQL数据库服务器中有三个数据库。information_schema  和mysql我们安装MySQL时可用的默认数据库，mysqldemo是我们创建的新数据库。

选择要使用的数据库
在使用特定数据库之前，必须使用USE  语句告诉MySQL您要使用哪个数据库。
```
USE database_name; 
```
您可以使用以下USE语句选择mysqldemo  示例数据库：
```
USE mysqldemo; 
```
从现在开始，所有操作（如查询数据，创建新表或调用您执行的存储过程）都将对当前数据库产生影响，即  mysqldemo。

删除数据库
删除数据库意味着永久删除数据库中包含的所有表和数据库本身。因此，执行此查询时要格外小心非常重要。

要删除数据库，请使用以下DROP DATABASE 语句：
```
DROP DATABASE [IF EXISTS] database_name; 
```
DROP DATABASE  子句后面是您要删除的数据库名称。与CREATE DATABASE  语句类似，IF EXISTS  语句的可选部分，用于防止您删除数据库服务器中不存在的数据库。

如果要练习DROP DATABASE  语句，可以创建一个新数据库，确保它已创建并删除它。

我们来看看以下查询：
```
CREATE DATABASE IF NOT EXISTS tempdb;
SHOW DATABASES;
DROP DATABASE IF EXISTS tempdb; 
```
三个陈述的顺序如下：

- 我们使用CREATE DATABASE语句创建了一个tempdb数据库。
- 我们使用SHOW DATABASES语句显示所有数据库。
- 我们使用DROP DATABASE语句删除tempdb。