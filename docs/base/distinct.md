MySQL DISTINCT 子句来消除结果集中的重复行。

MySQL DISTINCT子句简介
从表中查询数据时，可能会出现重复的行。要删除这些重复的行，在SELECT子句中请使用DISTINCT 语句。

使用DISTINCT 子句的语法如下：

```sql
SELECT DISTINCT
    columns
FROM
    table_name
WHERE
    where_conditions; 
```
MySQL的DISTINCT例子
让我们看一个使用DISTINCT 子句从employees表中选择员工唯一姓氏的简单实例。

```sql
+----------------+
| employees      |
+----------------+
| employeeNumber |
| lastName       |
| firstName      |
| extension      |
| email          |
| officeCode     |
| reportsTo      |
| jobTitle       |
+----------------+
8 rows in set (0.02 sec)
```
首先，employees使用SELECT语句从表中查询员工的姓氏：

```sql
SELECT
  lastname
FROM
  employees
ORDER BY lastname; 
```
运行结果：
```sql
+-----------+
| lastname  |
+-----------+
| Bondur    |
| Bondur    |
| Bott      |
| Bow       |
| Castillo  |
| Firrelli  |
| Firrelli  |
...
```
在结果中我们可以清楚的看到一些员工具有相同的姓氏，例如，  Bondur,Firrelli。

要删除重复的姓氏，请将DISTINCT 子句添加到SELECT 语句中，如下所示：

```sql
SELECT DISTINCT 
    lastname 
FROM 
    employees 
ORDER BY lastname; 
```
运行结果：

```sql
+-----------+
| lastname  |
+-----------+
| Bondur    |
| Bott      |
| Bow       |
| Castillo  |
| Firrelli  |
| Fixter    |
...
```
当我们使用DISTINCT 子句时，结果集中将删除重复的姓氏。

MySQL DISTINCT 和 NULL值
如果字段中有NULL值并且要对该列使用DISTINCT 子句，则MySQL仅保留一个NULL值，因为DISTINCT 子句将所有NULL值视为相同的值。

例如，在customers表中，我们有许多行，其  state列中包含有NULL值。

```sql
+------------------------+
| customers              |
+------------------------+
| customerNumber         |
| customerName           |
| contactLastName        |
| contactFirstName       |
| phone                  |
| addressLine1           |
| addressLine2           |
| city                   |
| state                  |
| postalCode             |
| country                |
| salesRepEmployeeNumber |
| creditLimit            |
+------------------------+
13 rows in set (0.01 sec)
```
当您使用DISTINCT子句查询客户的状态时，您将看到state中只有唯一个NULL值，并作为以下查询：

```sql
SELECT DISTINCT
    state
FROM
    customers; 
```
运行结果：

```sql
+---------------+
| state         |
+---------------+
| NULL          |
| NV            |
| Victoria      |
| CA            |
| NY            |
| PA            |
...
```
MySQL DISTINCT 多列
您可以将DISTINCT 子句与多个列一起使用。在这种情况下，MySQL使用这些列中的值组合来确定结果集中行的唯一性。

例如，要从customers表中获取城市和州的唯一组合，请使用以下查询：

```sql
SELECT DISTINCT
	state,city 
FROM
	customers 
WHERE
	state IS NOT NULL 
ORDER BY
	state,city; 
```
运行结果：

```sql
+---------------+----------------+
| state         | city           |
+---------------+----------------+
| BC            | Tsawassen      |
| BC            | Vancouver      |
| CA            | Brisbane       |
| CA            | Burbank        |
| CA            | Burlingame     |
| CA            | Glendale       |
| CA            | Los Angeles    |
| CA            | Pasadena       |
...
```
如果没有DISTINCT 语句，您将获得州和城市的重复组合，如下所示：

```sql
SELECT 
    state, city 
FROM 
    customers 
WHERE 
    state IS NOT NULL 
ORDER BY state, city; 
```
运行结果：

```sql
+---------------+----------------+
| state         | city           |
+---------------+----------------+
| BC            | Tsawassen      |
| BC            | Vancouver      |
| CA            | Brisbane       |
| CA            | Burbank        |
| CA            | Burlingame     |
| CA            | Glendale       |
| CA            | Los Angeles    |
| CA            | Pasadena       |
...
```
DISTINCT语句与GROUP BY 语句
如果在不使用聚合函数的情况下在SELECT语句中使用GROUP BY子句，则GROUP BY子句的行为与DISTINCT 子句类似。

以下语句使用GROUP BY子句从customers表中选择客户的唯一状态。

```sql
SELECT state
FROM
    customers
GROUP BY state; 
```
运行结果：

```sql
+---------------+
| state         |
+---------------+
| NULL          |
| BC            |
| CA            |
| Co. Cork      |
| CT            |
| Isle of Wight |
...
```
您可以使用以下DISTINCT子句获得同样的结果：

```sql
SELECT DISTINCT
    state 
FROM
    customers; 
```
运行结果：

```sql
+---------------+
| state         |
+---------------+
| NULL          |
| NV            |
| Victoria      |
| CA            |
| NY            |
| PA            |
| CT            |
| MA            |
...
```
一般来说，DISTINCT 语句是GROUP BY语句的特殊情况。之间的区别DISTINCT 条款，GROUP BY条款是GROUP BY条款排序结果集，而DISTINCT 条款不。

如果将ORDER BY子句添加到使用DISTINCT子句的语句中  ，则结果集将进行排序，并且与使用GROUP BY子句的语句返回的结果集相同。

```sql
SELECT DISTINCT
    state 
FROM
    customers 
ORDER BY state; 
```
运行结果：

```sql
+---------------+
| state         |
+---------------+
| NULL          |
| BC            |
| CA            |
| Co. Cork      |
| CT            |
| Isle of Wight |
| MA            |
...
```
MySQL DISTINCT和聚合函数
您可以将DISTINCT子句与聚合函数（例如，SUM，AVG和COUNT ）一起使用，将聚合函数应用于结果集之前删除重复的行。

例如，要计算美国客户的不同的州数，请使用以下查询：

```sql
SELECT
    COUNT(DISTINCT state)
FROM
    customers 
WHERE
    country = 'USA'; 
```
运行结果：

```sql
+-----------------------+
| COUNT(DISTINCT state) |
+-----------------------+
|                     8 |
+-----------------------+
1 row in set (0.00 sec)
```
MySQL DISTINCT 和 LIMIT子句
如果您将DISTINCT 子句与LIMIT子句一起使用，MySQL会在找到LIMIT子句中指定的唯一行数时立即停止搜索。

以下是在 customers表中查询选择前五个非null唯一州。

```sql
SELECT DISTINCT
    state
FROM
    customers 
WHERE
    state IS NOT NULL LIMIT 5; 
```
运行结果：

```sql
+----------+
| state    |
+----------+
| NV       |
| Victoria |
| CA       |
| NY       |
| PA       |
+----------+
5 rows in set (0.00 sec)
```