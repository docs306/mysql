MySQL SELECT语句查询表或视图中的数据。

SELECT语句允许您从表或视图中获取数据。表格包含行和列，如电子表格。通常，您希望查看子集行，列的子集或两者的组合。SELECT语句的结果称为结果集，它是一个行列表，每个行包含相同数量的列。

请参阅示例数据库中的下employees表。它有八列包含：: employee number, last name, first name, extension, email, office code, reports to, job title及多条记录。
```sql
+----------------+-----------+-----------+-----------+-----------------------+------------+-----------+----------------------+
| employeeNumber | lastName | firstName | extension | email | officeCode | reportsTo | jobTitle |
+----------------+-----------+-----------+-----------+-----------------------+------------+-----------+----------------------+
| 1002 | Murphy | Diane | x5800 | dmurphy@yiibai.com | 1 | NULL | President |
| 1056 | Patterson | Mary | x4611 | mpatterso@yiibai.com | 1 | 1002 | VP Sales |
| 1076 | Firrelli | Jeff | x9273 | jfirrelli@yiibai.com | 1 | 1002 | VP Marketing |
| 1088 | Patterson | William | x4871 | wpatterson@yiibai.com | 6 | 1056 | Sales Manager (APAC) |
| 1102 | Bondur | Gerard | x5408 | gbondur@gmail.com | 4 | 1056 | Sale Manager (EMEA) |
| 1143 | Bow | Anthony | x5428 | abow@gmail.com | 1 | 1056 | Sales Manager (NA) |
| 1165 | Jennings | Leslie | x3291 | ljennings@yiibai.com | 1 | 1143 | Sales Rep |
| 1166 | Thompson | Leslie | x4065 | lthompson@yiibai.com | 1 | 1143 | Sales Rep |
| 1188 | Firrelli | Julie | x2173 | jfirrelli@yiibai.com | 2 | 1143 | Sales Rep |
```
SELECT语句控制您要查看的列和行。例如，如果要对所有员工的名字，姓氏和职位感兴趣，或者您只想查看职位名称为销售代表的每位员工的信息，则SELECT语句可帮助您执行此操作。

我们来看看SELECT语句的语法：
```sql
SELECT
    column_1, column_2, ...
FROM
    table_1
[INNER | LEFT |RIGHT] JOIN table_2 ON conditions
WHERE
    conditions
GROUP BY column_1
HAVING group_conditions
ORDER BY column_1
LIMIT offset, length; 
```
以下是SELECT的使用及相关的组合关键字：

SELECT 后跟逗号分隔列或星号（*）列表，表示返回所有列。
FROM 指定要查询数据的表或视图。
JOIN 根据特定的连接条件从其他表中获取相关数据。
WHERE 子句过滤结果集中的行。
GROUP BY  子句将一组行分组到组中，并在每个组上应用聚合函数。
HAVING  子句根据GROUP BY子句定义的组过滤组。
ORDER BY  子句指定用于排序的列的列表。
LIMIT 约束返回的行数。
在查询语句中要求必须包含SELECT和FROM ,其它都是可选项。

您将在后续教程中更详细地了解每个子句。在本教程中，我们将重点关注SELECT语句的基本形式。

MySQL SELECT语句示例
SELECT语句允许您通过在SELECT子句中指定逗号分隔列的列表来查询表的部分数据。例如，如果您只想查看员工的名字，姓氏和职位，则使用以下查询：

```sql
SELECT
    lastname, firstname, jobtitle
FROM
    employees; 
```
即使  employees表有很多列，SELECT语句只返回表中所有行的三列数据，结果如下：
```sql
+-----------+-----------+----------------------+
| lastname  | firstname | jobtitle             |
+-----------+-----------+----------------------+
| Murphy    | Diane     | President            |
| Patterson | Mary      | VP Sales             |
| Firrelli  | Jeff      | VP Marketing         |
| Patterson | William   | Sales Manager (APAC) |
| Bondur    | Gerard    | Sale Manager (EMEA)  |
| Bow       | Anthony   | Sales Manager (NA)   |
...
```

如果要获取表中所有列的数据employees，可以列出SELECT子句中的所有列名。或者您只需使用星号（*）表示您希望从表的所有列获取数据，如下面的查询：

```sql
SELECT * FROM employees; 
```
返回表中的所有列和行employees。
```sql
+----------------+-----------+-----------+-----------+-----------------------+------------+-----------+----------------------+
| employeeNumber | lastName  | firstName | extension | email                 | officeCode | reportsTo | jobTitle             |
+----------------+-----------+-----------+-----------+-----------------------+------------+-----------+----------------------+
|           1002 | Murphy    | Diane     | x5800     | dmurphy@yiibai.com    | 1          |      NULL | President            |
|           1056 | Patterson | Mary      | x4611     | mpatterso@yiibai.com  | 1          |      1002 | VP Sales             |
|           1076 | Firrelli  | Jeff      | x9273     | jfirrelli@yiibai.com  | 1          |      1002 | VP Marketing         |
|           1088 | Patterson | William   | x4871     | wpatterson@yiibai.com | 6          |      1056 | Sales Manager (APAC) |
|           1102 | Bondur    | Gerard    | x5408     | gbondur@gmail.com     | 4          |      1056 | Sale Manager (EMEA)
...
```
在实例使用中不推荐使用（*）原因如下：

- 星号（*）返回您可能不使用的列中的数据。这会占用和浪费MySQL数据库服务器和应用程序之间产生不必要的I/O磁盘和网络流量。
- 明确指定列，则结果集更易于预测且更易于管理。想象一下，当您使用星号（*）并且有人通过添加更多列来更改表时，您将得到与您预期的结果集不同的结果集。
- 使用星号（*）可能会将敏感信息暴露给未经授权的用户。
