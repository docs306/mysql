MySQL CREATE DATABASE语句在服务器中创建新数据库。

MySQL将数据库实现为一个目录，其中包含与数据库中的表对应的所有文件。

要在MySQL中创建新数据库，请使用CREATE DATABASE具有以下语法的语句：
```
CREATE DATABASE [IF NOT EXISTS] database_name
[CHARACTER SET charset_name]
[COLLATE collation_name] 
```
首先，CREATE DATABASE子句指定database_name。数据库名称在MySQL服务器实例中必须是唯一的。如果您尝试使用已存在的名称创建数据库，MySQL会发出错误。

其次，为避免在意外创建已存在的数据库时出现错误，可以指定IF NOT EXISTS选项。在这种情况下，MySQL不会发出错误，而是终止CREATE DATABASE语句。

第三，您可以 在创建时为新数据库指定字符集和排序规则。如果省略CHARACTER SET和COLLATE子句，MySQL将使用新数据库的默认字符集和排序规则。

使用mysql程序创建一个新数据库
要通过mysql程序创建新数据库，请使用以下步骤：

首先，使用root用户登录MySQL服务器
```
>mysql -u root -p
Enter password: ******** 
```
root输入用户的密码，然后按Enter键。

接下来，要显示服务器中的现有数据库以确保您没有创建已存在的新数据库，请使用以下SHOW DATABASES命令：
```
mysql> SHOW DATABASES;
 
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| mysqldemo          |
| performance_schema |
| sys                |
+--------------------+
5 rows in set (0.00 sec) 
```
MySQL返回当前服务器中的五个现有数据库。

然后，CREATE DATABASE使用数据库发出命令，例如，testdb然后按Enter键：
```
mysql> CREATE DATABASE testdb;
Query OK, 1 row affected (0.12 sec) 
```
之后，如果要查看创建的数据库，可以使用以下SHOW CREATE DATABASE命令：
```
mysql> SHOW CREATE DATABASE testdb; 
+----------+-------------------------------------------------------------------+
| Database | Create Database                                                   |
+----------+-------------------------------------------------------------------+
| testdb   | CREATE DATABASE `testdb` /*!40100 DEFAULT CHARACTER SET latin1 */ |
+----------+-------------------------------------------------------------------+
1 row in set (0.00 sec)
```
MySQL返回数据库名称以及数据库的字符集和排序规则。

最后，要访问新创建的数据库，请使用以下USE database命令：
```
mysql> USE testdb;
Database changed 
```
现在，您可以开始在testdb数据库中创建表和其他数据库对象 。

要退出mysql程序，请在命令行键入exit：
```
mysql> exit
Bye 
```