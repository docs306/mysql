# MySQL INNER JOIN

INNER JOIN子句根据连接条件从多个表中选择数据。

MySQL INNER JOIN子句将一个表中的行与其他表中的行进行匹配，并允许您查询包含两个表中列的行。

INNER JOIN子句是SELECT查询的可选部分。它出现在FROM语句之后。

在使用INNER JOIN之前，必须指定以下条件：

- 首先，出现在FROM子句中的主表。
- 第二，要与主表连接的表，表出现在INNER JOIN子句中。理论上，可以将表与许多其他表联接起来。但是，为了获得更好的性能，应限制要加入的表的数量。
- 第三，连接条件或连接谓词。连接条件出现在INNER JOIN子句的ON关键字之后。连接条件是将主表中的行与其他表中的行进行匹配的规则。

语法如下：
```sql
SELECT column_list
FROM t1
INNER JOIN t2 ON join_condition1
INNER JOIN t3 ON join_condition2
...
WHERE where_conditions; 
```
假设正在连接两个表t1并t2使用 INNER JOIN子句来简化上面的语法。

```sql
SELECT column_list
FROM t1
INNER JOIN t2 ON join_condition; 
```
- t1表中的每一行，将其与t2表的每一行进行比较，以检查它们是否都满足连接条件。  
- 当满足连接条件时，INNER JOIN将返回一个新行，行由两个表t1和t2表组成。

*注：必须根据连接条件匹配两个表t1和t2表中的行。如果未找到匹配项，查询将返回空结果集。当使用2个以上的表时，用法也是一样的。*


### INNER JOIN 中避免出现模糊的列错误

如果连接具有相同列名的多个表，则必须使用表限定符来引用SELECT 和ON子句中的列，以避免出现模糊列错误。

例如，如果两个表t1和两个t2表都具有相同的列c，则必须c使用表限定符作为 t1.C或 t2.C在SELECT和ON子句中引用列。

**为了节省键入表限定符的时间，可以在查询中使用表别名。**

例如，可以为表提供表的别名 t 并使用 t.column 而不是使用它来引用其列 verylongtablename.column。

### INNER JOIN
示例数据库中的 products和 productlines表。
```
+--------------------+
| products           |
+--------------------+
| productCode        |
| productName        |
| productLine        |
| productScale       |
| productVendor      |
| buyPrice           |
| MSRP               |
+--------------------+

+-----------------+
| productlines    |
+-----------------+
| productLine     |
| textDescription |
| htmlDescription |
| image           |
+-----------------+
```
products 和 productlines 表以 productLine 列关联。products表中的productLine列称为外键列。

通常连接具有外键关系的表像 productlines 和 products表。

从 products 表取 productCode 与 productName
从 productlines 表取 textDescription
因此，需要通过 productline 使用 INNER JOIN子 句根据列匹配行来从两个表中选择数据：
```sql
SELECT 
    t1.productCode, 
    t1.productName, 
    t2.textDescription
FROM products t1
INNER JOIN productlines t2 ON t1.productline = t2.productline; 
```
运行结果：
```
+-------------+---------------------------------------------+--------------------------+
| productCode | productName                                 | left(textDescription,13) |
+-------------+---------------------------------------------+--------------------------+
| S10_1949    | 1952 Alpine Renault                         | Attention car ...     |
| S10_4757    | 1972 Alfa Romeo GTA                         | Attention car ...     |
| S10_4962    | 1962 LanciaA Delta                          | Attention car ...     |
| S12_1099    | 1968 Ford Mustang                           | Attention car ...     |
| S12_1108    | 2001 Ferrari Enzo                           | Attention car ...     |
...
```

两个表的连接列具有相同的名称 productline，可以使用 USING()：
```sql
SELECT 
    productCode, 
    productName, 
    textDescription
FROM products
INNER JOIN productlines USING (productline); 
```
它返回相同的结果集，但此语法 不必使用表别名。

### INNER JOIN 使用 GROUP BY

orders 和 orderdetails 表格。
```
+----------------+
| orders         |
+----------------+
| orderNumber    |
| orderDate      |
| status         |
| comments       |
| customerNumber |
+----------------+
7 rows in set (0.01 sec)

+-----------------+
| orderdetails    |
+-----------------+
| orderNumber     |
| productCode     |
| quantity        |
| price           |
| orderLineNumber |
+-----------------+
5 rows in set (0.00 sec)
```
可以使用 INNER JOIN, GROUP BY 子句从orders和orderdetails表中获取订单号，订单状态和总销售额：
```sql
SELECT T1.orderNumber, status, SUM(quantity * price) total
FROM orders AS T1
INNER JOIN orderdetails AS T2 ON T1.orderNumber = T2.orderNumber
GROUP BY orderNumber; 
```
运行结果：
```
+-------------+------------+----------+
| orderNumber | status     | total    |
+-------------+------------+----------+
|       10100 | Shipped    | 10223.83 |
|       10105 | Shipped    | 53959.21 |
...
```
同样，以下查询等同于上面的查询：

```sql
SELECT orderNumber,status,SUM(quantity * price) total
FROM orders
INNER JOIN orderdetails USING (orderNumber)
GROUP BY orderNumber; 
```
### INNER JOIN 使用非等于运算符
还可以使用其他运算符（如大于（>），小于（<）和非等于（<>）运算符来形成连接词。

以下查询使用小于号
```sql
SELECT 
    o.orderNumber, 
    o.productName, 
    p.msrp, 
    o.price
FROM products p
INNER JOIN orderdetails o ON p.productcode = o.productcode AND p.msrp > o.price
WHERE p.productcode = 'S10_1678'; 
```
运行结果：
```
+-------------+---------------------------------------+-------+-----------+
| orderNumber | productName                           | msrp  | price |
+-------------+---------------------------------------+-------+-----------+
|       10107 | 1969 Harley Davidson Ultimate Chopper | 95.70 |     81.35 |
|       10121 | 1969 Harley Davidson Ultimate Chopper | 95.70 |     86.13 |
|       10134 | 1969 Harley Davidson Ultimate Chopper | 95.70 |     90.92 |
|       10145 | 1969 Harley Davidson Ultimate Chopper | 95.70 |     76.56 |
|       10159 | 1969 Harley Davidson Ultimate Chopper | 95.70 |     81.35 |
```