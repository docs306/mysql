MySQL IN运算符来确定指定的值是否存在于列表或子查询中。

MySQL IN运算符简介
IN  运算符允许你确定一个指定的值在一组值匹配的任何值或子查询。

以下IN  运算符的语法：

```sql
SELECT 
    column1,column2,...
FROM
    table_name
WHERE 
 (expr|column_1) IN ('value1','value2',...); 
```
我们可以仔细的查以下方式：

在WHERE子句中使用带column或IN运算符的表达式（expr）
用逗号（,）分隔列表中的值。
IN运算符逻辑是：column 在指定列表中则返回1 ，不在其中则返回0.

当列表中的值都是常量时，MySQL执行以下步骤：

- 首先，评估基于所述值类型的的column_1所述的或expr 表达的结果  。 
- 其次，对值进行排序。
- 第三，使用二进制搜索算法搜索值。因此，使用IN  运算符查询执行带有常量列表的速度非常快。
请注意：如果expr列表中的任何值NULL，则IN  运算符返回NULL。

IN运算符与NOT运算符组合以来检查值是否与列表或子查询中的任何值不匹配。也可以其它语句中在WHERE子句中使用IN运算符，例如  UPDATE，和DELETE。

MySQL IN运算符实例
让我们练习一些使用IN运算符的例子。请参阅示例数据库中的offices表：

```
+--------------+
| offices      |
+--------------+
| officeCode   |
| city         |
| phone        |
| addressLine1 |
| addressLine2 |
| state        |
| country      |
| postalCode   |
| territory    |
+--------------+
9 rows in set (0.02 sec)
```
如果要查找位于美国和法国的办事处，可以使用IN运算符作为以下查询：

```sql
SELECT 
    officeCode, 
    city, 
    phone, 
    country
FROM
    offices
WHERE
    country IN ('USA' , 'France'); 
```
运行结果：

```
+------------+---------------+-----------------+---------+
| officeCode | city          | phone           | country |
+------------+---------------+-----------------+---------+
| 1          | San Francisco | +1 650 219 4782 | USA     |
| 2          | Boston        | +1 215 837 0825 | USA     |
| 3          | NYC           | +1 212 555 3000 | USA     |
| 4          | Paris         | +33 14 723 4404 | France  |
+------------+---------------+-----------------+---------+
4 rows in set (0.01 sec)
```
您可以使用OR以下查询获得与运算符相同的结果：

```sql
SELECT 
    officeCode, 
    city, 
    phone
FROM
    offices
WHERE
    country = 'USA' OR country = 'France'; 
```
如果列表有许多值，则需要使用多个OR 运算符构造一个非常长的语句。因此，IN运算符缩短查询并更具可读性。

要获得不在美国和法国的办事处，请NOT IN  在下面的WHERE条款中使用：

```sql
SELECT 
    officeCode, 
    city, 
    phone
FROM
    offices
WHERE
    country NOT IN ('USA' , 'France'); 
+------------+---------+------------------+
| officeCode | city    | phone            |
+------------+---------+------------------+
| 5          | Beijing | +86 33 224 5000  |
| 6          | Sydney  | +61 2 9264 2451  |
| 7          | London  | +44 20 7877 2041 |
+------------+---------+------------------+
3 rows in set (0.00 sec)
```
MySQL IN与子查询一起使用
IN操作通常与子查询使用。子查询不是提供文字值列表，而是从一个或多个表中获取值列表，并将它们用作IN运算符的输入值。

我们来看看示例数据库中的orders和orderDetails表：

```sql
+----------------+
| orders         |
+----------------+
| orderNumber    |
| orderDate      |
| requiredDate   |
| shippedDate    |
| status         |
| comments       |
| customerNumber |
+----------------+
7 rows in set (0.02 sec)

+-----------------+
| orderdetails    |
+-----------------+
| orderNumber     |
| productCode     |
| quantityOrdered |
| priceEach       |
| orderLineNumber |
+-----------------+
5 rows in set (0.00 sec)
```
例如，如果你想找到的订单的总价值是大于60,000，您使用的IN运算符的查询：

```sql
SELECT    
 orderNumber, 
 customerNumber, 
 status, 
 shippedDate
FROM    
 orders
WHERE orderNumber IN
(
 SELECT 
 orderNumber
 FROM 
 orderDetails
 GROUP BY 
 orderNumber
 HAVING SUM(quantityOrdered * priceEach) > 60000
); 
```
运行结果：

```sql
+-------------+----------------+---------+-------------+
| orderNumber | customerNumber | status  | shippedDate |
+-------------+----------------+---------+-------------+
|       10165 |            148 | Shipped | 2013-12-26  |
|       10287 |            298 | Shipped | 2014-09-01  |
|       10310 |            259 | Shipped | 2014-10-18  |
+-------------+----------------+---------+-------------+
3 rows in set (0.03 sec)
```
上面的整个查询可以分解为两个单独的查询。

首先，子查询返回一个订单号列表，其值大于60,000使用GROUP BY 和HAVING 子句：

```sql
SELECT 
    orderNumber
FROM
    orderDetails
GROUP BY 
    orderNumber
HAVING 
    SUM(quantityOrdered * priceEach) > 60000; 
```
运行结果：

```sql
+-------------+
| orderNumber |
+-------------+
|       10165 |
|       10287 |
|       10310 |
+-------------+
3 rows in set (0.00 sec)
```
其次，外部查询使用WHERE子句中的IN运算符从orders表中获取数据：

```sql
SELECT 
    orderNumber, 
    customerNumber, 
    status, 
    shippedDate
FROM
    orders
WHERE
    orderNumber IN (10165,10287,10310); 
```
运行结果：

```sql
+-------------+----------------+---------+-------------+
| orderNumber | customerNumber | status  | shippedDate |
+-------------+----------------+---------+-------------+
|       10165 |            148 | Shipped | 2013-12-26  |
|       10287 |            298 | Shipped | 2014-09-01  |
|       10310 |            259 | Shipped | 2014-10-18  |
+-------------+----------------+---------+-------------+
3 rows in set (0.01 sec)
```