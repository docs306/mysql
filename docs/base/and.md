MySQL AND运算符组合多个布尔表达式来过滤数据。

MySQL AND运算符简介
AND操作是将两个或多个逻辑运算符布尔表达式只有当两个表达式为true返回true。如果两个表达式的一个评估为假返回false。

以下是AND运算符的语法：

```sql
boolean_expression_1 AND boolean_expression_2 
```
下表说明了AND组合true，false和null时运算符的结果。
```
TRUE	FALSE	NULL
TRUE	TRUE	FALSE	NULL
FALSE	FALSE	FALSE	FALSE
NULL	NULL	FALSE	NULL
```
AND运算符往往是使用在SELECT，UPDATE，DELETE语句中的WHERE子句中。AND运算符还用于 INNER JOIN 和   LEFT JOIN子句中关联条件。

逻辑操作符AND 和OR 也称作短路操作符（short-circuitlogic）或者惰性求值（lazy evaluation）：它们的参数从左向右解析，一旦结果可以确定就停止。

请考虑以下实例。

```sql
SELECT 1 = 0 AND 1 / 0 ; 
```
运行结果：

```sql
+-----------------+
| 1 = 0 AND 1 / 0 |
+-----------------+
|               0 |
+-----------------+
1 row in set (0.00 sec)
```
请注意，在MySQL中，零被视为false，非零被视为true。

MySQL仅评估表达式1 = 0 AND 1 / 0的第一部分1 = 0。因为表达式1 = 0返回false，所以MySQL可以得出整个表达式的结果，这是false。MySQL不再评估表达式的剩余部分，即1 / 0; 如果是这样，它将发出错误，因为除以零错误。

MySQL AND 运算符实例
让我们使用示例数据库中的customers表进行演示。

```sql
+------------------------+
| customers              |
+------------------------+
| customerNumber         |
| customerName           |
| contactLastName        |
| contactFirstName       |
| phone                  |
| addressLine1           |
| addressLine2           |
| city                   |
| state                  |
| postalCode             |
| country                |
| salesRepEmployeeNumber |
| creditLimit            |
+------------------------+
13 rows in set (0.01 sec)
```
以下语句使用AND运算符查找位于美国加利福尼亚州（CA）的客户：

```sql
SELECT 
    customername, 
    country, 
    state
FROM
    customers
WHERE
    country = 'USA' AND state = 'CA'; 
```
运行结果：

```sql
+------------------------------+---------+-------+
| customername                 | country | state |
+------------------------------+---------+-------+
| Mini Gifts Distributors Ltd. | USA     | CA    |
| Mini Wheels Co.              | USA     | CA    |
| Technics Stores Inc.         | USA     | CA    |
| Toys4GrownUps.com            | USA     | CA    |
| Boards & Toys Co.            | USA     | CA    |
| Collectable Mini Designs Co. | USA     | CA    |
| Corporate Gift Ideas Co.     | USA     | CA    |
| Men 'R' US Retailers, Ltd.   | USA     | CA    |
| The Sharp Gifts Warehouse    | USA     | CA    |
| West Coast Collectables Co.  | USA     | CA    |
| Signal Collectibles Ltd.     | USA     | CA    |
+------------------------------+---------+-------+
11 rows in set (0.00 sec)
```
通过使用AND运算符，您可以组合两个以上的布尔表达式。例如，以下查询返回位于美国加利福尼亚州且信用额度大于100K的客户。

```sql
SELECT    
 customername, 
 country, 
 state, 
 creditlimit
FROM    
 customers
WHERE country = 'USA'
 AND state = 'CA'
 AND creditlimit > 100000; 
```
运行结果：

```sql
+------------------------------+---------+-------+-------------+
| customername                 | country | state | creditlimit |
+------------------------------+---------+-------+-------------+
| Mini Gifts Distributors Ltd. | USA     | CA    |   210500.00 |
| Collectable Mini Designs Co. | USA     | CA    |   105000.00 |
| Corporate Gift Ideas Co.     | USA     | CA    |   105000.00 |
+------------------------------+---------+-------+-------------+
3 rows in set (0.00 sec)
```