MySQL CREATE TABLE语句在数据库中创建新表  。

MySQL CREATE TABLE语法
要在数据库中创建新表，请使用MySQL CREATE TABLE  语句。CREATE TABLE 语句是MySQL中最复杂的语句之一。

以下说明了语句简化版的语法  CREATE TABLE  ：
```
CREATE TABLE [IF NOT EXISTS] table_name(
    column_list
) ENGINE=storage_engine 
```
让我们更详细地研究一下语法。

首先，指定要在CREATE TABLE  子句之后创建的表的名称。表名在数据库中必须是唯一的。IF NOT EXISTS子句是可选，允许您检查您正在创建的表是否已存在于数据库中。如果是这种情况，MySQL将忽略整个语句，不会创建任何新表。强烈建议你在每个CREATE TABLE语句中使用IF NOT EXISTS ，以避免创建已存在的新表时出错。

其次，  您在column_list部分列中为表指定列，列以逗号分隔。

第三，您可以选择在ENGINE子句中为表指定存储引擎。您可以使用任何存储引擎，如InnoDB和MyISAM。如果您没有明确声明存储引擎，MySQL将默认使用InnoDB。

自MySQL版本5.5以来，InnoDB成为默认的存储引擎。InnoDB存储引擎带来了关系数据库管理系统的许多好处，例如ACID事务，参照完整性和崩溃恢复。在以前的版本中，MySQL使用MyISAM作为默认存储引擎。

要在CREATE TABLE  语句中为表定义列，请使用以下语法：
```
column_name data_type(length) [NOT NULL] [DEFAULT value] [AUTO_INCREMENT] 
```
上面语法中最重要的组件是：

将column_name 指定列的名称。每列都有特定的数据类型和最大长度，例如：VARCHAR(255) 
NOT NULL指示列不允许NULL值。
将DEFAULT value用于指定列的默认值。
这AUTO_INCREMENT  表示只要将新行插入表中，就会自动生成一列的值。每个表只有一AUTO_INCREMENT 列。
如果要将列或一组列设置为主键，请使用以下语法：
```
PRIMARY KEY (col1,col2,...) 
```
MySQL CREATE TABLE语句示例
以下语句创建一个名为的新表tasks：

MySQL CREATE TABLE示例
```
CREATE TABLE IF NOT EXISTS tasks (
    task_id INT AUTO_INCREMENT,
    title VARCHAR(255) NOT NULL,
    start_date DATE,
    due_date DATE,
    status TINYINT NOT NULL,
    priority TINYINT NOT NULL,
    description TEXT,
    PRIMARY KEY (task_id)
)  ENGINE=INNODB; 
```
任务表包含以下列：

这task_id是一个自动增量列。如果使用INSERT语句向表中添加新行而未指定task_id列的值，则task_id列将采用以1开头的自动生成的整数。这task_id是主键列。
title列是一个可变字符串列，其最大长度为255.这意味着您不能将长度大于255的字符串插入此列。NOT NULL表示列必须有一个值。换句话说，您必须在插入或更新此列时提供值。
start_date和due_date是其接受NULL日期列。
status和priority是TINYINT不容许NULL列。
description列是一个TEXT接受NULL 的列。
