MySQL IS NULL运算符来测试值是否为NULL值。

MySQL IS NULL运算符简介
要测试值是否值NULL，请使用 IS NULL运算符。以下是IS NULL运算符的语法：

value IS NULL 
如果值为NULL，则表达式返回true。否则，它返回false。

请注意，MySQL没有内置BOOLEAN类型。它使用TINYINT(1)来表示BOOLEAN 值，即，true表示1，false表示0。

因为它IS NULL是一个比较运算符，所以您可以在任何可以使用运算符的地方使用它，例如在  SELECTor WHERE子句中。请参阅以下示例：

```sql
SELECT 1 IS NULL,  -- 0
       0 IS NULL,  -- 0
       NULL IS NULL; -- 1 
```
运行结果：

```
+-----------+-----------+--------------+
| 1 IS NULL | 0 IS NULL | NULL IS NULL |
+-----------+-----------+--------------+
|         0 |         0 |            1 |
+-----------+-----------+--------------+
1 row in set (0.00 sec)
```
要检查值是否不是NULL，请使用IS NOT NULL运算符，如下所示：

```sql
value IS NOT NULL 
```
如果值不是，则此表达式返回true（1）NULL。否则，它返回false（0）。请考虑以下示例：

```sql
SELECT 1 IS NOT NULL, -- 1
       0 IS NOT NULL, -- 1
       NULL IS NOT NULL; -- 0 
```
运行结果：

```
+---------------+---------------+------------------+
| 1 IS NOT NULL | 0 IS NOT NULL | NULL IS NOT NULL |
+---------------+---------------+------------------+
|             1 |             1 |                0 |
+---------------+---------------+------------------+
1 row in set (0.00 sec)
```

MySQL NULL的例子
我们将使用示例数据库中的customers表进行演示。

```
+------------------------+
| customers              |
+------------------------+
| customerNumber         |
| customerName           |
| contactLastName        |
| contactFirstName       |
| phone                  |
| addressLine1           |
| addressLine2           |
| city                   |
| state                  |
| postalCode             |
| country                |
| salesRepEmployeeNumber |
| creditLimit            |
+------------------------+
13 rows in set (0.02 sec)
```
以下查询使用IS NULL运算符查找没有销售代表的客户：

```sql
SELECT 
    customerName, 
    country, 
    salesrepemployeenumber
FROM
    customers
WHERE
    salesrepemployeenumber IS NULL
ORDER BY customerName; 
```
运行结果：
```
+--------------------------------+--------------+------------------------+
| customerName                   | country      | salesrepemployeenumber |
+--------------------------------+--------------+------------------------+
| ANG Resellers                  | Spain        |                   NULL |
| Anton Designs, Ltd.            | Spain        |                   NULL |
| Asian Shopping Network, Co     | Singapore    |                   NULL |
| Asian Treasures, Inc.          | Ireland      |                   NULL |
| BG&E Collectables              | Switzerland  |                   NULL |
| Cramer Spezialitten, Ltd       | Germany      |                   NULL |
...
```
此示例使用IS NOT NULL运算符来获取有销售代表的客户：

```sql
SELECT 
    customerName, 
    country, 
    salesrepemployeenumber
FROM
    customers
WHERE
    salesrepemployeenumber IS NOT NULL
ORDER BY customerName; 
```
运行结果：
```
+------------------------------------+-------------+------------------------+
| customerName                       | country     | salesrepemployeenumber |
+------------------------------------+-------------+------------------------+
| Alpha Cognac                       | France      |                   1370 |
| American Souvenirs Inc             | USA         |                   1286 |
| Amica Models & Co.                 | Italy       |                   1401 |
| Anna's Decorations, Ltd            | Australia   |                   1611 |
| Atelier graphique                  | France      |                   1370 |
| Australian Collectables, Ltd       | Australia   |                   1611 |
...
```
MySQL IS NULL的特殊功能
为了与ODBC程序兼容，MySQL支持IS NULL运算符的一些特殊功能。

1）如果具有约束并包含特殊日期的列DATE或DATETIME列，则可以使用运算符查找此类行。NOT NULL'0000-00-00'IS NULL

```sql
CREATE TABLE IF NOT EXISTS projects (
    id INT AUTO_INCREMENT,
    title VARCHAR(255),
    begin_date DATE NOT NULL,
    complete_date DATE NOT NULL,
    PRIMARY KEY(id)
);
 
INSERT INTO projects(title,begin_date, complete_date)
VALUES('New CRM','2020-01-01','0000-00-00'),
      ('ERP Future','2020-01-01','0000-00-00'),
      ('VR','2020-01-01','2030-01-01');
 
 
SELECT 
    *
FROM
    projects
WHERE
    complete_date IS NULL; 
```
运行结果：
```
+----+------------+------------+---------------+
| id | title      | begin_date | complete_date |
+----+------------+------------+---------------+
|  1 | New CRM    | 2020-01-01 | 0000-00-00    |
|  2 | ERP Future | 2020-01-01 | 0000-00-00    |
+----+------------+------------+---------------+
2 rows in set (0.00 sec)
```
在此示例中，我们创建了一个名为projects的新表，并将一些数据插入到表中。最后一个查询用于IS NULL获取值中complete_date值为的行'0000-00-00'。

2）如果变量@@sql_auto_is_null设置为1，则可以在INSERT使用IS NULL运算符执行语句后获取生成列的值。请注意，默认情况下，变量@@sql_auto_is_null为0.请考虑以下示例：

首先，将变量设置@@sql_auto_is_null为1。

SET @@sql_auto_is_null = 1; 
其次，在projects表中插入一个新行：
```sql
INSERT INTO projects(title,begin_date, complete_date)
VALUES('MRP III','2010-01-01','2020-12-31'); 
```
第三，使用  IS NULL运算符获取id列的生成值：

```sql
SELECT 
    id
FROM
    projects
WHERE
    id IS NULL; 
```
运行结果：
```
+----+
| id |
+----+
|  4 |
+----+
1 row in set (0.00 sec)
```
MySQL IS NULL优化
MySQL对于IS NULL运算符执行相同的优化，就像它对等号（=）运算符一样。

例如，MySQL使用索引时它搜索NULL与IS NULL如下面的查询运算符所示：

```sql
SELECT 
    customerNumber, 
    salesRepEmployeeNumber
FROM
    customers
WHERE
    salesRepEmployeeNumber IS NULL; 
查看EXPLAIN查询：

EXPLAIN SELECT 
    customerNumber, 
    salesRepEmployeeNumber
FROM
    customers
WHERE
    salesRepEmployeeNumber IS NULL; 
```
运行结果：
```
+----+-------------+-----------+------+------------------------+------------------------+---------+-------+------+--------------------------+
| id | select_type | table     | type | possible_keys          | key                    | key_len | ref   | rows | Extra                    |
+----+-------------+-----------+------+------------------------+------------------------+---------+-------+------+--------------------------+
|  1 | SIMPLE      | customers | ref  | salesRepEmployeeNumber | salesRepEmployeeNumber | 5       | const |   22 | Using where; Using index |
+----+-------------+-----------+------+------------------------+------------------------+---------+-------+------+--------------------------+
1 row in set (0.00 sec)
```
MySQL还可以针对col = value OR col IS NULL组合进行优化，请参阅以下示例：

```sql
EXPLAIN SELECT 
    customerNumber,
    salesRepEmployeeNumber
FROM
    customers
WHERE
    salesRepEmployeeNumber = 1370 OR
    salesRepEmployeeNumber IS NULL; 
```
运行结果：
```
+----+-------------+-----------+-------------+------------------------+------------------------+---------+-------+------+--------------------------+
| id | select_type | table     | type        | possible_keys          | key                    | key_len | ref   | rows | Extra                    |
+----+-------------+-----------+-------------+------------------------+------------------------+---------+-------+------+--------------------------+
|  1 | SIMPLE      | customers | ref_or_null | salesRepEmployeeNumber | salesRepEmployeeNumber | 5       | const |   29 | Using where; Using index |
+----+-------------+-----------+-------------+------------------------+------------------------+---------+-------+------+--------------------------+
1 row in set (0.01 sec)
```
在此示例中，EXPLAIN显示ref_or_null应用优化的时间。

如果您有一个列组合键，MySQL可以对任何关键部分执行优化。假设列k1和k2表上有索引t1，以下查询正在利用索引：

```sql
SELECT 
    *
FROM
    t1
WHERE
    k1 IS NULL; 
```