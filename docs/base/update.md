MySQL UPDATE语句更新表中的数据。

MySQL UPDATE语句简介
您可以使用UPDATE语句更新表中的现有数据。您还可以使用UPDATE语句更改表中单行，一组行或所有行的列值。

以下说明了MySQL UPDATE语句的语法：

```sql
UPDATE [LOW_PRIORITY] [IGNORE] table_name 
SET 
    column_name1 = expr1,
    column_name2 = expr2,
    ...
[WHERE
    condition];
``` 
UPDATE声明：

首先，指定要在UPDATE关键字后更新数据的表名。
其次，SET子句指定要修改的列和新值。要更新多个列，请使用逗号分隔的分配列表。您可以以文字值，表达式或子查询的形式在每列的赋值中提供值。
第三，使用WHERE子句中的条件指定要更新的行。WHERE子句是可选项。如果省略WHERE子句，UPDATE语句将更新表中的所有行。
注意：  WHERE子句非常重要，您可能只想改变一行; 但是，您可能会忘记WHERE子句并意外更新表的所有行。

MySQL在UPDATE语句中支持两个修饰符。

LOW_PRIORITY修正指示UPDATE延迟更新，直到没有连接从表中读取数据的语句。LOW_PRIORITY只对表级锁的存储引擎使用生效，例如，MyISAM数据，MERGE，MEMORY。
IGNORE修改使UPDATE语句继续更新，即使发生错误的行。导致错误（例如重复键冲突）的行不会更新。
MySQL UPDATE示例
让我们UPDATE用MySQL示例数据库中的一些表来练习语句。

使用MySQL UPDATE修改单列中的值示例
在此示例中，我们将更新Mary Patterson新电子邮件的电子邮件mary.patterso@classicmodelcars.com。

首先，为了确保更新成功，我们employees使用以下SELECT语句从表中查询Mary的电子邮件：

```sql
SELECT 
    firstname, lastname, email
FROM
    employees
WHERE
    employeeNumber = 1056; 
```
运行结果：
```
+-----------+-----------+----------------------+
| firstname | lastname  | email                |
+-----------+-----------+----------------------+
| Mary      | Patterson | mpatterso@yiibai.com |
+-----------+-----------+----------------------+
1 row in set (0.02 sec)
```
其次，我们可以使用UPDATE以下查询中显示的语句将Mary的电子邮件更新为新电子邮件mary.patterson@classicmodelcars.com：

```sql
UPDATE employees 
SET 
    email = 'mary.patterson@classicmodelcars.com'
WHERE
    employeeNumber = 1056; 
```
因为我们只想更新一行，所以我们使用WHERE子句使用员工编号指定行  1056。SET子句将email列的值设置为新电子邮件。

第三，我们SELECT再次执行语句以验证更改。

```sql
SELECT 
    firstname, lastname, email
FROM
    employees
WHERE
    employeeNumber = 1056; 
```
运行结果：
```
+-----------+-----------+-------------------------------------+
| firstname | lastname  | email                               |
+-----------+-----------+-------------------------------------+
| Mary      | Patterson | mary.patterson@classicmodelcars.com |
+-----------+-----------+-------------------------------------+
1 row in set (0.00 sec)
```
使用MySQL UPDATE修改多列中的值
要更新多列中的值，您需要在SET子句中指定赋值。例如，以下语句更新员工编号1056的姓氏和电子邮件列：

```sql
UPDATE employees 
SET 
    lastname = 'Hill',
    email = 'mary.hill@classicmodelcars.com'
WHERE
    employeeNumber = 1056; 
```
我们来检查一下这些变化：

```
SELECT 
    firstname, lastname, email
FROM
    employees
WHERE
    employeeNumber = 1056; 
```
运行结果：
```
+-----------+----------+--------------------------------+
| firstname | lastname | email                          |
+-----------+----------+--------------------------------+
| Mary      | Hill     | mary.hill@classicmodelcars.com |
+-----------+----------+--------------------------------+
1 row in set (0.00 sec)
```
使用MySQL UPDATE更新SELECT语句返回的行
您可以使用子查询得到的值复制给指定的字段更新。

例如，在customers表中，一些客户没有任何销售代表。saleRepEmployeeNumber列的值是NULL如下：

```sql
SELECT 
    customername, salesRepEmployeeNumber
FROM
    customers
WHERE
    salesRepEmployeeNumber IS NULL; 
```
运行结果：
```
+------------------------------------+------------------------+
| customername                       | salesRepEmployeeNumber |
+------------------------------------+------------------------+
| Atelier graphique                  |                   NULL |
| Signal Gift Stores                 |                   NULL |
| Australian Collectors, Co.         |                   NULL |
| La Rochelle Gifts                  |                   NULL |
| Baane Mini Imports                 |                   NULL |
| Mini Gifts Distributors Ltd.       |                   NULL |
...
```
我们可以为这些客户提供销售代表并进行更新。

为此，我们可以选择一个随机员工，其职位名称Sales Rep来自employees表，并更新employees表  。

此查询从作业标题为的表中选择一个随机员工。employeesSales Rep

```sql
SELECT 
    employeeNumber
FROM
    employees
WHERE
    jobtitle = 'Sales Rep'
ORDER BY RAND()
LIMIT 1; 
```
要更新表中的销售代表员工编号列customers，我们将上面的查询放在UPDATE语句的SET子句中，如下所示：

```sql
UPDATE customers 
SET 
    salesRepEmployeeNumber = (SELECT 
            employeeNumber
        FROM
            employees
        WHERE
            jobtitle = 'Sales Rep'
        LIMIT 1)
WHERE
    salesRepEmployeeNumber IS NULL; 
```
如果您从employees表中查询数据   ，您将看到每个客户都有销售代表。换句话说，以下查询不返回任何行。

```sql
SELECT 
     salesRepEmployeeNumber
FROM
    customers
WHERE
    salesRepEmployeeNumber IS NOT NULL; 
```
