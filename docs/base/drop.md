MySQL DROP DATABASE语句删除服务器中的现有数据库。

DROP DATABASE语句删除数据库中的所有表并永久删除数据库。因此，使用此语句时应非常小心。

以下显示了DROP DATABASE语句的语法：
```
DROP DATABASE [IF EXISTS] database_name; 
```
在此语句中，指定要删除的数据库的名称。

如果您尝试删除不存在的数据库，MySQL将发出错误。

若要防止在删除不存在的数据库时发生错误，可以使用IF EXISTS选项。在这种情况下，MySQL终止语句而不发出任何错误。

DROP DATABASE语句返回已删除的表的数量。

在MySQL中，模式是数据库的同义词，因此，您可以互换使用它们：
```
DROP SCHEMA [IF EXISTS] database_name; 
```
在下一节中，我们将使用CREATE DATABASE教程中创建的testdb和testdb2。如果您没有这些数据库，则可以按照上一个教程创建它们。

MySQL DROP DATABASE使用mysql程序示例
首先，使用root用户登录MySQL服务器。请注意，您可以使用自己的数据库用户而不是root用户。
```
>mysql -u root -p
Enter password: ******** 
```
root输入用户的密码，然后按Enter键。

其次，使用SHOW DATABASES语句查看服务器中的所有现有数据库：
```
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| mysqldemo          |
| performance_schema |
| sys                |
| testdb             |
| testdb2            |
+--------------------+
7 rows in set (0.00 sec)
```
三，发表DROP DATABASE声明：
```
mysql> DROP DATABASE testdb; 
Query OK, 0 rows affected (0.03 sec)
```
MySQL返回零受影响的行。这意味着testdb数据库没有表。