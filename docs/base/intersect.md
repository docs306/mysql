MySQL INTERSECT运算符。

SQL INTERSECT 运算符简介
INTERSECT运算符是一个集合运算符仅返回两个查询或多个查询的不同行。

以下说明了INTERSECT运算符的语法。
```
(SELECT column_list 
FROM table_1)
INTERSECT
(SELECT column_list
FROM table_2); 
```
INTERSECT运算符比较两个查询的结果，并返回作为由左，右输出的查询的不同的行。

要将INTERSECT运算符用于两个查询，应用以下规则：

列的顺序和数量必须相同。
相应列的数据类型必须兼容。
下图说明了INTERSECT操作员。

MySQL INTERSECT

左查询生成（1,2,3）的结果集。

正确的查询返回结果集（2,3,4）。

INTERSECT返回包括两者的结果集（2,3）的不同的行。

与UNION运算符不同，运算INTERSECT符返回两个圆之间的交集。

需要注意的是SQL标准有三个集操作符，其中包括  UNION，INTERSECT，和MINUS。

MySQL INTERSECT 模拟
不幸的是，MySQL不支持INTERSECT操作符。但是，您可以模拟INTERSECT 运算符。

让我们为演示创建一些示例数据。

下面的语句创建 表t1和t2，然后将数据插入到这两个表。
```
CREATE TABLE t1 (
    id INT PRIMARY KEY
);
 
CREATE TABLE t2 LIKE t1;
 
INSERT INTO t1(id) VALUES(1),(2),(3);
 
INSERT INTO t2(id) VALUES(2),(3),(4); 
```
以下查询返回t1表中的行。
```
SELECT id
FROM t1; 
```
运行结果：
```
+----+
| id |
+----+
|  1 |
|  2 |
|  3 |
+----+
3 rows in set (0.04 sec)
```
以下查询返回表中的行t2：
```
SELECT id
FROM t2; 
```
运行结果：
```
+----+
| id |
+----+
| 2  |
| 3  |
| 4  |
+----+
3 rows in set (0.01 sec)
```
使用DISTINCT运算符和INNER JOIN子句模拟MySQL INTERSECT运算符。
以下语句使用DISTINCT和INNER JOIN子句返回两个表中的不同行：

```sql
SELECT DISTINCT 
   id 
FROM t1
   INNER JOIN t2 USING(id); 
```
运行结果：
```
+----+
| id |
+----+
|  2 |
|  3 |
+----+
2 rows in set, 3 warnings (0.00 sec)
```
这个怎么运作。

INNER JOIN子句从左表和右表返回行。
DISTINCT操作将删除重复的行。
使用IN运算符和子查询模拟MySQL INTERSECT运算符
以下语句使用IN运算符和子查询返回两个结果集的交集。

```sql
SELECT DISTINCT
    id
FROM
    t1
WHERE
    id IN (SELECT 
            id
        FROM
            t2); 
```
运行结果：
```
+----+
| id |
+----+
|  2 |
|  3 |
+----+
2 rows in set, 3 warnings (0.00 sec)
```
这个怎么运作。

子查询返回第一个结果集。
外部查询使用IN运算符仅选择第一个结果集中的值。DISTINCT可确保只有不同的值被选择。