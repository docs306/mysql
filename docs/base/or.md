MySQL OR运算符组合布尔表达式来过滤数据。

MySQL OR运算符简介
MySQL OR运算符组合两个布尔表达式，当任一条件为真时返回true。

以是OR运算符的语法。

```sql
boolean_expression_1 OR boolean_expression_2 
```
这两个  boolean_expression_1和boolean_expression_2 是返回真，假，或NULL布尔表达式。

下表显示了OR运算符的结果。

```sql
TRUE	FALSE	NULL
TRUE	TRUE	TRUE	TRUE
FALSE	TRUE	FALSE	NULL
NULL	TRUE	NULL	NULL
```
MySQL OR 短路评估
MySQL对OR运营商使用短路评估。换句话说，当MySQL可以确定结果时，它会停止评估语句的其余部分。

请参阅以下示例。

```sql
SELECT 1 = 1 OR 1 / 0; 
```
运行结果：
```
+----------------+
| 1 = 1 OR 1 / 0 |
+----------------+
|              1 |
+----------------+
1 row in set (0.00 sec)
```
因为表达式1 = 1总是返回true，所以MySQL不会再评估1 / 0.如果确实如此，它会因为零除错误而发出错误。

运算符优先级
当您在表达式中使用多个逻辑运算符时，MySQL始终会在AND运算符之后计算OR运算符。这称为运算符优先级，它确定运算符的执行顺序。MySQL首先评估具有更高优先级的运算符。

请参阅以下示例。

```sql
SELECT true OR false AND false; 
```
运行结果：

```sql
| true OR false AND false |
+-------------------------+
|                       1 |
+-------------------------+
1 row in set (0.00 sec)
```
这个怎么运作

首先，MySQL评估AND运算符，因此表达式  false AND false返回false。
其次，MySQL评估OR运算符，因此表达式true OR false返回true。
要更改评估顺序，请使用括号，例如：

```sql
SELECT (true OR false) AND false; 
```
运行结果：

```sql
+---------------------------+
| (true OR false) AND false |
+---------------------------+
|                         0 |
+---------------------------+
1 row in set (0.00 sec)
```
这个怎么运作

首先，MySQL评估括号中的表达式(true OR false)返回true
其次，MySQL评估语句的剩余部分，true AND false返回false。
MySQL OR运算符示例
我们将使用示例数据库中的   customers表进行演示。

```
+------------------------+
| customers              |
+------------------------+
| customerNumber         |
| customerName           |
| contactLastName        |
| contactFirstName       |
| phone                  |
| addressLine1           |
| addressLine2           |
| city                   |
| state                  |
| postalCode             |
| country                |
| salesRepEmployeeNumber |
| creditLimit            |
+------------------------+
13 rows in set (0.01 sec)
```
例如，要获得位于美国或法国的客户，请在WHERE子句中使用OR运算符：

```sql
SELECT    
 customername, 
 country
FROM    
 customers
WHERE country = 'USA'
 OR country = 'France'; 
```
运行结果：

```sql
+------------------------------+---------+
| customername                 | country |
+------------------------------+---------+
| Atelier graphique            | France  |
| Signal Gift Stores           | USA     |
| La Rochelle Gifts            | France  |
| Mini Gifts Distributors Ltd. | USA     |
| Mini Wheels Co.              | USA     |
| Land of Toys Inc.            | USA     |
```
正如您可以看到结果一样，查询将返回位于美国或法国的客户。

以下实例返回位于美国或法国且信用额度大于10,000的客户。

```sql
SELECT 
  customername, 
  country, 
  creditLimit
FROM 
  customers
WHERE(country = 'USA'
  OR country = 'France')
  AND creditlimit > 100000; 
```
运行结果：

```
+------------------------------+---------+-------------+
| customername                 | country | creditLimit |
+------------------------------+---------+-------------+
| La Rochelle Gifts            | France  |   118200.00 |
| Mini Gifts Distributors Ltd. | USA     |   210500.00 |
| Land of Toys Inc.            | USA     |   114900.00 |
| Saveley & Henriot, Co.       | France  |   123900.00 |
| Muscle Machine Inc           | USA     |   138500.00 |
| Diecast Classics Inc.        | USA     |   100600.00 |
| Collectable Mini Designs Co. | USA     |   105000.00 |
| Marta's Replicas Co.         | USA     |   123700.00 |
| Mini Classics                | USA     |   102700.00 |
| Corporate Gift Ideas Co.     | USA     |   105000.00 |
| Online Diecast Creations Co. | USA     |   114200.00 |
+------------------------------+---------+-------------+
11 rows in set (0.02 sec)
```
请注意，如果您不使用括号，查询将返回位于美国的客户或位于法国且信用额度大于10,000的客户。

```sql
SELECT    
 customername, 
 country, 
 creditLimit
FROM    
 customers
WHERE country = 'USA'
 OR country = 'France'
 AND creditlimit > 100000; 
```
运行结果：

```
+------------------------------+---------+-------------+
| customername                 | country | creditLimit |
+------------------------------+---------+-------------+
| Signal Gift Stores           | USA     |    71800.00 |
| La Rochelle Gifts            | France  |   118200.00 |
| Mini Gifts Distributors Ltd. | USA     |   210500.00 |
| Mini Wheels Co.              | USA     |    64600.00 |
| Land of Toys Inc.            | USA     |   114900.00 |
| Saveley & Henriot, Co.       | France  |   123900.00 |
...
```