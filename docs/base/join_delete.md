# DELETE JOIN


DELETE JOIN 与 INNER JOIN
MySQL还允许您使用 INNER JOIN语句中的 子句DELETE删除表中的行和另一个表中的匹配行。

例如，要删除两行T1，并T2满足指定条件表：
```sql
DELETE T1, T2 FROM T1
INNER JOIN T2 ON T1.key = T2.key
WHERE condition; 
```
注：把表名T1和T2放在 DELETE和 FROM 关键字之间。如果省略T1表，则DELETE语句仅删除T2表中的行。同样，如果省略T2表，则DELETE语句将仅删除T1表中的行。

表达式T1.key = T2.key指定匹配行T1和T2将删除的表的条件。

中条件WHERE子句确定的行中T1和T2，将被删除。

MySQL DELETE JOIN的INNER JOIN例子
假设，我们有两个表，t1并t2具有以下结构和数据：

```sql
DROP TABLE IF EXISTS t1, t2;
 
CREATE TABLE t1 (id INT PRIMARY KEY AUTO_INCREMENT);
 
CREATE TABLE t2 (id VARCHAR(20) PRIMARY KEY, ref INT NOT NULL);
 
INSERT INTO t1 VALUES (1),(2),(3);
 
INSERT INTO t2(id,ref) VALUES('A',1),('B',2),('C',3);
```

以下语句删除t1表中id为1的行，并使用 DELETE...INNER JOIN语句删除在t2表中 ref = 1的行：
```sql
DELETE t1,t2 FROM t1
INNER JOIN t2 ON t2.ref = t1.id
WHERE t1.id = 1;
```
声明返回以下消息：2 row(s) affected 表明已删除了两行。

### 使用LEFT JOIN删除JOIN
经常使用 SELECT语句中的 LEFT JOIN子句来查找左表中的行，这些行在右表中有或没有匹配的行。

还可以使用 DELETE语句中的 LEFT JOIN子句删除表（左表）中的行，这些行在另一个表（右表）中没有匹配的行。

以下语法使用 DELETE语句中 LEFT JOIN子句从T1表中删除T2表中没有相应行的行：
```sql
DELETE T1 FROM T1
LEFT JOIN T2 ON T1.key = T2.key 
WHERE T2.key IS NULL; 
```
注：只把T1表放在 DELETE的关键字后面，而不像使用 INNER JOIN子句那样T1和T2两个表。

**DELETE JOIN 与 LEFT JOIN**

customers和orders表：客户和订单表
每个客户都有零个或多个订单，每个订单属于一个且只有一个客户。

可以使用 DELETE和 LEFT JOIN子句来清理客户主数据。
```sql
DELETE customers FROM customers
LEFT JOIN orders o ON customers.customerNumber = o.customerNumber 
WHERE o.orderNumber IS NULL; 
```

查找没有订单的客户并删除：
```sql
SELECT c.customerNumber, c.customerName, o.orderNumber 
FROM customers c
LEFT JOIN orders o ON c.customerNumber = o.customerNumber
WHERE o.orderNumber IS NULL; 
```