MySQL序列自动为表列的ID列生成唯一编号。

创建MySQL序列
在MySQL中，序列是按升序生成的整数列表，即1,2,3 ......许多应用程序需要序列来生成唯一的数字，主要用于识别，例如CRM中的客户ID，HR中的员工编号和设备编号。服务管理系统。

要自动在MySQL中创建序列，请将AUTO_INCREMENT属性设置为列，列通常是主键列。

使用AUTO_INCREMENT属性时将应用以下规则  ：

每个表只有一AUTO_INCREMENT列，其数据类型通常是整数。
- AUTO_INCREMENT列必须编入索引，这意味着它可以是索引PRIMARY KEY或UNIQUE索引。
- AUTO_INCREMENT列必须具有  NOT NULL约束。将  AUTO_INCREMENT属性设置为列时，MySQL会自动将NOT NULL  约束隐式添加  到列中。
创建MySQL序列示例
下面的语句创建一个表名为  employees 具有  emp_no列是  AUTO_INCREMENT列：
```
CREATE TABLE employees (
    emp_no INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(50),
    last_name VARCHAR(50)
); 
```
MySQL序列如何工作
AUTO_INCREMENT列具有以下属性：

- AUTO_INCREMENT列的起始值为1，当您NULL向列中插入值或在INSERT语句中省略其值时，它会增加1  。
- 要获取最后生成的序列号，请使用LAST_INSERT_ID()  函数。我们经常使用后续语句的最后一个插入ID，例如，将数据插入表中。最后生成的序列在会话中是唯一的。换句话说，如果另一个连接生成序列号，则可以通过连接使用LAST_INSERT_ID()函数获取序列号。
- 如果在表中插入新行并为序列列指定值，则如果列中不存在序列号，则MySQL将插入序列号，如果已存在则发出错误。如果插入一个大于下一个序列号的新值，MySQL将使用新值作为起始序列号，并生成一个大于当前序列号的唯一序列号，以供下次使用。这会在序列中产生间隙。
- 如果使用UPDATE语句将AUTO_INCREMENT列中的值更新  为已存在的值，则如果列具有唯一索引，MySQL将发出重复键错误。如果将AUTO_INCREMENT列更新为大于列中现有值的值，MySQL将使用下一行的最后一个插入序列号的下一个数字。例如，如果最后一个插入序列号为3，则将其更新为10，新行的序列号为4。
- 如果使用DELETE语句删除最后插入的行，MySQL可能会也可能不会重复使用已删除的序列号，具体取决于表的存储引擎。如果删除一行，MyISAM表不会重复使用已删除的序列号，例如，表中的最后一个插入ID是10，如果删除它，MySQL仍会生成下一个序列号，即新行的序列号为11。与MyISAM表类似，InnoDB表在删除行时不重用序列号。
- 为列设置 AUTO_INCREMENT 属性后，可以通过各种方式重置自动增量值，例如，使用ALTER TABLE语句。

让我们看一些例子来更好地理解MySQL序列。

首先，在employees表中插入两个新行：
```
INSERT INTO employees(first_name,last_name)
VALUES('John','Doe'),
      ('Mary','Jane'); 
```
其次，从employees表中选择数据：
```
SELECT * FROM employees; 
+--------+------------+-----------+
| emp_no | first_name | last_name |
+--------+------------+-----------+
|      1 | John       | Doe       |
|      2 | Mary       | Jane      |
+--------+------------+-----------+
2 rows in set (0.00 sec)
```
第三，删除第二名员工，emp_no即2：
```
DELETE FROM employees 
WHERE
    emp_no = 2; 
mysql> SELECT * FROM employees;
+--------+------------+-----------+
| emp_no | first_name | last_name |
+--------+------------+-----------+
|      1 | John       | Doe       |
+--------+------------+-----------+
1 row in set (0.00 sec)
```
第四，插入一名新员工：
```
INSERT INTO employees(first_name,last_name)
VALUES('Jack','Lee'); 
mysql> SELECT * FROM employees;
+--------+------------+-----------+
| emp_no | first_name | last_name |
+--------+------------+-----------+
|      1 | John       | Doe       |
|      3 | Jack       | Lee       |
+--------+------------+-----------+
2 rows in set (0.00 sec)
```
因为employees表的存储引擎是InnoDB，所以它不会重用已删除的序列号。新行emp_no  为3。

第五，将emp_no 为3员工更新为1 ：
```
UPDATE employees 
SET 
    first_name = 'Joe',
    emp_no = 1
WHERE
    emp_no = 3; 
```
MySQL发出主键重复输入错误。
```
ERROR 1062 (23000): Duplicate entry '1' for key 'PRIMARY'
```
我们来解决它。
```
UPDATE employees 
SET 
    first_name = 'Joe',
    emp_no = 10
WHERE
    emp_no = 3; 
mysql> select * from employees;
+--------+------------+-----------+
| emp_no | first_name | last_name |
+--------+------------+-----------+
|      1 | John       | Doe       |
|     10 | Joe        | Lee       |
+--------+------------+-----------+
2 rows in set (0.00 sec)
```
第六，在将序列号更新为10后插入新员工：
```
INSERT INTO employees(first_name,last_name)
VALUES('Wang','Lee'); 
mysql> select * from employees;
+--------+------------+-----------+
| emp_no | first_name | last_name |
+--------+------------+-----------+
|      1 | John       | Doe       |
|      4 | Wang       | Lee       |
|     10 | Joe        | Lee       |
+--------+------------+-----------+
3 rows in set (0.00 sec)
```
最后一个插入的下一个序列号是4，因此，MySQL使用4号作为新行而不是11行。

