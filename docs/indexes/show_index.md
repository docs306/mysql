MySQL 显示索引

MySQL SHOW INDEXES命令简介
要查询表的索引信息，请使用以下SHOW INDEXES语句：
```
SHOW INDEXES FROM table_name; 
```
要获取表的索引，请在FROM关键字后指定表名。语句将返回与当前数据库中的表关联的索引信息。

如果未连接到任何数据库，或者要获取其他数据库中表的索引信息，则可以指定数据库名称：
```
SHOW INDEXES FROM table_name 
IN database_name; 
```
以下查询与上面的查询类似：
```
SHOW INDEXES FROM database_name.table_name; 
```
注意，INDEX和KEY是INDEXES同义词，IN是FROM同义词，因此，您可以在SHOW INDEXES列中使用这些同义词。例如：
```
SHOW INDEX IN table_name 
FROM database_name; 
```
要么
```
SHOW KEY FROM tablename
IN databasename; 
```
将SHOW INDEXES返回以下信息：

table
表的名称

NON_UNIQUE
如果索引可以包含重复项，则为1;如果可以，则为0。

KEY_NAME
索引的名称。主键索引始终具有PRIMARY名称。

seq_in_index
索引中的列序列号。第一列序列号从1开始。

column_name
列名称

collation
排序规则表示列在索引中的排序方式。A表示升序，B表示降序或NULL表示未分类。

cardinality
基数返回索引中估计的唯一值数。

请注意，基数越高，查询优化器使用索引进行查找的可能性就越大。

sub_part
索引前缀。如果对整个列编制索引，则为null。否则，它会显示部分索引列的索引字符数。

packed
表示密钥是如何打包的; NUL，如果不是。

null
YES 如果列可能包含NULL值，如果不包含空值则为空。

INDEX_TYPE
表示使用诸如索引方法BTREE，HASH，RTREE，或FULLTEXT。

comment
有关索引的信息未在其自己的列中描述。

index_comment
显示使用COMMENT属性创建索引时指定的索引的注释。

visible
索引是否对查询优化器可见或不可见; YES如果是，NO如果不是。

expression
如果索引使用表达式而不是列或列前缀值，则表达式指示键部分的表达式，并且column_name列也为NULL。

过滤索引信息
要过滤索引信息，请使用以下WHERE子句：
```
SHOW INDEXES FROM table_name
WHERE condition; 
```
您可以使用SHOW INDEXES语句返回的任何信息来过滤索引信息。例如，以下语句仅返回表的不可见索引：
```
SHOW INDEXES FROM table_name
WHERE VISIBLE = 'NO'; 
```
MySQL的SHOW INDEXES例子
我们将创建一个新表名为contacts演示SHOW INDEXES命令：
```
CREATE TABLE contacts(
    contact_id INT AUTO_INCREMENT,
    first_name VARCHAR(100) NOT NULL,
    last_name VARCHAR(100) NOT NULL,
    email VARCHAR(100),
    phone VARCHAR(20),
    PRIMARY KEY(contact_id),
    UNIQUE(email),
    INDEX phone(phone) INVISIBLE,
    INDEX name(first_name, last_name) comment "By first name and last name"
); 
```
以下命令返回表中的所有索引信息contacts：
```
SHOW INDEXES FROM contacts; 
```
输出是：
```
+----------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+-----------------------------+---------+------------+
| Table    | Non_unique | Key_name | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment               | Visible | Expression |
+----------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+-----------------------------+---------+------------+
| contacts |          0 | PRIMARY  |            1 | contact_id  | A         |           0 |     NULL |   NULL |      | BTREE      |         |                             | YES     | NULL       |
| contacts |          0 | email    |            1 | email       | A         |           0 |     NULL |   NULL | YES  | BTREE      |         |                             | YES     | NULL       |
| contacts |          1 | phone    |            1 | phone       | A         |           0 |     NULL |   NULL | YES  | BTREE      |         |                             | NO      | NULL       |
| contacts |          1 | name     |            1 | first_name  | A         |           0 |     NULL |   NULL |      | BTREE      |         | By first name and last name | YES     | NULL       |
| contacts |          1 | name     |            2 | last_name   | A         |           0 |     NULL |   NULL |      | BTREE      |         | By first name and last name | YES     | NULL       |
+----------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+-----------------------------+---------+------------+
5 rows in set (0.00 sec)
```
要获取contacts表的不可见索引，请添加WHERE如下子句：
```
SHOW INDEXES FROM contacts
WHERE visible = 'NO'; 
```
这是输出：
```
+----------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| Table    | Non_unique | Key_name | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment | Visible | Expression |
+----------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| contacts |          1 | phone    |            1 | phone       | A         |           0 |     NULL |   NULL | YES  | BTREE      |         |               | NO      | NULL       |
+----------+------------+----------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
1 row in set (0.01 sec)
```