MySQL 唯一索引

MySQL UNIQUE索引简介
要强制执行一列或多列的唯一性值，通常使用PRIMARY KEY约束。但是，每个表只能有一个主键。因此，如果要包含多个列或一组具有唯一值的列，则不能使用主键约束。

幸运的是，MySQL提供了另一种指标叫做UNIQUE索引，使您可以强制执行值的唯一的一列或多列。与PRIMARY KEY 索引不同，UNIQUE每个表可以有多个索引。

要创建UNIQUE索引，请使用以下CREATE UNIQUE INDEX语句：
```
CREATE UNIQUE INDEX index_name
ON table_name(index_column_1,index_column_2,...);
``` 
在一列或多列中强制执行值唯一性的另一种方法是使用UNIQUE约束。

创建UNIQUE约束时，MySQL会UNIQUE在幕后创建索引。

以下语句说明了在创建表时如何创建唯一约束。
```
CREATE TABLE table_name(
...
   UNIQUE KEY(index_column_,index_column_2,...) 
); 
```
在此语句中，您还可以使用UNIQUE INDEX而不是UNIQUE KEY因为它们是同义词。

如果要向现有表添加唯一约束，可以使用以下ALTER TABLE语句：
```
ALTER TABLE table_name
ADD CONSTRAINT constraint_name UNIQUE KEY(column_1,column_2,...); 
MySQL UNIQUE Index＆NULL
```
与其他数据库系统不同，MySQL将NULL值视为不同的值。因此，您可以在UNIQUE索引中包含多个NULL值。

这就是MySQL的设计方式。即使它被报告为错误，它也不是错误。

另一个重要的一点是，UNIQUE除了BDB 存储引擎之外，约束不适用于NULL值。

MySQL UNIQUE索引示例
假设您要管理应用程序中的联系人。您还希望contacts表中每个联系人的电子邮件必须是唯一的。

要强制执行此规则，请在CREATE TABLE语句中创建唯一约束，如下所示：
```
DROP TABLE contacts;
CREATE TABLE IF NOT EXISTS contacts (
    id INT AUTO_INCREMENT PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    phone VARCHAR(15) NOT NULL,
    email VARCHAR(100) NOT NULL,
    UNIQUE KEY unique_email (email)
); 
```
如果使用SHOW INDEXES语句，您将看到My​​SQL UNIQUE为email列创建了索引。
```
SHOW INDEXES FROM contacts; 
+----------+------------+--------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| Table    | Non_unique | Key_name     | Seq_in_index | Column_name | Collation | Cardinality | Sub_part | Packed | Null | Index_type | Comment | Index_comment | Visible | Expression |
+----------+------------+--------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
| contacts |          0 | PRIMARY      |            1 | id          | A         |           0 |     NULL |   NULL |      | BTREE      |         |               | YES     | NULL       |
| contacts |          0 | unique_email |            1 | email       | A         |           0 |     NULL |   NULL |      | BTREE      |         |               | YES     | NULL       |
+----------+------------+--------------+--------------+-------------+-----------+-------------+----------+--------+------+------------+---------+---------------+---------+------------+
2 rows in set (0.04 sec)
```
让我们在contacts表格中插入一行。
```
INSERT INTO contacts(first_name,last_name,phone,email)
VALUES('John','Doe','(408)-999-9765','john.doe@mysqltutorial.org'); 
```
现在，如果您尝试插入其电子邮件所在的行john.doe@mysqltutorial.org，您将收到错误消息。
```
INSERT INTO contacts(first_name,last_name,phone,email)
VALUES('Johny','Doe','(408)-999-4321','john.doe@mysqltutorial.org'); 
ERROR 1062 (23000): Duplicate entry 'john.doe@mysqltutorial.org' for key 'unique_email'
```
假设你想要的组合first_name，last_name和   phone也接触中是唯一的。在这种情况下，您可以使用CREATE INDEX语句UNIQUE为这些列创建索引，如下所示：
```
CREATE UNIQUE INDEX idx_name_phone
ON contacts(first_name,last_name,phone); 
```
添加下面一行到contacts因为组合表会导致错误first_name，last_name以及phone已经存在。
```
INSERT INTO contacts(first_name,last_name,phone,email)
VALUES('john','doe','(408)-999-9765','john.d@mysqltutorial.org'); 
ERROR 1062 (23000): Duplicate entry 'john-doe-(408)-999-9765' for key 'idx_name_phone'
```