### MySQL循环语句
允许您根据条件重复执行一段SQL代码。有三种循环语句在MySQL： WHILE，REPEAT 和LOOP。

### WHILE循环

语句的语法如下：
```sql
WHILE expression DO
   statements
END WHILE 
```
WHILE 循环检查expression在每次迭代的开始。如果expression评估为TRUE，MySQL将 在评估statements之间执行  WHILE，END WHILE直到expression评估为止FALSE。WHILE 循环称为预测试循环，因为它在statements 执行之前检查表达式。

以下是WHILE 在存储过程中使用循环语句的示例：
```sql
DELIMITER $$
DROP PROCEDURE IF EXISTS test_mysql_while_loop$$ 
CREATE PROCEDURE test_mysql_while_loop ( ) 
	BEGIN
	DECLARE x INT;
	DECLARE str VARCHAR ( 255 );
	SET x = 1;
	SET str = '';
	WHILE x <= 5 DO
		SET str = CONCAT( str, x, ',' );
		SET x = x + 1;
	END WHILE;
	SELECT str;
END $$
DELIMITER ; 
```
在test_mysql_while_loop上面的存储过程中：

- 首先，我们str 重复构建字符串，直到x 变量的值  大于5。
- 然后，我们使用SELECT语句显示最终字符串。
- 请注意，如果我们不初始化  x变量，则其默认值为NULL。因此，WHILE循环语句中的条件始终是TRUE 并且您将具有无限循环，这不是期望的。

我们来测试一下test_mysql_while_loop存储过程：
```sql
CALL test_mysql_while_loop(); 
+------------+
| str        |
+------------+
| 1,2,3,4,5, |
+------------+
1 row in set (0.01 sec)
```

### REPEAT循环

语法如下：
```
REPEAT
 statements;
UNTIL expression
END REPEAT 
```
首先，MySQL执行statements，然后评估expression。如果expression评估为FALSE，则MySQL statements 重复执行直到expression 评估为止TRUE。

因为REPEAT 循环语句expression 在执行后检查statements，所以REPEAT循环语句也称为测试后循环。

以下流程图说明了REPEAT循环语句：
我们可以test_mysql_while_loop使用WHILE loop语句重写上面使用REPEAT loop语句的存储过程：
```sql
DELIMITER $$
DROP PROCEDURE IF EXISTS mysql_test_repeat_loop $$ 
CREATE PROCEDURE mysql_test_repeat_loop ( ) BEGIN
	DECLARE x INT;
	DECLARE str VARCHAR ( 255 );
	SET x = 1;
	SET str = '';
	REPEAT
		SET str = CONCAT( str, x, ',' );
		SET x = x + 1;
	UNTIL x > 5 
	END REPEAT;
	SELECT str;
END $$
DELIMITER ; 
```
注意UNTIL 表达式中没有分号（;）。
```
CALL mysql_test_repeat_loop(); 
+------------+
| str        |
+------------+
| 1,2,3,4,5, |
+------------+
1 row in set (0.00 sec)
```
LOOP，LEAVE和ITERATE语句
有两个语句允许您控制循环：

- LEAVE语句允许您立即退出循环而无需等待检查条件。LEAVE语句的作用类似于PHP，C / C ++和Java等其他语言中的 break 语句。
- ITERATE语句允许您跳过其下的整个代码并开始新的迭代。ITERATE语句类似于PHP，C / C ++和Java中的continue语句。
- MySQL还为您提供了LOOP一个重复执行代码块的语句，并具有使用循环标签的额外灵活性。

以下是使用LOOP 循环语句的示例：
```
DELIMITER $$
DROP PROCEDURE IF EXISTS test_mysql_loop $$ 
CREATE PROCEDURE test_mysql_loop() 
BEGIN
  DECLARE x INT;
  DECLARE str VARCHAR ( 255 );
  SET x = 1;
  SET str = '';
  loop_label :LOOP
    IF x > 10 THEN
      LEAVE loop_label;
    END IF;
    SET x = x + 1;
    IF ( x MOD 2 ) THEN
      ITERATE loop_label;
    ELSE 
      SET str = CONCAT( str, x, ',' );
    END IF;
  END LOOP;
  SELECT str;
END $$
DELIMITER ; 
mysql> call test_mysql_loop();
+-------------+
| str         |
+-------------+
| 2,4,6,8,10, |
+-------------+
1 row in set (0.00 sec)
```
在这个例子中，
存储过程仅构造具有偶数的字符串，例如，2,4和6。
我们loop_label  在LOOP声明之前放置了一个循环标签。
如果值  x 大于10，则由于LEAVE语句而终止循环。
如果the的值x 是奇数，则ITERATE 语句忽略其下的所有内容并开始新的迭代。
如果the的值x 是偶数，则ELSE语句中的块将构建具有偶数的字符串。