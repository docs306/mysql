### MySQL存储过程参数简介
您开发的存储过程几乎都需要参数。这些参数使存储过程更加灵活和有用。在MySQL中，参数具有以下三种模式之一：  IN,OUT或INOUT。

- IN - 是默认模式。IN在存储过程中定义参数时，调用程序必须将参数传递给存储过程。此外，IN参数的值受到保护。这意味着即使IN参数的值在存储过程内部发生更改，其原始值也会在存储过程结束后保留​​。换句话说，存储过程仅适用于IN参数的副本。
- OUT- OUT可以在存储过程内更改参数的值，并将其新值传递回调用程序。请注意，存储过程OUT在启动时无法访问参数的初始值。
- INOUT- INOUT  参数是IN  和OUT  参数的组合。这意味着调用程序可以传递参数，并且存储过程可以修改INOUT参数，并将新值传递回调用程序。
在存储过程中定义参数的语法如下：

- MODE param_name param_type(param_size) 
- MODE可能是IN，OUT或INOUT，根据在存储过程中的参数的目的。
- 这param_name是参数的名称。参数的名称必须遵循MySQL中列名的命名规则。
- 遵循参数名称是其数据类型和大小。与变量一样，参数的数据类型可以是任何有效的  MySQL数据类型。
- 如果存储过程具有多个参数，则每个参数由逗号(,) 分隔。

让我们通过一些例子来练习，以便更好地理解。 我们将使用示例数据库中的表进行演示。

### IN参数示例
以下示例说明如何在GetOfficeByCountry存储过程中使用IN参数来选择位于特定国家/地区的办事处。
```
DELIMITER //
CREATE PROCEDURE GetOfficeByCountry(IN countryName VARCHAR(255))
 BEGIN
 SELECT officeCode,city,phone 
 FROM offices
 WHERE country = countryName;
 END //
DELIMITER ; 
```
这countryName是IN存储过程的参数。在存储过程中，我们选择位于countryName参数指定的国家/地区的所有办公室。

假设，我们希望在美国获得所有办公室，我们只需要将值（USA）传递给存储过程，如下所示：
```
CALL GetOfficeByCountry('USA'); 
+------------+---------------+-----------------+
| officeCode | city          | phone           |
+------------+---------------+-----------------+
| 1          | San Francisco | +1 650 219 4782 |
| 2          | Boston        | +1 215 837 0825 |
| 3          | NYC           | +1 212 555 3000 |
+------------+---------------+-----------------+
3 rows in set (0.00 sec)
```
要获取法国的所有办事处，我们将France文字字符串传递给GetOfficeByCountry 存储过程，如下所示：
```
CALL GetOfficeByCountry('France') 
+------------+-------+-----------------+
| officeCode | city  | phone           |
+------------+-------+-----------------+
| 4          | Paris | +33 14 723 4404 |
+------------+-------+-----------------+
1 row in set (0.00 sec)
```
OUT参数示例
以下存储过程按订单状态返回订单数。它有两个参数：

- orderStatus：  IN参数，即我们要计算订单的订单状态。
- total：OUT存储特定订单状态的订单数的  参数。
以下是CountOrderByStatus存储过程的源代码。
```
DELIMITER $$
CREATE PROCEDURE CountOrderByStatus(
 IN orderStatus VARCHAR(25),
 OUT total INT)
BEGIN
 SELECT count(orderNumber)
 INTO total
 FROM orders
 WHERE status = orderStatus;
END$$
DELIMITER ; 
```
为了获得已发货的订单数量，我们调用CountOrderByStatus  存储过程并将订单状态作为Shipped传递值，并传递参数（@total）以获取返回值。
```
CALL CountOrderByStatus('Shipped',@total);
SELECT @total; 
+--------+
| @total |
+--------+
|    303 |
+--------+
1 row in set (0.00 sec)
```
要获取进程中的订单数，我们CountOrderByStatus按如下方式调用存储过程：
```
CALL CountOrderByStatus('in process',@total);
SELECT @total AS  total_in_process; 
+------------------+
| total_in_process |
+------------------+
|                7 |
+------------------+
1 row in set (0.00 sec)
```
INOUT参数示例
以下示例演示如何INOUT在存储过程中使用  参数。
```
DELIMITER $$
CREATE PROCEDURE set_counter(INOUT count INT(4),IN inc INT(4))
BEGIN
 SET count = count + inc;
END$$
DELIMITER ; 
```
这个怎么运作。

- set_counter  存储过程接受一个INOUT  参数（count）和一个IN参数（inc）。
- 在存储过程中，我们通过inc参数的值增加counter(count) 。
看看我们如何调用set_counter  存储过程：
```
SET @counter = 1;
CALL set_counter(@counter,1); -- 2
CALL set_counter(@counter,1); -- 3
CALL set_counter(@counter,5); -- 8
SELECT @counter; -- 8 
```