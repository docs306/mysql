# SIGNAL / RESIGNAL语句引发错误条件

- SIGNAL 语句从存储的程序（例如存储过程，存储函数，触发器或事件）向调用者返回错误或警告条件。
- SIGNAL 语句使您可以控制返回的信息，例如SQLSTATE值和消息。

以下说明了SIGNAL语句的语法：
```
SIGNAL SQLSTATE | condition_name;
SET condition_information_item_name_1 = value_1,
    condition_information_item_name_1 = value_2, etc; 
```
继SIGNAL关键字是一个SQLSTATE值或声明的条件名称 DECLARE CONDITION声明。请注意，SIGNAL语句必须始终指定使用值定义的 SQLSTATE值或命名条件  SQLSTATE。

要为调用者提供信息，请使用SET子句。如果要使用值返回多个条件信息项名称，则需要用逗号分隔每个名称/值对。

condition_information_item_name可MESSAGE_TEXT，MYSQL_ERRORNO，CURSOR_NAME，等。

以下存储过程将订单行项目添加到现有销售订单中。如果订单号不存在，它会发出错误消息。
```
DELIMITER $$
CREATE PROCEDURE AddOrderItem(
	in orderNo int,
	in productCode varchar(45),
	in qty int,
	in price double,
	in lineNo int )
BEGIN
	DECLARE C INT;
	SELECT COUNT(orderNumber) INTO C FROM orders WHERE orderNumber = orderNo;
	-- check if orderNumber exists
	IF(C != 1) THEN
		SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'Order No not found in orders table';
	END IF;
-- more code below
-- ...
END $$
DELIMITER ; 
```
首先，它使用我们传递给存储过程的输入订单号来计算订单。

其次，如果订单数不是1，则会引发错误， 并显示SQLSTATE 45000错误消息，表明订单表中不存在订单号。

请注意，这45000是一个通用SQLSTATE值，用于说明未处理的用户定义的异常。

如果我们调用存储过程  AddOrderItem()并传递一个不存在的订单号，我们将收到一条错误消息。
```
CALL AddOrderItem(10,'S10_1678',1,95.7,1); 
ERROR 1644 (45000): Order No not found in orders table
```

### MySQL RESIGNAL 语句
除了SIGNAL  语句之外，MySQL还提供了RESIGNAL  用于引发警告或错误情况的语句。

RESIGNAL  语句与SIGNAL  功能和语法方面的语句类似，不同之处在于：

- 您必须在错误或警告处理程序中使用RESIGNAL语句，否则，您将收到一条错误消息，指出“当处理程序未处于活动状态时RESIGNAL”。请注意，您可以SIGNAL 在存储过程内的任何位置使用语句。
- 您可以省略RESIGNAL语句的所有属性，甚至是SQLSTATE值。
- 如果RESIGNAL 单独使用语句，则所有属性都与传递给条件处理程序的属性相同。

以下存储过程在将错误消息发送给调用方之前更改错误消息。
```
DELIMITER $$
CREATE PROCEDURE Divide(IN numerator INT, IN denominator INT, OUT result double)
BEGIN
	DECLARE division_by_zero CONDITION FOR SQLSTATE '22012';
	DECLARE CONTINUE HANDLER FOR division_by_zero
	RESIGNAL SET MESSAGE_TEXT = 'Division by zero / Denominator cannot be zero';
	IF denominator = 0 THEN
		SIGNAL division_by_zero;
	ELSE
		SET result := numerator / denominator;
	END IF;
END $$
DELIMITER ; 
```
我们调用  Divide()存储过程。
```
CALL Divide(10,0,@result); 
ERROR 1644 (22012): Division by zero / Denominator cannot be zero
```