### 存储过程变量
简介：在本教程中，您将了解存储过程中的变量，如何声明和使用变量。此外，您将了解变量的范围。

变量是一个命名数据对象，其值可以在存储过程  执行期间更改。我们通常使用存储过程中的变量来保存即时结果。这些变量是存储过程的本地变量。您必须在使用之前声明变量。

### 声明变量
要在存储过程中声明变量，请使用以下DECLARE  语句：
```
DECLARE variable_name datatype(size) DEFAULT default_value; 
```
让我们更详细地研究上面的语句：

- 首先，在DECLARE关键字后面指定变量名称  。变量名必须遵循MySQL表列名的命名规则。
- 其次，指定变量的数据类型及其大小。变量可以具有任何MySQL数据类型如INT，VARCHAR，和  DATETIME。
- 第三，当你声明一个变量时，它的初始值是NULL。您可以使用DEFAULT关键字为变量分配默认值  。
例如，我们可以声明一个total_sale用数据类型INT和默认值命名的变量  0  ，如下所示：
```
DECLARE total_sale INT DEFAULT 0; 
```
MySQL允许您使用单个DECLARE语句声明两个或多个共享相同数据类型的变量，如下所示：
```
DECLARE x, y INT DEFAULT 0; 
```
在这个例子中，我们定义了两个整型变量  x和  y，并设置其默认值为零。

### 变量赋值
声明变量后，即可开始使用它。要为变量赋一个值，请使用SET 语句，例如：
```
DECLARE total_count INT DEFAULT 0;
SET total_count = 10; 
```
total_count变量的值10  在赋值之后。

除了SET 语句之外，您还可以使用  SELECT INTO 语句将查询结果（返回标量值）分配给变量。请参阅以下示例：
```
DECLARE total_products INT DEFAULT 0;
 
SELECT 
   COUNT(*) INTO total_products
FROM 
   products; 
```
在上面的例子中：

- 首先，我们声明了一个名为的变量total_products  并将其值初始化为0。
- 然后，我们使用SELECT INTO  语句为total_products  变量分配我们从示例数据库中的products  表中选择的产品数。
### 变量范围
变量有自己的范围来定义其生命周期。如果在存储过程中声明变量，则当END存储过程的语句到达时，它将超出范围。

如果在BEGIN END  块内声明变量，则在END达到时它将超出范围。您可以在不同的范围内声明两个或多个具有相同名称的变量，因为变量仅在其自己的范围内有效。但是，在不同的范围内声明具有相同名称的变量并不是一种好的编程习惯。

名称以@符号开头的变量是会话变量。会话结束前可以访问它。

