# MySQL CASE语句

除了IF语句之外，MySQL还提供了一个名为CASE语句的替代条件语句。CASE语句使代码更具可读性和效率。

- CASE声明有两种形式：简单和搜索CASE语句。

### 简单的CASE声明
我们来看看简单CASE语句的语法：
```
CASE  case_expression
   WHEN when_expression_1 THEN commands
   WHEN when_expression_2 THEN commands
   ...
   ELSE commands
END CASE; 
```
可以您使用简单CASE语句来检查表达式的值与一组唯一值的匹配。

case_expression可以是任何有效的表达式。我们将case_expression的值与每个WHEN子句中的when_expression进行比较，例如when_expression_1，when_expression_2等。如果case_expression和when_expression_n的值相等，则执行相应的WHEN分支中的命令（commands）。

如果WHEN子句中的when_expression与case_expression的值匹配，则ELSE子句中的命令将被执行。ELSE子句是可选的。如果省略ELSE子句，并且找不到匹配项，MySQL将引发错误。

示例以下说明如何使用简单的CASE语句：
```
DELIMITER $$
 
CREATE PROCEDURE GetCustomerShipping(
 in  p_customerNumber int(11), 
 out p_shiping varchar(50))
BEGIN
    DECLARE customerCountry varchar(50);
 
    SELECT country INTO customerCountry
 FROM customers
 WHERE customerNumber = p_customerNumber;
 
    CASE customerCountry
 WHEN  'USA' THEN
    SET p_shiping = '2-day Shipping';
 WHEN 'Canada' THEN
    SET p_shiping = '3-day Shipping';
 ELSE
    SET p_shiping = '5-day Shipping';
 END CASE;
END $$

DELIMITER ; 
```
存储过程如何工作。

GetCustomerShipping()存储过程接受客户编号作为IN参数，并返回一个基于客户国的出货期。
在存储过程中，首先，我们根据输入的客户编号获取客户的国家/地区。然后，我们使用简单CASE语句来比较客户的国家/地区以确定运输时间。如果客户位于USA，则运输期间为2-day shipping。如果客户在Canada，则运输期间为3-day shipping。来自其他国家的客户都有5-day shipping。
以下流程图演示了确定运输时间的逻辑。

### MySQL CASE语句流程图

以下是上述存储过程的测试脚本：
```
SET @customerNo = 112;
 
SELECT country into @country
FROM customers
WHERE customernumber = @customerNo;
 
CALL GetCustomerShipping(@customerNo,@shipping);
 
SELECT @customerNo AS Customer,
       @country    AS Country,
       @shipping   AS Shipping;
``` 
这是输出：
```
+----------+---------+----------------+
| Customer | Country | Shipping       |
+----------+---------+----------------+
|      112 | USA     | 2-day Shipping |
+----------+---------+----------------+
1 row in set (0.00 sec)
```

### 搜索CASE声明
简单CASE语句仅允许您将表达式的值与一组不同的值进行匹配。为了执行更复杂的匹配（例如范围），可以使用搜索的 CASE语句。搜索的CASE语句等同于IF  语句，但是它的构造更具可读性。

以下说明了搜索CASE语句的语法：
```
CASE
    WHEN condition_1 THEN commands
    WHEN condition_2 THEN commands
    ...
    ELSE commands
END CASE; 
```
MySQL会对WHEN子句中的每个条件进行求值，直到找到值为TRUE的条件，然后commands在THEN子句中执行相应的条件。

如果没有TRUE条件，则执行ELSE子句中的命令。如果您没有指定ELSE子句且没有条件TRUE，MySQL将引发错误。

MySQL不允许你在THENor ELSE子句中使用空的commands 。如果您不想ELSE在防止MySQL引发错误的同时处理子句中的逻辑，则可以 在BEGIN END 的ELSE子句中放置一个空块。

以下示例演示如何使用搜索CASE语句查找客户级别SILVER，GOLD或PLATINUM基于客户的信用额度。
```
DELIMITER $$
 
CREATE PROCEDURE GetCustomerLevel(
 in  p_customerNumber int(11), 
 out p_customerLevel  varchar(10))
BEGIN
    DECLARE creditlim double;
 
    SELECT creditlimit INTO creditlim
 FROM customers
 WHERE customerNumber = p_customerNumber;
 
    CASE  
 WHEN creditlim > 50000 THEN 
    SET p_customerLevel = 'PLATINUM';
 WHEN (creditlim <= 50000 AND creditlim >= 10000) THEN
    SET p_customerLevel = 'GOLD';
 WHEN creditlim < 10000 THEN
    SET p_customerLevel = 'SILVER';
 END CASE;
 
END$$

DELIMITER ; 
```
如果信用额度是：

超过50K，那么客户就是  PLATINUM客户。
小于50K且大于10K，那么客户就是  GOLD客户。
不到10K，那么客户就是  SILVER客户。
我们可以通过执行以下测试脚本来测试存储过程：
```
CALL GetCustomerLevel(112,@level);
SELECT @level AS 'Customer Level'; 
+----------------+
| Customer Level |
+----------------+
| PLATINUM       |
+----------------+
1 row in set (0.00 sec)
```

### IF CASE 的选择

MySQL提供既IF和CASE语句使你可以执行基于某些条件的代码块，这被称为流控制。对于大多数开发人员，之间进行选择IF和CASE仅仅是一个个人喜好的问题。但是，当您决定使用IF或时CASE，您应考虑以下几点：

- 一个简单的CASE语句是不是更可读IF语句，当你对一个范围内唯一值的比较单一的表达。此外，简单CASE语句比IF语句更有效。
- 当您基于多个值检查复杂表达式时，IF语句更容易理解。
- 如果选择使用CASE语句，则必须确保至少有一个CASE条件匹配。否则，您需要定义错误处理程序以捕获错误。不必使用IF语句执行此操作。
- 在大多数组织(公司)中，总是有一些所谓的开发指导文件，为开发人员提供了编程风格的命名约定和指导，那么您应参考本文档并遵循开发实践。
- 在某些情况下，IF和CASE之间的混合会让您的存储过程更具可读性和效率。