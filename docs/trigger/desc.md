### MySQL触发器简介
在MySQL中，触发器是一组SQL语句，当对关联表上的数据进行更改时会自动调用这些语句。可以定义触发器在INSERT，UPDATE或DELETE语句更改数据之前或之后调用。在MySQL 5.7.2版之前，您可以为每个表定义最多六个触发器。

- BEFORE INSERT - 在将数据插入表格之前激活。
- AFTER INSERT - 将数据插入表格后激活。
- BEFORE UPDATE - 在更新表中的数据之前激活。
- AFTER UPDATE - 更新表中的数据后激活。
- BEFORE DELETE - 在从表中删除数据之前激活。
- AFTER DELETE - 从表中删除数据后激活。
但是，从MySQL版本5.7.2+开始，您可以为同一触发事件和操作时间定义多个触发器。

当您使用不使用语句INSERT，DELETE或UPDATE语句来修改表中的数据，与表关联的触发器不被调用。例如，TRUNCATE语句删除表的所有数据，但不调用与表关联的触发器。

有一些语句使用INSERT其它语句，如REPLACE语句  或  LOAD DATA语句。如果使用这些语句，则会调用与表关联的相应触发器。

必须为与表关联的每个触发器使用唯一名称。但是，您可以为不同的表定义相同的触发器名称，但这是一种很好的做法。

您应使用以下命名约定命名触发器：
```
(BEFORE | AFTER)_tableName_(INSERT| UPDATE | DELETE) 
```
例如，before_order_update是在order更新表中的行之前调用的触发器。

以下命名约定与上面的命名约定一样好。
```
tablename_(BEFORE | AFTER)_(INSERT| UPDATE | DELETE) 
```
例如，order_before_update与before_order_update上面的触发器相同。

### MySQL触发存储
MySQL将触发器存储在数据目录中，例如，/data/mysqldemo/使用名为的文件tablename.TRG和 triggername.TRN：

tablename.TRG文件将触发器映射到相应的表。
triggername.TRN文件包含触发器定义。
您可以通过将触发器文件复制到备份文件夹来备份MySQL触发器。您还可以使用mysqldump工具备份触发器。

### MySQL触发器限制
MySQL触发器涵盖标准SQL中定义的所有功能。但是，在应用程序中使用它们之前，您应了解一些限制。

MySQL触发器不能：

- 使用SHOW，LOAD DATA，LOAD TABLE，BACKUP DATABASE，RESTORE，FLUSH和RETURN语句。
- 使用隐式或显式提交或回滚的语句，例如COMMIT，ROLLBACK，START TRANSACTION，LOCK / UNLOCK TABLES，ALTER，CREATE，DROP，   RENAME。
- 使用 准备语句 PREPARE和EXECUTE之类的
使用动态SQL语句。
从MySQL版本5.1.4开始，触发器可以调用存储过程或存储函数，这是以前版本的限制。