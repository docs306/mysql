MySQL 删除用户
简介：在本教程中，您将学习如何使用MySQL DROP USER语句删除用户帐户。

MySQL DROP USER语句简介
要删除一个或多个用户帐户，请使用以下DROP USER语句：

DROP USER user, [user],...;
 
要删除用户，请在DROP USER子句后面的格式中指定帐户名称'user_name'@'host_name'。如果要一次删除多个用户，请使用逗号分隔用户列表。

如果删除不存在的用户帐户，MySQL将发出错误。从MySQL 5.7.8开始，您可以使用IF EXISTS子句指示语句为每个不存在的用户帐户发出警告，而不是发出错误。

DROP USER [IF EXISTS] user, [user],...; 
除了删除用户帐户外，DROP USER语句还会删除所有授权表中的所有权限。

删除用户帐户示例
要查看MySQL数据库服务器中的所有用户，请使用以下SELECT语句：

SELECT user, host FROM mysql.user; 
这是我们的数据库服务器中的用户帐户列表：
```
+------------------+-----------+
| user             | host      |
+------------------+-----------+
| api@localhost    | %         |
| crm_dev          | %         |
| crm_read         | %         |
| crm_write        | %         |
| remote           | %         |
| rfc              | %         |
| root             | %         |
| superadmin       | %         |
| auditor          | localhost |
| crm_dev1         | localhost |
| crm_read1        | localhost |
| crm_write1       | localhost |
| crm_write2       | localhost |
| dbadmin          | localhost |
| mysql.infoschema | localhost |
| mysql.session    | localhost |
| mysql.sys        | localhost |
| super            | localhost |
+------------------+-----------+
18 rows in set (0.07 sec)
```
假设您要删除用户帐户dbadmin@localhost，请使用以下语句：

DROP USER dbadmin@localhost; 
mysql.user再次从表中查询数据，您将看到dbadmin@localhost已删除的数据。
```
SELECT user, host FROM mysql.user; 
```
用户帐户dbadmin@localhost已被删除，如以下结果集所示：
```
+------------------+-----------+
| user             | host      |
+------------------+-----------+
| api@localhost    | %         |
| crm_dev          | %         |
| crm_read         | %         |
| crm_write        | %         |
| remote           | %         |
| rfc              | %         |
| root             | %         |
| superadmin       | %         |
| auditor          | localhost |
| crm_dev1         | localhost |
| crm_read1        | localhost |
| crm_write1       | localhost |
| crm_write2       | localhost |
| mysql.infoschema | localhost |
| mysql.session    | localhost |
| mysql.sys        | localhost |
| super            | localhost |
+------------------+-----------+
17 rows in set (0.06 sec)
```
要在单个语句中删除用户'api@localhost'@%和remote用户帐户DROP USER，请使用以下语句：

DROP USER 'api@localhost', remote; 
让我们验证删除操作。

SELECT user, host FROM mysql.user; 
我们还剩下四个帐户，这两个帐户已被删除。
```
+------------------+-----------+
| user             | host      |
+------------------+-----------+
| crm_dev          | %         |
| crm_read         | %         |
| crm_write        | %         |
| rfc              | %         |
| root             | %         |
| superadmin       | %         |
| auditor          | localhost |
| crm_dev1         | localhost |
| crm_read1        | localhost |
| crm_write1       | localhost |
| crm_write2       | localhost |
| mysql.infoschema | localhost |
| mysql.session    | localhost |
| mysql.sys        | localhost |
| super            | localhost |
+------------------+-----------+
15 rows in set (0.01 sec)
```
假设用户帐户已登录并且活动会话正在运行。如果删除用户帐户，则不会停止打开的会话。活动会话将继续，直到用户退出。通常，在这种情况下，您应在执行DROP USER语句之前立即关闭用户的会话。

首先，您需要使用SHOW PROCESSLIST语句来标识用户的进程ID 。
```
mysql> show processlist;
+----+-----------------+----------------------+-------+---------+---------+------------------------+------------------+
| Id | User            | Host                 | db    | Command | Time    | State                  | Info             |
+----+-----------------+----------------------+-------+---------+---------+------------------------+------------------+
| 66 | root            | localhost:55402      | mysql | Query   |       0 | starting               | show processlist |
| 78 | dbadmin         | localhost:63139      | NULL  | Sleep   |     115 |                        | NULL             |
+----+-----------------+----------------------+-------+---------+---------+------------------------+------------------+
9 rows in set (0.01 sec)
```
如您所见，dbadmin@localhost用户帐户具有进程ID 78。

其次，你通过使用来杀死这个过程

KILL 78; 
用户帐户dbadmin@localhost收到错误消息：

ERROR 2013 (HY000): Lost connection to MySQL server during query
第三，执行DROP USER语句删除用户帐户dbadmin@localhost：

DROP USER dbadmin@localhost; 