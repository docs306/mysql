MySQL 权限撤销
简介：在本教程中，您将学习如何使用MySQL REVOKE语句来撤销MySQL帐户的权限。

我们强烈建议您按照下面的教程更好地了解MySQL REVOKE的工作原理：

MySQL门禁系统入门
如何创建MySQL用户
如何为MySQL用户授予权限
MySQL REVOKE语句简介
要从用户帐户撤消权限，请使用MySQL REVOKE语句。MySQL允许您撤消用户的一项或多项权限或所有权限。

以下说明了从用户撤消特定权限的语法：
```
REVOKE   privilege_type [(column_list)]      
        [, priv_type [(column_list)]]...
ON [object_type] privilege_level
FROM user [, user]... 
```
让我们更详细地研究MySQL REVOKE语句。

- 首先，在REVOKE关键字后面指定要从用户撤消的权限列表。您需要用逗号分隔权限。
- 其次，指定在ON子句中撤销特权的特权级别。
- 第三，指定要撤消FROM子句中的权限的用户帐户。
请注意，要撤消用户帐户的权限，您必须具有  GRANT OPTION权限以及要撤消的权限。

要撤消用户的所有权限，请使用以下形式的REVOKE语句：
```
REVOKE ALL PRIVILEGES, GRANT OPTION FROM user [, user]… 
```
要执行REVOKE ALL语句，您必须具有mysql数据库的全局CREATE USER特权或UPDATE特权。

要撤消代理用户，请使用以下REVOKE PROXY命令：
```
REVOKE PROXY ON user FROM user [, user]... 
```
代理用户是MySQL中可以模仿其他用户的有效用户，因此，代理用户具有其模拟的用户的所有权限。

在撤消用户权限之前，最好使用以下SHOW GRANTS语句检查用户是否具有权限：
```
SHOW GRANTS FOR user; 
```
MySQL REVOKE示例
假设rfc用户对示例数据库具有SELECT，UPDATE和DELETE权限。如果你想撤销和来自特权用户，你可以按照如下操作步骤：mysqldemo UPDATEDELETErfc

首先，检查用户使用SHOW GRANTS语句的权限：
```
SHOW GRANTS FOR rfc; 
GRANT SELECT, UPDATE, DELETE ON 'mysqldemo'.* TO 'rfc'@'%'; 
```
请注意，并按照  授予的特权用户  的教程，你可以创建RFC帐户，并授予SELECT，UPDATE以及DELETE权限的，如下所示：
```
CREATE USER IF EXISTS rfc IDENTIFIED BY 'dolphin';
 
GRANT SELECT, UPDATE, DELETE ON  mysqldemo.* TO rfc;
``` 
其次，你可以撤销UPDATE，并DELETE从特权 rfc 用户：
```
REVOKE UPDATE, DELETE ON mysqldemo.*  FROM rfc; 
```
第三，您可以rfc使用SHOW GRANTS命令再次检查用户的权限。
```
SHOW GRANTS FOR 'rfc'@'localhost'; 
GRANT SELECT ON 'mysqldemo'.* TO 'rfc'@'%'; 
```
如果要撤消rfc用户的所有权限，请执行以下命令：
```
REVOKE ALL PRIVILEGES, GRANT OPTION FROM rfc; 
```
如果rfc 再次检查用户的权限，您将看到 rfc 用户没有权限。
```
SHOW GRANTS FOR rfc; 
GRANT USAGE ON *.* TO 'rfc'@'%'; 
```
注意：USAGE权限意味着MySQL中没有权限。

当MySQL REVOKE命令生效时
MySQL REVOKE语句的效果取决于权限级别，如下所示：

- 对全局特权所做的更改仅在客户端在后续会话中连接到MySQL时生效。更改不会应用于所有当前连接的用户。
- 在下一个USE语句之后应用数据库权限的更改。
- 表和列权限的更改将应用​​于更改后发出的所有查询。