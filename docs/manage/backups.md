mysqldump 备份工具
简介：在本教程中，您将学习如何使用mysqldump工具备份MySQL数据库。

MySQL GUI工具（如phpMyAdmin，SQLyog等）通常可以轻松地为备份MySQL数据库提供功能。但是，如果您的数据库很大，备份过程可能会非常慢，因为备份文件需要通过网络传输到客户端PC。结果，备份过程增加了MySQL数据库服务器的锁定和可用时间。

MySQL提供了一个非常有用的工具，可以非常快速地在服务器上本地备份或转储MySQL数据库。备份文件存储在服务器的文件系统中，因此您只需在需要时下载它。

备份MySQL数据库的工具是mysqldump。它位于MySQL安装文件夹的root / bin文件夹中。

mysqldump是MySQL提供的一个程序，可用于转储数据库以备份或传输数据库到另一个数据库服务器。

转储文件包含一组用于创建数据库对象的SQL语句。此外，mysqldump可用于生成CSV，分隔符或XML文件。在本教程中，我们将仅关注如何使用mysqldump工具备份MySQL数据库。

在本教程中，我们将仅关注如何使用mysqldump工具备份MySQL数据库。

如何备份MySQL数据库
要备份MySQL数据库，首先必须在数据库服务器中存在数据库，并且您也可以访问服务器。如果您没有远程桌面，可以使用SSH或Telnet登录远程服务器。备份MySQL数据库的命令如下：

如果无法远程桌面登录，可以使用SSH或Telnet登录远程服务器。备份MySQL数据库的命令如下：

mysqldump -u [username] –p[password] [database_name] > [dump_file.sql] 
上面命令的参数如下：

[username]：有效的MySQL用户名。
[password]：用户的有效密码。请注意，-p和密码之间没有空格。
[database_name]：要备份的数据库名称
[dump_file.sql]：要生成的转储文件。
通过执行上述命令，所有数据库结构和数据都将导出到单个[dump_file.sql]转储文件中。例如，为了支持我们的示例数据库mysqldemo，我们使用以下命令：

mysqldump -u mysqltutorial –psecret  mysqldemo > c:\temp\backup001.sql 
如何仅备份MySQL数据库结构
如果你只想备份数据库结构，你只需要添加一个选项-no-data告诉mysqldump只需要导出数据库结构，如下所示：

mysqldump -u [username] –p[password] –no-data [database_name] > [dump_file.sql] 
例如，要仅使用结构备份我们的示例数据库，请使用以下命令：

mysqldump -u mysqltutorial –psecret  -no-data mysqldemo > c:\temp\backup002.sql 
如何仅备份MySQL数据库数据
有一种情况是您要在登台和开发系统中刷新数据，因此这些系统中的数据与生产系统相同。

在这种情况下，您只需要从生产系统导出数据并将其导入到登台和开发系统。要仅备份数据，请使用mysqldump的-no-create-info选项，如下所示：

mysqldump -u [username] –p[password] –no-create-info [database_name] > [dump_file.sql] 
例如，要仅使用数据备份我们的示例数据库，请使用以下命令：

mysqldump –u mysqltutorial –psecret –no-create-info mysqldemo > c:\temp\backup002.sql 
如何将多个MySQL数据库备份到单个文件中
如果要通过[database_name]中的命令备份多个数据库，只需单独的数据库名称。如果要备份数据库服务器中的所有数据库，请使用-all-database选项。

mysqldump -u [username] –p[password]  [dbname1,dbname2,…] > [dump_file.sql]
 
mysqldump -u [username] –p[password] –all-database > [dump_file.sql] 