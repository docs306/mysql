MySQL 数据库列表
简介：在本教程中，您将学习如何使用MySQL SHOW DATABASES命令列出MySQL数据库服务器中的所有数据库。

使用MySQL SHOW DATABASES
要列出MySQL服务器主机上的所有数据库，请使用以下SHOW DATABASES命令：

SHOW DATABASES; 
例如，要列出本地MySQL数据库服务器中的所有数据库，首先登录到数据库服务器，如下所示：

>mysql -u root -p
Enter password: **********
mysql> 
然后使用SHOW DATABASES命令：
```
+--------------------+
| Database           |
+--------------------+
| information_schema |
| mysql              |
| mysqldemo          |
| performance_schema |
+--------------------+
4 rows in set (0.01 sec)
```
SHOW SCHEMAS命令是同义词SHOW DATABASES，因此以下命令返回与上面相同的结果：

SHOW SCHEMAS; 
如果要查询与特定模式匹配的数据库，请使用以下LIKE 子句：

SHOW DATABASES LIKE pattern; 
例如，以下语句返回以字符串结尾的数据库'schema';
```
mysql> SHOW DATABASES LIKE '%schema';
+--------------------+
| Database (%schema) |
+--------------------+
| information_schema |
| performance_schema |
+--------------------+
2 rows in set (0.00 sec)
```
重要的是要注意，如果MySQL数据库服务器启动--skip-show-database，SHOW DATABASES除非您拥有SHOW DATABASES权限，否则不能使用语句。

从information_schema查询数据库数据
如果LIKE子句中的条件不足，则可以直接从information_schema数据库中的schemata表中查询数据库信息。

例如，以下查询返回与SHOW DATABASES命令相同的结果。
```
SELECT schema_name 
FROM information_schema.schemata; 
以下SELECT语句返回名称以'schema'或结尾的数据库's'。

SELECT schema_name
FROM information_schema.schemata
WHERE schema_name LIKE '%schema' OR 
      schema_name LIKE '%s'; 
```
它返回以下结果集：
```
+--------------------+
| schema_name        |
+--------------------+
| information_schema |
| performance_schema |
+--------------------+
2 rows in set (0.00 sec)
```