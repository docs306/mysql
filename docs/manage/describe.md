使用DESCRIBE语句
要显示表的所有列，请使用以下步骤：

登录MySQL数据库服务器。
切换到特定数据库。
使用DESCRIBE声明。
以下示例演示如何orders在mysqldemo数据库中显示表的列。

第1步。登录MySQL数据库。

>mysql -u root -p
Enter password: **********
mysql> 
第2步。发出USE命令切换到数据库mysqldemo：

mysql> USE mysqldemo;
Database changed
mysql> 
第3步。使用DESCRIBE声明。
```
mysql> DESCRIBE orders;
+----------------+-------------+------+-----+---------+-------+
| Field          | Type        | Null | Key | Default | Extra |
+----------------+-------------+------+-----+---------+-------+
| orderNumber    | int(11)     | NO   | PRI | NULL    |       |
| orderDate      | date        | NO   |     | NULL    |       |
| requiredDate   | date        | NO   |     | NULL    |       |
| shippedDate    | date        | YES  |     | NULL    |       |
| status         | varchar(15) | NO   |     | NULL    |       |
| comments       | text        | YES  |     | NULL    |       |
| customerNumber | int(11)     | NO   | MUL | NULL    |       |
+----------------+-------------+------+-----+---------+-------+
7 rows in set (0.01 sec) 
```
实际上，您使用的DESC语句是语句的简写DESCRIBE。例如，以下语句等同于DESCRIBE以上内容：

DESC orders; 
MySQL SHOW COLUMNS命令
获取表中列列表的更灵活方法是使用MySQL SHOW COLUMNS 命令。

SHOW COLUMNS FROM table_name; 
要显示表的列，请FROM在SHOW COLUMNS语句的子句中指定表名。若要在不是当前数据库的数据库中显示表的列，请使用以下格式：

SHOW COLUMNS FROM database_name.table_name; 
要么

SHOW COLUMNS FROM table_name IN database_name; 
例如，要获取orders表的列，请使用以下SHOW COLUMNS语句：

SHOW COLUMNS FROM orders; 
如您所见，此SHOW COLUMNS命令的结果与DESC语句的结果相同。

要获取有关列的更多信息，请将FULL关键字添加到SHOW COLUMNS命令，如下所示：

SHOW FULL COLUMNS FROM table_name; 
例如，以下语句列出了mysqldemo数据库中支付表的所有列。
```
mysql> SHOW FULL COLUMNS FROM payments \G;
*************************** 1. row ***************************
     Field: customerNumber
      Type: int(11)
 Collation: NULL
      Null: NO
       Key: PRI
   Default: NULL
     Extra:
Privileges: select,insert,update,references
   Comment:
*************************** 2. row ***************************
     Field: checkNumber
      Type: varchar(50)
 Collation: utf8_general_ci
      Null: NO
       Key: PRI
   Default: NULL
     Extra:
Privileges: select,insert,update,references
   Comment:
*************************** 3. row ***************************
     Field: paymentDate
      Type: date
 Collation: NULL
      Null: NO
       Key:
   Default: NULL
     Extra:
Privileges: select,insert,update,references
   Comment:
*************************** 4. row ***************************
     Field: amount
      Type: decimal(10,2)
 Collation: NULL
      Null: NO
       Key:
   Default: NULL
     Extra:
Privileges: select,insert,update,references
   Comment:
4 rows in set (0.02 sec)
```
正如你所看到的，SHOW FULL COLUMNS命令添加collation，privileges和comment列到结果集。

SHOW COLUMNS命令允许您使用LIKE 运算符或WHERE 子句过滤表的列   ：

SHOW COLUMNS FROM table_name LIKE pattern;
 
SHOW COLUMNS FROM table_name WHERE expression; 
例如，要仅显示以字母开头的列c，请LIKE按如下方式使用运算符：
```
mysql> SHOW COLUMNS FROM payments LIKE 'c%';
+----------------+-------------+------+-----+---------+-------+
| Field          | Type        | Null | Key | Default | Extra |
+----------------+-------------+------+-----+---------+-------+
| customerNumber | int(11)     | NO   | PRI | NULL    |       |
| checkNumber    | varchar(50) | NO   | PRI | NULL    |       |
+----------------+-------------+------+-----+---------+-------+
2 rows in set (0.00 sec)
```