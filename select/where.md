

可以在WHERE子句中使用复杂的形成搜索条件，例如：

 - BETWEEN 选择值范围内的值。
 - LIKE 匹配基于模式匹配的值。
 - IN 指定值是否与集合中的任何值匹配。
 - IS NULL检查值是否为NULL。

 > 注意: 在MySQL中，零被视为false，非零被视为true。
 > 注意：查询不区分大小写，因此b%和B%模式返回的结果相同。


### And
AND操作是将两个或多个逻辑运算符布尔表达式只有当两个表达式为true返回true

AND运算符的语法：
```sql
boolean_expression_1 AND boolean_expression_2 
```
1. AND运算符往往是使用在SELECT，UPDATE，DELETE语句中的 WHERE子句中。
2. AND运算符还用于 INNER JOIN 和 LEFT JOIN 子句中关联条件。

逻辑操作符AND 和 OR也称作 短路操作符 或者 惰性求值 它们的参数从左向右解析，一旦结果可以确定就停止


### Or
OR运算符组合两个布尔表达式，当任一条件为真时返回true。
```
SELECT true OR false AND false; 
```
这个语句中 And 会先执行
- **注意：**

1. 加上括号可以改变优先级。
2. 不加括号，按NOT、AND、OR的优先级进行执行，NOT > AND > OR。


### In
IN  运算符允许你确定一个指定的值在一组值匹配的任何值或子查询

语法:
```
SELECT column1,column2,... FROM table_name
WHERE (expr|column1) IN ('value1','value2',...); 
```
可以仔细的查以下方式：
- 在WHERE子句中使用带column或IN运算符的表达式（expr）
- 用逗号（,）分隔列表中的值。
IN运算符逻辑是：column 在指定列表中则返回1，不在其中则返回0.

当列表中的值都是常量时，MySQL执行以下步骤：
- 首先，评估基于所述值类型的的column_1所述的或expr 表达的结果。 
- 其次，对值进行排序。
- 第三，使用二进制搜索算法搜索值。因此，使用IN  运算符查询执行带有常量列表的速度非常快。
> 请注意：如果expr列表中的任何值NULL，则IN  运算符返回NULL。

IN运算符与NOT运算符组合以来检查值是否与列表或子查询中的任何值不匹配。<br/>
也可以其它语句中在WHERE子句中使用IN运算符，例如  UPDATE，和DELETE

如果列表有多个值，用多个 OR 语句会非常长的语句。而 IN 缩短查询并更具可读性。
```sql
/** 查询不在上海和北京的咖啡馆 */
SELECT city FROM offices
WHERE country NOT IN ('ShangHai' , 'BeiJing');
```

1. IN与子查询一起使用
IN操作通常与子查询使用。子查询不是提供文字值列表，而是从一个或多个表中获取值列表，并将它们用作IN运算符的输入值。

如果想找订单的总价值是大于60000，可以使用IN运算符的查询：
```sql
SELECT orderNum, status, shippedDate FROM orders
WHERE orderNum IN
(SELECT orderNum FROM orderDetails
GROUP BY orderNum HAVING SUM(quantity * price) > 60000); 
```
上面的整个查询可以分解为两个单独的查询。

- 首先，子查询返回一个订单号列表，其值大于60000使用GROUP BY 和HAVING 子句：
```sql
SELECT orderNum FROM orderDetails
GROUP BY orderNum HAVING SUM(quantity * price) > 60000
````
- 其次，外部查询使用WHERE子句中的IN运算符从orders表中获取数据：
```sql
SELECT orderNum, status, shippedDate FROM orders
WHERE orderNum IN (10165,10287,10310);
```
### Like
LIKE运算符检测一个字符串是否包含指定的或不包含。

语法：
```sql
expression LIKE pattern ESCAPE escape_character 
```
MySQL提供了两个通配符供 LIKE 使用：百分号%和下划线_
- 百分号（%）通配符匹配任何零个或多个字符的字符串
- 下划线（ _ ）通配符匹配任何单个字符

例如：
- s%匹配任何个数字符以s字符开头，例如sun和six。
- se_匹配以 se 开头，后面跟着的任何一个字符，如see和sea。

```sql
SELECT lastName, firstName FROM employees
WHERE lastname LIKE '%on%'; 
```
1. LIKE和NOT运算符实例
MySQL可以使用NOT运算符与LIKE运算符组合以查找与特定模式不匹配的字符串。

假设要搜索姓氏不以B字符开头的员工，可以使用NOT LIKE以下查询中显示的模式：
```sql
SELECT lastName, firstName FROM employees
WHERE lastName NOT LIKE 'B%'; 
```
2. LIKE 与ESCAPE子句
有时我们要匹配的模式包含通配符，例如`10％`, `_20` 等。
在这种情况下，您可以使用ESCAPE子句指定转义字符，以便 MySQL 将通配符解释为文字字符

如果未明确指定转义字符，则反斜杠字符\是缺省转义字符。

例如，如果要查找产品代码包含字符串的产品_20，可以使用%\_20%以下查询中显示的模式：
```sql
SELECT productCode, productName FROM products
WHERE productCode LIKE '%\_20%'; 
```
或者我们可以指定不同的转义字符，例如，以下ESCAPE子句使用$：
```sql
SELECT productCode, productName FROM products
WHERE productCode LIKE '%$_20%' ESCAPE '$';
```
### Between
BETWEEN运算符是一个逻辑运算符，指定是否在某个范围内的值是。

BETWEEN运算符通常用于SELECT，UPDATE和DELETE语句的WHERE子句中。

语法:
```sql
expr [NOT] BETWEEN begin_expr AND end_expr; 
````
expr 是在 begin_expr 和 end_expr定义的范围内测试的表达式。
- 三个表达式：expr, begin_expr和 end_expr 必须具有相同的数据类型。

的BETWEEN操作者如果的值返回真 expr 大于或等于（> =）的值 begin_expr，且小于或等于的（<=）的值 end_expr，否则它返回零。

所述NOT BETWEEN返回true，如果值 expr 小于（<）的值 begin_expr 或比值的值大 end_expr，否则返回0。

如果有任何表达式NULL，则BETWEEN运算符返回 NULL 。

如果要指定独占范围，可以使用大于（>）和小于（<）运算符。

1. BETWEEN和数字示例
```sql
SELECT productName, price FROM products
WHERE price BETWEEN 90 AND 100; 
```

2. BETWEEN和日期实例
使用 BETWEEN 带日期值时，应使用类型转换将列或表达式的类型显式转换为DATE类型。

示例返回在01/01/2003到01/31/2003之间具有所需日期的订单：
```sql
SELECT orderNum, requiredDate FROM orders
WHERE requiredDate BETWEEN CAST('2013-01-01' AS DATE) AND CAST('2013-01-31' AS DATE); 
```
使用CAST运算符将文字字符串'2013-01-01'和'2013-12-31'转换为DATE值


### IS Null
要测试值是否值NULL，请使用 IS NULL运算符。以下是IS NULL运算符的语法：
```
value IS NULL 
```
如果值为NULL，则表达式返回true。否则，它返回false。

请注意，MySQL没有内置BOOLEAN类型。它使用TINYINT(1)来表示BOOLEAN 值，即，true表示1，false表示0。

因为它IS NULL是一个比较运算符，可以在任何可以使用运算符的地方使用它，如 SELECT or WHERE子句中.

要检查值是否不是NULL，请使用IS NOT NULL运算符，如下所示：
```
value IS NOT NULL 
```

1. IS NULL的特殊功能
为了与ODBC程序兼容，MySQL支持IS NULL运算符的一些特殊功能。

1）如果具有约束并包含特殊日期的列DATE或DATETIME列，则可以使用运算符查找此类行。'0000-00-00'IS NULL

2. IS NULL优化
MySQL对于IS NULL运算符执行相同的优化，就像它对等号（=）运算符一样。


### EXISTS

EXISTS是一个布尔运算符返回true或false。EXISTS经常使用的在一个子查询，以测试一个“存在”状态。

以下说明了EXISTS运营商的常见用法。
```sql
SELECT column1 FROM table_name
WHERE [NOT] EXISTS(subquery); 
```
如果子查询返回任何行，则EXISTS运算符返回true，否则返回false。

此外，EXISTS一旦找到匹配的行，立即终止进一步的处理。由于此特性，您可以EXISTS在某些情况下使用运算符来提高查询的性能。

NOT EXISTS是否定操作符。换句话说，如果子查询没有返回任何行，NOT EXISTS则返回true，否则返回false。

您可以使用`SELECT *，SELECT column，SELECT constant` 或在子查询任何东西。
结果是相同的，因为MySQL忽略 `select_list` 在 SELECT子句中出现的结果。

假设要查找至少有一个销售订单的客户，请按以下方式使用EXISTS运算符：
```sql
SELECT customerNum, customer FROM customers
WHERE
    EXISTS(SELECT 1 FROM orders WHERE orders.customerNum = customers.customerNum);
```
对于在customers表每一行，查询检查customerNum 的orders表。

如果表 customerNum 中出现的customers表存在于orders表中，则子查询返回第一个匹配的行。<br/>
结果EXISTS运算符返回true并停止扫描orders表。否则，子查询不返回任何行EXISTS操作符返回false。

要获得未有任何销售订单的客户，请使用 NOT EXISTS 运算符作为以下语句：
```sql
SELECT customerNum, customerName FROM customers
WHERE
    NOT EXISTS(SELECT 1 FROM orders WHERE orders.customerNum = customers.customerNum); 
```

1. EXISTS与IN
要查找至少有一个销售订单的客户，可以使用IN运算符，如下所示：
```sql
SELECT customerNum, customerName FROM customers
WHERE customerNum IN (SELECT customerNum FROM orders); 
```
让我们比较使用的查询IN操作与使用的一个EXISTS使用操作符EXPLAIN的语句。
```sql
EXPLAIN SELECT customerNum, customerName FROM customers
WHERE EXISTS(SELECT 1 FROM orders WHERE orders.customerNum = customers.customerNum); 
```

使用EXISTS运算符的查询比使用IN运算符的查询要快得多。

原因是EXISTS操作员基于“至少找到”原则工作。它返回true并在找到至少一个匹配行后停止扫描表。

另一方面，当IN运算符与子查询组合时，MySQL必须首先处理子查询，然后使用子查询的结果来处理整个查询。

一般的经验法则是，如果子查询包含大量数据，则EXISTS运算符可提供更好的性能。

但是，如果从子查询返回的结果集非常小，则使用IN运算符的查询将执行得更快。


