### 子查询带有比较运算符
您可以使用比较运算符（例如，=，>，<等等）将子查询返回的单个值与WHERE子句中的表达式进行比较。

例如，以下查询返回具有最高付款金额的客户。
```sql
SELECT customerNum, checkNum, amount FROM payments
WHERE amount = (SELECT MAX(amount) FROM payments); 
```


### 子查询带有IN和NOT IN运算符
如果子查询返回多个值，则可以在WHERE子句中使用其他运算符，例如IN或NOT IN运算符。
```sql
SELECT customerName FROM customers
WHERE customerNum NOT IN (SELECT DISTINCT customerNum FROM  orders);
```


### 子查询中的FROM子句
在FROM子句中使用子查询时，从子查询返回的结果集将用作临时表。此表称为派生表或实现子查询。

以下子查询查找销售订单中的最大，最小和平均项数：
```sql
SELECT MAX(items), MIN(items), FLOOR(AVG(items))
FROM (SELECT orderNum, COUNT(orderNum) AS items FROM orderdetails GROUP BY orderNum) AS lineitems; 
```
> 注意：FLOOR()函数用于从项目的平均值中删除小数位。


选择购买价格高于每个 产品系列中所有产品的平均购买价格的产品。
```sql
SELECT productName, buyprice FROM products p1
WHERE buyprice > (SELECT AVG(buyprice) FROM products WHERE productLine = p1.productLine);
```


### 子查询带有EXISTS和NOT EXISTS
当子查询与  EXISTSor NOT EXISTS运算符一起使用时，子查询返回布尔值TRUEor FALSE。

以下查询说明了与EXISTS运算符一起使用的子查询：
```sql
SELECT * FROM table_name WHERE EXISTS( subquery ); 
```
在上面的查询中，如果子查询返回任何行，则EXISTS subquery返回TRUE，否则返回FALSE。

EXISTS和NOT EXISTS经常在用于相关子查询。


查询选择总值大于60000的销售订单。
```sql
SELECT orderNum, SUM(priceEach * quantity) total FROM orderdetails
INNER JOIN orders USING (orderNum)
GROUP BY orderNum HAVING SUM(price * quantity) > 60000; 
```

查询作为相关子查询来查找通过使用EXISTS运算符放置至少一个总值大于60K的销售订单的客户：
```sql
SELECT customerNum, customerName FROM customers
WHERE
    EXISTS(SELECT orderNum, SUM(price * quantity) FROM orderdetails INNER JOIN orders USING (orderNum)
        WHERE customerNum = customers.customerNum
        GROUP BY orderNum
        HAVING SUM(price * quantity) > 60000); 
```

